import sync from "browser-sync";
const browserSync = sync.create();

import {
    pcss,
    additionalStyles,
    adminStyles,
    postcssMain,
    formBuilderPostCSS,
    landingsPostCSS,
    portalPostCSS,
    profilePostCSS,
    shopPostCSS,
    talentPostCSS,
} from "./pcss.js";
import { tailwind } from "./tailwind.js";
import { babelMeta, babelMain } from "./babel.js";

import { path } from "../gulp-settings.js";

export function serve() {
    browserSync.init({
        proxy: path.projectLink,
        open: false,
        // uncomment if you don't have internet connection
        // online: false
    });

    const autoRefresh = browserSync.reload; // make false if don't want to autoRefresh

    // Tailwind Watcher
    app.gulp
        .watch(
            [
                "./app/modules/**/*.php",
                "./app/layout/*.php",
                "./app/modules/!panel/**",
                "./app/public/js/*.js",
                `${app.path.styles.tailwind}`,
            ],
            app.gulp.series(tailwind)
        )
        .on("change", autoRefresh);

    // Additional Styles
    app.gulp
        .watch(
            [
                `${app.path.styles.componentsSDAll}`,
                `!${app.path.styles.adminStyles}`,
            ],
            app.gulp.series(additionalStyles)
        )
        .on("change", autoRefresh);

    // Admin-panel Styles
    app.gulp
        .watch([`${app.path.styles.adminStyles}`], app.gulp.series(adminStyles))
        .on("change", autoRefresh);

    // form-builder Styles
    app.gulp
        .watch(
            [`${app.path.styles.modules.formbuilder}`],
            app.gulp.series(formBuilderPostCSS)
        )
        .on("change", autoRefresh);

    // landings Styles
    app.gulp
        .watch(
            [`${app.path.styles.modules.landings}`],
            app.gulp.series(landingsPostCSS)
        )
        .on("change", autoRefresh);

    // portal Styles
    app.gulp
        .watch(
            [`${app.path.styles.modules.portal}`],
            app.gulp.series(portalPostCSS)
        )
        .on("change", autoRefresh);

    // profile Styles
    app.gulp
        .watch(
            [`${app.path.styles.modules.profile}`],
            app.gulp.series(profilePostCSS)
        )
        .on("change", autoRefresh);

    // shop Styles
    app.gulp
        .watch(
            [`${app.path.styles.modules.shop}`],
            app.gulp.series(shopPostCSS)
        )
        .on("change", autoRefresh);

    // talent Styles
    app.gulp
        .watch(
            [`${app.path.styles.modules.talent}`],
            app.gulp.series(talentPostCSS)
        )
        .on("change", autoRefresh);

    // PostCSS Watcher
    app.gulp
        .watch(
            [
                `${app.path.styles.pcssAll}`,
                `!${app.path.styles.tailwind}`,
                `!${app.path.styles.componentsSDAll}`,
                `!${app.path.styles.modules.folder}`,
            ],
            app.gulp.series(postcssMain)
        )
        .on("change", autoRefresh);

    // babel main.js Watcher
    app.gulp
        .watch(`${app.path.babelMain}`, app.gulp.series(babelMain))
        .on("change", autoRefresh);

    // babel meta-settings.js Watcher
    app.gulp
        .watch(`${app.path.babelMetaSettings}`, app.gulp.series(babelMeta))
        .on("change", autoRefresh);
}
