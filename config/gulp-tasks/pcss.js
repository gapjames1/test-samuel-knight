import postcss from "gulp-postcss";
import pcssImport from "postcss-import";
import pcssMixins from "postcss-mixins";
import nested from "postcss-nested";
import pcssShort from "postcss-short";
import pcssPresetEnv from "postcss-preset-env";
import cqPolyfill from "cq-prolyfill/postcss-plugin.js";
import pcssAutoprefixer from "autoprefixer";
import pcssFunctions from "postcss-functions";
import cssnano from "cssnano";
import pcssReporter from "postcss-reporter";
import pcssCustomProperties from "postcss-custom-properties";
import rename from "gulp-rename";
import postcssColor from "postcss-color-mod-function";
import rfs from "rfs";
import filters from "pleeease-filters";
import postcssExtend from "postcss-extend";
import sourcemaps from "gulp-sourcemaps";
import stylelint from "stylelint";
import scssVariables from "postcss-simple-vars";
import postcssShortNativeVars from 'postcss-short-native-vars';


import {
    ac,
    rc,
    perc,
    vw,
    setTransition,
} from "../../app/public/styles/_functions/pcss-functions.js";
import { reportFormatter } from "../gulp-settings.js";

export function postcssMain() {
    return pcss(app.path.styles.pcss);
}

export function additionalStyles() {
    return pcss(app.path.styles.additionalStyles);
}

export function adminStyles() {
    return pcss(app.path.styles.adminStyles, app.path.styles.adminStylesDist);
}

export function formBuilderPostCSS() {
    return pcss(app.path.styles.modules.formbuilder);
}

export function landingsPostCSS() {
    return pcss(app.path.styles.modules.landings);
}

export function portalPostCSS() {
    return pcss(app.path.styles.modules.portal);
}

export function profilePostCSS() {
    return pcss(app.path.styles.modules.profile);
}

export function shopPostCSS() {
    return pcss(app.path.styles.modules.shop);
}

export function talentPostCSS() {
    return pcss(app.path.styles.modules.talent);
}

export function pcss(filePathSrc, filePathDist = app.path.styles.css) {
    const plugins = [
        pcssImport,
        scssVariables,
        rfs({ breakpoint: 1440 }),
        pcssMixins,
        postcssExtend,
        postcssShortNativeVars,
        pcssFunctions({
            functions: { ac, rc, perc, vw, setTransition },
        }),
        pcssCustomProperties,
        postcssColor,
        pcssShort,
        nested,
        pcssPresetEnv({ stage: 0 }),
        pcssAutoprefixer,
        cqPolyfill,
        cssnano({
            convertValues: {
                length: false,
            },
            discardComments: {
                removeAll: true,
            },
        }),
        pcssReporter({
            clearReportedMessages: true,
            formatter: reportFormatter,
        }),
    ];

    return app.gulp
        .src(filePathSrc, { allowEmpty: true })
        .pipe(sourcemaps.init())
        .pipe(postcss(plugins))
        .pipe(
            rename({
                extname: ".css",
            })
        )
        .pipe(sourcemaps.write("."))
        .pipe(app.gulp.dest(filePathDist));
}

export function pcssStylelint() {
    const plugins = [
        pcssImport({
            plugins: [stylelint],
        }),
        pcssReporter({
            clearReportedMessages: true,
            formatter: reportFormatter,
        }),
    ];

    return app.gulp.src(`${app.path.pcss}`).pipe(postcss(plugins));
}
