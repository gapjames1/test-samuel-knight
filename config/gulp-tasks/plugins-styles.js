import concat from "gulp-concat";
import cssnano from "gulp-cssnano";
import sourcemaps from "gulp-sourcemaps";

export function pluginsStyles() {
    return app.gulp
        .src(app.path.cssPlugins)
        .pipe(sourcemaps.init())
        .pipe(
            cssnano({
                convertValues: {
                    length: false,
                },
                discardComments: {
                    removeAll: true,
                },
            })
        )
        .pipe(concat("plugins-styles.css"))
        .pipe(sourcemaps.write("."))
        .pipe(app.gulp.dest(`${app.path.css}`));
}
