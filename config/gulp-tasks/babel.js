import babel from "gulp-babel";
import uglify from "gulp-uglify";
import concat from "gulp-concat";
import sourcemaps from "gulp-sourcemaps";

export function babelMeta() {
    return app.gulp
        .src(`${app.path.babelMetaSettings}`)
        .pipe(sourcemaps.init())
        .pipe(
            babel({
                presets: [["@babel/preset-env", { modules: false }]],
            })
        )
        .pipe(uglify())
        .pipe(sourcemaps.write("."))
        .pipe(app.gulp.dest(`${app.path.jsMain}`));
}

export function babelMain() {
    return app.gulp
        .src(`${app.path.babelMain}`)
        .pipe(sourcemaps.init())
        .pipe(
            babel({
                presets: [["@babel/preset-env", { modules: false }]],
            })
        )
        .pipe(uglify())
        .pipe(sourcemaps.write("."))
        .pipe(app.gulp.dest(`${app.path.jsMain}`));
}
