import * as nodePath from "path";
const rootFolder = nodePath.basename(nodePath.resolve());

const publicFolder = `./app/public`;

export const path = {
    projectLink: "cms-admin.loc",

    styles: {
        css: `${publicFolder}/css/`,
        pcss: `${publicFolder}/styles/style.pcss`,
        pcssAll: `${publicFolder}/styles/**/*.*`,
        tailwind: `${publicFolder}/styles/tailwind.pcss`,
        componentsSDAll: `${publicFolder}/styles/components/*.pcss`,
        adminStyles: `${publicFolder}/styles/components/admin-panel.pcss`,
        additionalStyles: `${publicFolder}/styles/components/additional_styles.pcss`,

        adminStylesDist: `${publicFolder}/css/backend`,

        modules: {
            folder: `${publicFolder}/styles/modules/*.pcss`,
            formbuilder: `${publicFolder}/styles/modules/formbuilder.pcss`,
            landings: `${publicFolder}/styles/modules/landings.pcss`,
            portal: `${publicFolder}/styles/modules/portal.pcss`,
            profile: `${publicFolder}/styles/modules/profile.pcss`,
            shop: `${publicFolder}/styles/modules/shop.pcss`,
            talent: `${publicFolder}/styles/modules/talent.pcss`,
        },
    },

    js: `${publicFolder}/js/**/*.js`,
    jsMain: `${publicFolder}/js/`,
    babelMain: `${publicFolder}/babel/main.js`,
    babelMetaSettings: `${publicFolder}/babel/meta-settings.js`,

    rootFolder: rootFolder,
    publicFolder: publicFolder,
};

export const reportFormatter = ({ messages, source }) =>
    messages
        .map(
            (message) =>
                message?.plugin &&
                `${"".padEnd(10)}⚠️  \x1b[43m[${
                    message?.plugin
                }]\x1b[49m: ${source}:${message?.line || ""}:${
                    message?.column || ""
                } \x1b[33m${message?.text}\x1b[39m \n`
        )
        .join("");
