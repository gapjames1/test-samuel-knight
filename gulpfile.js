import gulp from "gulp";
import { plugins } from "./config/gulp-plugins.js";
import { path } from "./config/gulp-settings.js";

global.app = {
    isBuild: process.argv.includes("--build"),
    isDev: !process.argv.includes("--build"),
    gulp: gulp,
    path: path,
    plugins: plugins,
};

import {
    pcss,
    additionalStyles,
    adminStyles,
    postcssMain,
    pcssStylelint,
    formBuilderPostCSS,
    landingsPostCSS,
    portalPostCSS,
    profilePostCSS,
    shopPostCSS,
    talentPostCSS,
} from "./config/gulp-tasks/pcss.js";
import { tailwind } from "./config/gulp-tasks/tailwind.js";
import { js } from "./config/gulp-tasks/js.js";
import { babelMain, babelMeta } from "./config/gulp-tasks/babel.js";

import { serve } from "./config/gulp-tasks/_serve.js";

const devTasks = gulp.series(
    additionalStyles,
    adminStyles,
    postcssMain,

    // Modules Compile
    formBuilderPostCSS,
    landingsPostCSS,
    portalPostCSS,
    profilePostCSS,
    shopPostCSS,
    talentPostCSS,

    tailwind,
    babelMeta,
    babelMain,
    serve
);

const dev = gulp.series(devTasks);

export { dev };
