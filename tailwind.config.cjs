/** @type {import('tailwindcss').Config} */
module.exports = {
    /**
     * FD content
     * **/
    // content: ["./src/**/*.{html,js}"],

    /**
     * SD content
     * **/
    content: ['./app/modules/**/*.php', './app/layout/*.php', './app/modules/!panel/**', './app/public/js/*.js'],
    theme: {
        extend: {
            colors: {
                transparent: "transparent",
                current: "currentColor",
                black: "#161616",
                "bg-second": "#343434",
                main: "#3cc5fb",
                white: "#ffffff",
                grey: "#a0a0a0",
                yellow: "#fdcc05",
                green: "#13c330",
                cyan: "#24ebcd",
                blue: "#0089C6",
                pink: "#ed145b",
                orange: "#FF8C00",
            },
        },

        screens: {
            "desk-xl": { min: "1921px", max: "2561px" },
            // => @media (min-width: 1921px and max-width: 2561px) { ... }

            "desk-lg": { min: "1819px" },
            // => @media (min-width: 1819px) { ... }

            "desk-md": { min: "1619px" },
            // => @media (min-width: 1619px) { ... }

            "desk-sm": { max: "1281px" },
            // => @media (max-width: 1279px) { ... }

            "tab": { max: "1025px" },
            // => @media (max-width: 1025px) { ... }

            "tab-md": { max: "901px" },
            // => @media (max-width: 901px) { ... }

            "tab-sm": { max: "769px" },
            // => @media (max-width: 769px) { ... }

            "mob-xl": { max: "651px" },
            // => @media (max-width: 651px) { ... }

            "mob-lg": { max: "551px" },
            // => @media (max-width: 551px) { ... }

            "mob": { max: "431px" },
            // => @media (max-width: 431px) { ... }

            "mob-sm": { max: "361px" },
            // => @media (max-width: 361px) { ... }
        },
    },
    corePlugins: {
        position: false,
    },
    plugins: [],
    important: true,
};
