<?php if (!Request::isAjax()) { ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php echo reFilter(Request::getParam('include_code_top')); // Top JS code ?>


    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="format-detection" content="telephone=no" />
		<meta
			name="viewport"
			content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=5"
		/>
    <title><?= Request::getTitle() ?> | <?= Request::getParam('title_prefix') ?></title>

    <?php if (Request::getKeywords()) { ?>
        <meta content="<?= Request::getKeywords() ?>" name="keywords">
    <?php } ?>
    <?php if (Request::getDescription()) { ?>
        <meta content="<?= Request::getDescription() ?>" name="description">
    <?php } ?>
    <?php if (Request::getCanonical() !== false) { ?>
        <link rel="canonical" href="<?= SITE_URL . Request::getCanonical() ?>"/>
    <?php } ?>

    <?php
    if (CONTROLLER == 'jobs' && ACTION == 'view')
        $ogImage = Request::getParam('og_job');
    else if (CONTROLLER == 'blogs' && ACTION == 'view')
        $ogImage = Request::getParam('og_blog');
    else
        $ogImage = Request::getParam('og_image');

    ?>
    <meta property="og:title" content="<?= Request::getTitle() ?>">
    <meta property="og:url" content="<?= SITE_URL . trim(_URI_,'/') ?>">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="<?= SITE_NAME ?>">
    <meta property="og:description" content="<?= Request::getDescription() ?>">
    <meta property="og:image" content="<?= Request::getImageOG() ?: SITE_URL . 'app/data/og/' . $ogImage ?>">

    <!-- Twitter open-graph tags -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="<?= Request::getTitle(); ?>">
    <meta name="twitter:description" content="<?= Request::getDescription(); ?>">
    <meta name="twitter:image" content="<?= Request::getImageOG() ?: SITE_URL . 'app/data/og/' . $ogImage ?>">

    <!-- Favicon -->
    <link href="<?= _SITEDIR_ ?>data/setting/<?= Request::getParam('favicon'); ?>" rel="shortcut icon" />

    <!-- Connect CSS files -->
    <!-- normalize -->
    <link href="<?= _SITEDIR_ ?>public/css/plugins/normalize.css" type="text/css" rel="stylesheet" />

    <!--Jquery UI-->
    <link
            href="<?= _SITEDIR_ ?>public/css/plugins/jquery-ui.min.css"
            type="text/css"
            rel="stylesheet"
    />

    <!--Swiper-->
    <link
            href="<?= _SITEDIR_ ?>public/css/plugins/swiper-bundle.min.css"
            type="text/css"
            rel="stylesheet"
    />

    <link
            href="<?= _SITEDIR_ ?>public/plugins/select2.min.css"
            type="text/css"
            rel="stylesheet"
    />

    <link href="<?= _SITEDIR_ ?>public/plugins/aos.min.css" type="text/css" rel="stylesheet" />
    <link href="<?= _SITEDIR_ ?>public/plugins/plyr.min.css" type="text/css" rel="stylesheet" />
    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.css"
    />
    <link href="<?= _SITEDIR_ ?>public/tailwind.css" type="text/css" rel="stylesheet" />
    <link href="<?= _SITEDIR_ ?>public/css/style.css?(cache)" type="text/css" rel="stylesheet" />
    <link href="<?= _SITEDIR_ ?>public/css/additional_styles.css" type="text/css" rel="stylesheet" />
    <link href="<?= _SITEDIR_ ?>public/plugins/notification/snackbar/snackbar.min.css" rel="stylesheet" type="text/css" />


    <!-- Connect JS files -->
    <script>var site_url = '<?= SITE_URL ?>';</script>
    <script>var site_dir = '<?= _SITEDIR_ ?>';</script>

    <script src="<?= _SITEDIR_ ?>public/js/plugins/jquery.min.js" type="text/javascript"></script>

    <script src="<?= _SITEDIR_ ?>public/js/backend/function.js?(cache)"></script>
    <script src="<?= _SITEDIR_ ?>public/js/backend/event.js?(cache)"></script>

    <!--Scroll Lock-->
    <script src="<?= _SITEDIR_ ?>public/js/scroll-lock.min.js"></script>
</head>

<body>
<?php   $class = '';
if (CONTROLLER ==  'page' && ACTION == 'index'){
    $class = 'home-page';
}elseif (CONTROLLER == 'jobs' && ACTION == 'index'){
    $class = 'job-search';
}elseif (CONTROLLER == 'news' && ACTION == 'index'){
    $class = 'news-page';
}elseif (CONTROLLER == 'news' && ACTION == 'view'){
    $class = 'news-post';
}elseif (CONTROLLER == 'jobs' && ACTION == 'view'){
    $class = 'job-post-page';
}elseif (CONTROLLER == 'page' && ACTION == 'candidates'){
    $class = 'candidates-page';
}elseif (CONTROLLER == 'page' && ACTION == 'terms'){
    $class = 'cookie-terms-page';
}elseif (CONTROLLER == 'page' && ACTION == 'cookies'){
    $class = 'cookie-terms-page';
}

?>
<div id="site" class="<?= $class ?>">
    <!-- POPUPS -->
    <div id="popup"></div>
    <div id="notice"></div>
    <!-- /POPUPS -->

    <!-- Header & Menu -->
    <header class="header">
        <div class="cont">
            <a href="{URL:home}" class="logo">
                <img src="<?= _SITEDIR_ ?>public/images/logo.svg" alt="" />
            </a>

            <a href="{URL:contact-us}" class="btn-contact"> Contact Us </a>
        </div>
    </header>
    <aside class="main-menu">
        <div class="navbar">
            <nav class="navbar-nav nav-slide-left">
                <ul class="menu">
                    <li class="menu-item">
                        <a class="menu-link" href="./index.html">Home</a>
                    </li>
                    <li class="menu-item dropdown">
                        <div class="dropdown-menu-link-wr">
                            <a class="menu-link" href="./about-us.html">Who We Are</a>
                            <span class="plus"></span>
                        </div>

                        <ul class="dropdown-menu">
                            <li><a class="menu-link" href="/">Become a Knight</a></li>
                            <li><a class="menu-link" href="/">Meet The Team</a></li>
                        </ul>
                    </li>
                    <li class="menu-item">
                        <a class="menu-link" href="./insights.html">Clients</a>
                    </li>
                    <li class="menu-item">
                        <a class="menu-link" href="./insights.html">Candidates</a>
                    </li>
                    <li class="menu-item dropdown">
                        <div class="dropdown-menu-link-wr">
                            <a class="menu-link" href="./about-us.html">Sectors</a>
                            <span class="plus"></span>
                        </div>
                        <ul class="dropdown-menu">
                            <li><a class="menu-link" href="/">Energy</a></li>
                            <li><a class="menu-link" href="/">Projects</a></li>
                            <li><a class="menu-link" href="/">Tech</a></li>
                            <li><a class="menu-link" href="/">Rail</a></li>
                        </ul>
                    </li>
                    <li class="menu-item">
                        <a class="menu-link" href="./client-hub.html">Media Hub</a>
                    </li>
                    <li class="menu-item">
                        <a class="menu-link" href="./contact-us.html"
                        >Current Opportunities</a
                        >
                    </li>
                </ul>
            </nav>
        </div>
        <div class="menu-toggle">
            <span></span>
        </div>
    </aside>

    <div class="header-close-wrapper"></div>
    <!-- /Header & Menu -->

    <main class="main" id="content">
        <?php } ?>

        <?php
        echo $this->Load('contentPart'); // Content from view page
        ?>

        <?php if (!Request::isAjax()) { ?>
    </main>

    <!--  loader  -->
    <div
            id="global-loader"
    >
        <div
                role="status"
        >
            <svg aria-hidden="true" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
            </svg>
            <span>Loading...</span>
        </div>
    </div>
    <!--  end loader  -->

    <!-- Footer -->
    <footer class="footer">
        <div class="cont">
            <div class="footer-line"></div>
            <div class="footer-navbar">
                <nav>
                    <div class="footer-social-box">
                        <h4>Follow us on social</h4>
                        <ul class="social-links">
                            <li>
                                <a href="#" class="footer-link">
                                    <i class="icon-linkedin"></i>
                                    <span>LinkedIn</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="footer-link">
                                    <i class="icon-twitter"></i>
                                    <span>Twitter</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="footer-link">
                                    <i class="icon-instagram"></i>
                                    <span>Instagram</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-locations">
                        <h4>Locations</h4>
                        <ul class="footer-locations-list">
                            <li><a href="#" class="footer-link">Head Office, UK</a></li>
                            <li><a href="#" class="footer-link">Boston, USA</a></li>
                            <li><a href="#" class="footer-link">London, UK</a></li>
                            <li><a href="#" class="footer-link">Dallas, USA</a></li>
                            <li><a href="#" class="footer-link">Bristol, UK</a></li>
                            <li><a href="#" class="footer-link">Madrid, Spain</a></li>
                            <li><a href="#" class="footer-link">Chicago, USA</a></li>
                        </ul>
                    </div>
                </nav>
                <ul class="policy-box">
                    <li>
                        <h4><a href="#">Privacy Policy</a></h4>
                    </li>
                    <li>
                        <h4><a href="#">Modern Slavery</a></h4>
                    </li>
                    <li>
                        <h4><a href="#">T&Cs Permanent Staff</a></h4>
                    </li>
                    <li>
                        <h4><a href="#">Client Agreement</a></h4>
                    </li>
                </ul>
            </div>
            <div class="footer-copyright">
                <p>© <?php date('Y') ?> Samuel Knight International. All rights reserved.</p>
                <a
                        href="https://www.boldidentities.com/"
                        target="_blank"
                        id="bold-credits"
                        class="my-4"
                >
                    <img src="<?= _SITEDIR_ ?>public/images/bold-logos/BOLD-logo-white.gif" alt="" />
                </a>
            </div>
        </div>
    </footer>

    <!-- /FOOTER -->

    <?php if (Request::getParam('tracker') == 'yes') { ?>
        <!-- Bug Tracker -->
        <div id="api_content"></div>

        <div class="scan__block">
            <a class="report__button" onclick="load('issue_manager/create_task', 'project=<?= Request::getParam('tracker_api') ?>', 'url=' + window.location.href);">Report<br>an issue</a>
            <?php /*<a class="report__button" onclick="load('https://donemen.com/api/create_task', 'project=<?= CONF_PROJECT ?>', 'url=' + window.location.href);">Report<br>an issue</a>*/?>
        </div>
        <!-- /Bug Tracker -->
    <?php } ?>
</div>

<?php echo reFilter(Request::getParam('include_code_bottom')); // Bottom JS code ?>

<script src="<?= _SITEDIR_ ?>public/plugins/notification/snackbar/snackbar.min.js"></script>

<script src="<?= _SITEDIR_ ?>public/js/plugins/lazysizes.min.js" type="text/javascript"></script>

<script src="<?= _SITEDIR_ ?>public/js/plugins/plyr.js" type="text/javascript"></script>
<!--<script src="./js/plugins/jquery-ui.min.js"></script>-->
<script src="<?= _SITEDIR_ ?>public/js/plugins/jquery.ui.touch-punch.min.js"></script>
<script
        src="https://kit.fontawesome.com/aaa56c7348.js"
        crossorigin="anonymous"
></script>

<!--Scroll Lock-->
<script src="<?= _SITEDIR_ ?>public/js/plugins/scroll-lock.min.js"></script>

<script src="<?= _SITEDIR_ ?>public/js/plugins/select2-min.js"></script>

<!--Position Sticky with js-->
<script src="<?= _SITEDIR_ ?>public/js/plugins/stickyfill.min.js"></script>

<!-- Container Query Polyfill -->
<script src="<?= _SITEDIR_ ?>public/js/plugins/cq-prolyfill.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.min.js"></script>
<script src="<?= _SITEDIR_ ?>public/js/plugins/autoRoundBorders.js"></script>
<script src="<?= _SITEDIR_ ?>public/js/plugins/aos.js"></script>
<script type="module" src="<?= _SITEDIR_ ?>public/js/main.js?(cache)"></script>
</body>
</html>
<?php } ?>
