<?php

class About_usController extends Controller
{
    use Validator;

    public function indexAction()
    {
        Model::import('panel/stories');
        $this->view->stories = StoriesModel::getByPage('about');

        Model::import('news');
        $this->view->blogs = NewsModel::search(false,false,false,false,5);

        Model::import('jobs');
        $this->view->jobs = JobsModel::search(false,false,false,false,false,false,false,5);

        Request::setTitle('About Us');
    }


    public function contact_usAction()
    {
        Request::setTitle('Contact');
    }

    public function meet_the_teamAction()
    {
        Model::import('panel/team');
        $this->view->team = TeamModel::getAllUsers(" `display_team` = 'yes' ORDER BY `sort`");

        $this->view->team_senior = TeamModel::getAllUsers(" `display_team` = 'yes' AND `display_senior` = 'yes' ORDER BY `sort`");


        Request::setTitle('Contact');
    }

}
