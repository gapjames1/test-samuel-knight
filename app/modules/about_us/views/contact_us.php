<section class="hero-section-contact">
    <div class="cont">
        <div class="hero-section-caption-wr">
            <div class="hero-section-caption">
                <h1
                        class="hero-section-title title-marker clip-title"
                        data-aos="clip-down"
                >
                    <span>Contact</span> <br />
                    <span>Us.</span>
                </h1>
            </div>
        </div>
        <div class="hero-sector-icons hero-sector-icons-contact">
            <div
                    class="sector-icon bg-blue"
                    data-sector="energy"
                    data-aos="fade-left"
            >
                <img
                        alt="hero-sector"
                        src="<?= _SITEDIR_ ?>public/images/hero-sector-icons/hero-sector-energy.svg"
                />
                <div class="hover-text">
                    <a href="mailto:energy@samuel-knight.com"
                    >energy@samuel-knight.com</a
                    >
                    <a href="tel:01234 567 890">01234 567 890</a>
                </div>
            </div>
            <div
                    class="sector-icon bg-gray"
                    data-sector="projects"
                    data-aos="fade-left"
            >
                <img
                        alt="hero-sector"
                        src="<?= _SITEDIR_ ?>public/images/hero-sector-icons/hero-sector-projects.svg"
                />
                <div class="hover-text">
                    <a href="mailto:energy@samuel-knight.com"
                    >energy@samuel-knight.com</a
                    >
                    <a href="tel:01234 567 890">01234 567 890</a>
                </div>
            </div>
            <div
                    class="sector-icon bg-green"
                    data-sector="tech"
                    data-aos="fade-left"
            >
                <img
                        alt="hero-sector"
                        src="<?= _SITEDIR_ ?>public/images/hero-sector-icons/hero-sector-tech.svg"
                />
                <div class="hover-text">
                    <a href="mailto:energy@samuel-knight.com"
                    >energy@samuel-knight.com</a
                    >
                    <a href="tel:01234 567 890">01234 567 890</a>
                </div>
            </div>
            <div
                    class="sector-icon bg-orange"
                    data-sector="rail"
                    data-aos="fade-left"
            >
                <img
                        alt="hero-sector"
                        src="<?= _SITEDIR_ ?>public/images/hero-sector-icons/hero-sector-rail.svg"
                />
                <div class="hover-text">
                    <a href="mailto:energy@samuel-knight.com"
                    >energy@samuel-knight.com</a
                    >
                    <a href="tel:01234 567 890">01234 567 890</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="contact-map-wr">
    <div class="contact-map">
        <img src="<?= _SITEDIR_ ?>public/images/mission-map.svg" alt="map" />
        <div class="map-pin-items">
            <div class="map-pin-item">
                <div class="map-pin"></div>
                <div class="map-popup">
                    <h3
                            class="title-marker title-marker-center title-marker-small"
                    >
                        London, UK
                    </h3>
                    <a href="tel:+442037635796" class="tel-link"
                    >+44 (203) 763 5796</a
                    >
                    <a href="#" class="address-link">
                        Samuel Knight International Ltd Thomas House 84 Ecclestone
                        Square London SW1V 1PX
                    </a>
                </div>
            </div>
            <div class="map-pin-item">
                <div class="map-pin"></div>
                <div class="map-popup">
                    <h3
                            class="title-marker title-marker-center title-marker-small"
                    >
                        London, UK
                    </h3>
                    <a href="tel:+442037635796" class="tel-link"
                    >+44 (203) 763 5796</a
                    >
                    <a href="#" class="address-link">
                        Samuel Knight International Ltd Thomas House 84 Ecclestone
                        Square London SW1V 1PX
                    </a>
                </div>
            </div>
            <div class="map-pin-item">
                <div class="map-pin"></div>
                <div class="map-popup">
                    <h3
                            class="title-marker title-marker-center title-marker-small"
                    >
                        London, UK
                    </h3>
                    <a href="tel:+442037635796" class="tel-link"
                    >+44 (203) 763 5796</a
                    >
                    <a href="#" class="address-link">
                        Samuel Knight International Ltd Thomas House 84 Ecclestone
                        Square London SW1V 1PX
                    </a>
                </div>
            </div>
            <div class="map-pin-item">
                <div class="map-pin"></div>
                <div class="map-popup">
                    <h3
                            class="title-marker title-marker-center title-marker-small"
                    >
                        London, UK
                    </h3>
                    <a href="tel:+442037635796" class="tel-link"
                    >+44 (203) 763 5796</a
                    >
                    <a href="#" class="address-link">
                        Samuel Knight International Ltd Thomas House 84 Ecclestone
                        Square London SW1V 1PX
                    </a>
                </div>
            </div>
            <div class="map-pin-item">
                <div class="map-pin"></div>
                <div class="map-popup">
                    <h3
                            class="title-marker title-marker-center title-marker-small"
                    >
                        London, UK
                    </h3>
                    <a href="tel:+442037635796" class="tel-link"
                    >+44 (203) 763 5796</a
                    >
                    <a href="#" class="address-link">
                        Samuel Knight International Ltd Thomas House 84 Ecclestone
                        Square London SW1V 1PX
                    </a>
                </div>
            </div>
            <div class="map-pin-item">
                <div class="map-pin"></div>
                <div class="map-popup">
                    <h3
                            class="title-marker title-marker-center title-marker-small"
                    >
                        London, UK
                    </h3>
                    <a href="tel:+442037635796" class="tel-link"
                    >+44 (203) 763 5796</a
                    >
                    <a href="#" class="address-link">
                        Samuel Knight International Ltd Thomas House 84 Ecclestone
                        Square London SW1V 1PX
                    </a>
                </div>
            </div>
            <div class="map-pin-item">
                <div class="map-pin"></div>
                <div class="map-popup">
                    <h3
                            class="title-marker title-marker-center title-marker-small"
                    >
                        London, UK
                    </h3>
                    <a href="tel:+442037635796" class="tel-link"
                    >+44 (203) 763 5796</a
                    >
                    <a href="#" class="address-link">
                        Samuel Knight International Ltd Thomas House 84 Ecclestone
                        Square London SW1V 1PX
                    </a>
                </div>
            </div>
            <div class="map-pin-item">
                <div class="map-pin"></div>
                <div class="map-popup">
                    <h3
                            class="title-marker title-marker-center title-marker-small"
                    >
                        London, UK
                    </h3>
                    <a href="tel:+442037635796" class="tel-link"
                    >+44 (203) 763 5796</a
                    >
                    <a href="#" class="address-link">
                        Samuel Knight International Ltd Thomas House 84 Ecclestone
                        Square London SW1V 1PX
                    </a>
                </div>
            </div>
            <div class="map-pin-item">
                <div class="map-pin"></div>
                <div class="map-popup">
                    <h3
                            class="title-marker title-marker-center title-marker-small"
                    >
                        London, UK
                    </h3>
                    <a href="tel:+442037635796" class="tel-link"
                    >+44 (203) 763 5796</a
                    >
                    <a href="#" class="address-link">
                        Samuel Knight International Ltd Thomas House 84 Ecclestone
                        Square London SW1V 1PX
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="contact-text-section">
    <div class="cont">
        <h3 class="title-marker title">Lorem ipsum dolor sit amet</h3>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a
            fermentum nulla, et rhoncus risus. Phasellus mattis augue id
            turpis ultrices, ac malesuada nibh pellentesque. Nunc porta mauris
            arcu, vel pretium orci porttitor vel. Fusce congue sit amet odio
            auctor facilisis. Aliquam elementum turpis at leo auctor lobortis.
            Suspendisse ut enim quis enim vestibulum iaculis. Nulla eget metus
            at enim pharetra rutrum sit amet id lorem.
        </p>
    </div>
</section>
<section class="drop-cv-section">
    <div class="cont">
        <div class="drop-form-wr">
            <h2 class="title title-marker">Drop us your CV</h2>
            <div class="drop-cv-form-motto text-gray">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
                    ultrices et ante ut ullamcorper. Suspendisse id ullamcorper
                    felis, in ultricies libero.
                </p>
            </div>
            <form action="#" class="drop-cv-form">
                <div class="input-wr">
                    <input name="name" placeholder="Your name" type="text" />
                </div>
                <div class="input-wr">
                    <input name="emal" placeholder="Your email" type="email" />
                </div>
                <div class="select-wr">
                    <select class="select2-item hidden" data-placeholder="Sector">
                        <option></option>
                        <option value="Energy">Energy</option>
                        <option value="Rail">Rail</option>
                        <option value="Projects">Projects</option>
                        <option value="Tech">Tech</option>
                    </select>
                </div>
                <div class="upload-btn">
                    <label>
                        <input
                                class="hidden"
                                id="cv_field"
                                name="cv_field"
                                type="hidden"
                                value="<?= post('cv_field', false, User::get('cv')); ?>"
                        />
                        <input
                                class="hidden"
                                accept=".doc, .docx, .txt, .pdf, .fotd"
                                class="pf-text-field"
                                name="cv_field"
                                onchange="initFile(this); load('cv/upload/', 'field=#cv_field', 'preview=.cv_file_name');"
                                type="file"
                        />
                        <span class="upload-status">Upload File</span>
                    </label>
                </div>
                <button class="submit-btn" type="submit">
                    <i class="icon-arrow-right"></i>
                </button>
            </form>
        </div>
    </div>
</section>
<div class="dive-section">
    <div class="cont">
        <h2
                class="title title-marker title-marker-center text-center clip-title"
                data-aos="clip-down"
        >
            Ready to dive in?
        </h2>
        <a href="maito:hello@samuel-knight.com" data-aos="fade-up"
        >hello@samuel-knight.com</a
        >
    </div>
</div>