<section class="hero-section">
    <div class="hero-section-bg sector-hero-section-bg">
        <div class="hero-l-gradient"></div>
        <div class="hero-black-layer mx-color"></div>
        <div class="hero-r-gradient"></div>
        <picture>
            <source
                    data-srcset="<?= _SITEDIR_ ?>public/images/hero-about-bg.webp"
                    type="image/webp"
            />
            <img
                    alt=""
                    class="lazyload"
                    data-src="<?= _SITEDIR_ ?>public/images/hero-about-bg.jpg"
            />
        </picture>
    </div>
    <div class="cont">
        <div class="hero-section-caption-wr">
            <div class="hero-section-caption">
                <h1
                        class="hero-section-title title-marker clip-title"
                        data-aos="clip-down"
                >
                    <span>About Us.</span>
                </h1>
                <div
                        class="hero-section-motto"
                        data-aos="fade-up"
                        data-aos-delay="300"
                >
                    <p>We are Samuel Knight</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="vision-section">
    <div class="cont">
        <div class="vision-wr">
            <div class="video-wr" data-aos="fade-left">
                <video
                        preload="auto"
                        class="plyr-video-item"
                        loop="loop"
                        muted=""
                        playsinline="playsinline"
                        data-poster="<?= _SITEDIR_ ?>public/images/video-poster.jpg"
                        poster="<?= _SITEDIR_ ?>public/images/video-poster.jpg"
                >
                    <source src="<?= _SITEDIR_ ?>public/images/video/video-2.mp4" type="video/mp4" />
                </video>
            </div>
            <div class="vision-content-wr">
                <h2 class="title title-marker clip-title" data-aos="clip-down">
                    Vision
                </h2>
                <div class="vision-content">
                    <div class="vision-content-caption" data-aos="fade-up">
                        <p class="text-gray">
                            Becoming the recognised best company to work for worldwide
                            which is 100% owned by all employees.
                        </p>
                    </div>
                    <ul class="vision-content-list">
                        <li data-aos="fade-up">
                            <h3>People</h3>
                            <div class="marker-line"></div>
                            <p>People-centric</p>
                        </li>
                        <li data-aos="fade-up">
                            <h3>Proud</h3>
                            <div class="marker-line"></div>
                            <p>Proud of our values</p>
                        </li>
                        <li data-aos="fade-up">
                            <h3>Purpose</h3>
                            <div class="marker-line"></div>
                            <p>Dedicates to our purpose</p>
                        </li>
                        <li data-aos="fade-up">
                            <h3>Passionate</h3>
                            <div class="marker-line"></div>
                            <p>Passionate about change</p>
                        </li>
                        <li data-aos="fade-up">
                            <h3>Planet</h3>
                            <div class="marker-line"></div>
                            <p>Creating a better tomorrow for our planet</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="ab-cards-section">
    <div class="cont">
        <div class="ab-swiper-wr">
            <div class="ab-swiper swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide" data-aos="fade-up">
                        <div class="ab-card">
                            <div class="ab-card-bg">
                                <div class="bg-layer"></div>
                                <picture>
                                    <source
                                            data-srcset="<?= _SITEDIR_ ?>public/images/ab-card-bg.webp"
                                            type="image/webp"
                                    />
                                    <img
                                            alt=""
                                            class="lazyload"
                                            data-src="<?= _SITEDIR_ ?>public/images/ab-card-bg.jpg"
                                    />
                                </picture>
                            </div>
                            <h2>Purpose</h2>
                            <h3 class="title-marker">Why we exist</h3>
                            <div data-simplebar class="ab-card-text">
                                <p>
                                    Creating a greener tomorrow today by working with our
                                    global community to achieve zero carbon emissions by
                                    2050.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide" data-aos="fade-up">
                        <div class="ab-card">
                            <div class="ab-card-bg">
                                <div class="bg-layer"></div>
                                <picture>
                                    <source
                                            data-srcset="<?= _SITEDIR_ ?>public/images/ab-card-bg.webp"
                                            type="image/webp"
                                    />
                                    <img
                                            alt=""
                                            class="lazyload"
                                            data-src="<?= _SITEDIR_ ?>public/images/ab-card-bg.jpg"
                                    />
                                </picture>
                            </div>
                            <h2>Mission</h2>
                            <h3 class="title-marker">How we’re going to get there</h3>
                            <div data-simplebar class="ab-card-text">
                                <p>
                                    Continuously investing in our employees, creating
                                    excellent career opportunities and having an open door
                                    to proposition whilst creating a belonging and winning
                                    culture.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="progress-section">
    <div class="cont">
        <div class="progress-swiper-wr">
            <div class="progress-swiper-line"></div>
            <div class="progress-swiper swiper">
                <div class="swiper-wrapper">
                    <?php if($this->stories) { ?>
                        <?php foreach($this->stories as $story) { ?>
                            <div data-aos="zoom-in" class="swiper-slide">
                                <div class="progress-card">
                                    <h3 class="progress-card-year"><?= $story->year ?></h3>
                                    <div class="progress-card-point"></div>
                                    <div class="progress-card-text"><?= $story->title ?></div>
                                    <div class="progress-card-popup-wr">
                                        <div class="progress-card-popup">
                                            <div class="progress-card-popup-container">
                                                <div class="progress-card-text">
                                                    <h3><?= $story->year ?></h3>
                                                    <?= reFilter($story->content) ?>
                                                </div>
                                                <a href="{URL:about-us}" class="btn"
                                                ><span>Learn more</span><span>Learn more</span></a
                                                >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="swiper-pagination"></div>
        <div
                class="progress-card-popup-wr hidden"
                id="progress-card-mobile-popup"
        ></div>
    </div>
</section>
<section class="journey-section journey-section-about-page">
    <div class="cont">
        <h2
                class="title title-marker text-center title-marker-center clip-title"
                data-aos="clip-down"
        >
            <span>SKI Life</span>
        </h2>
        <div class="journey-list grid grid-cols-5">
            <div
                    class="col-span-3 max-md:col-span-5 grid grid-cols-3 col-first"
            >
                <div data-aos="zoom-in" class="journey-list-item">
                    <picture>
                        <source
                                data-srcset="<?= _SITEDIR_ ?>public/images/journey/j-1.webp"
                                type="image/webp"
                        />
                        <img
                                alt=""
                                class="lazyload"
                                data-src="<?= _SITEDIR_ ?>public/images/journey/j-1.png"
                        />
                    </picture>
                </div>
                <div data-aos="zoom-in" class="journey-list-item">
                    <picture>
                        <source
                                data-srcset="<?= _SITEDIR_ ?>public/images/journey/j-2.webp"
                                type="image/webp"
                        />
                        <img
                                alt=""
                                class="lazyload"
                                data-src="<?= _SITEDIR_ ?>public/images/journey/j-2.png"
                        />
                    </picture>
                </div>
                <div data-aos="zoom-in" class="journey-list-item">
                    <picture>
                        <source
                                data-srcset="<?= _SITEDIR_ ?>public/images/journey/j-5.webp"
                                type="image/webp"
                        />
                        <img
                                alt=""
                                class="lazyload"
                                data-src="<?= _SITEDIR_ ?>public/images/journey/j-5.png"
                        />
                    </picture>
                </div>
                <div data-aos="zoom-in" class="journey-list-item">
                    <picture>
                        <source
                                data-srcset="<?= _SITEDIR_ ?>public/images/journey/j-6.webp"
                                type="image/webp"
                        />
                        <img
                                alt=""
                                class="lazyload"
                                data-src="<?= _SITEDIR_ ?>public/images/journey/j-6.png"
                        />
                    </picture>
                </div>
            </div>
            <div
                    class="col-span-2 max-md:col-span-5 grid grid-cols-2 col-last"
            >
                <div data-aos="zoom-in" class="journey-list-item">
                    <picture>
                        <source
                                data-srcset="<?= _SITEDIR_ ?>public/images/journey/j-3.webp"
                                type="image/webp"
                        />
                        <img
                                alt=""
                                class="lazyload"
                                data-src="<?= _SITEDIR_ ?>public/images/journey/j-3.png"
                        />
                    </picture>
                </div>
                <div data-aos="zoom-in" class="journey-list-item">
                    <picture>
                        <source
                                data-srcset="<?= _SITEDIR_ ?>public/images/journey/j-4.webp"
                                type="image/webp"
                        />
                        <img
                                alt=""
                                class="lazyload"
                                data-src="<?= _SITEDIR_ ?>public/images/journey/j-4.png"
                        />
                    </picture>
                </div>
                <div data-aos="zoom-in" class="journey-list-item">
                    <picture>
                        <source
                                data-srcset="<?= _SITEDIR_ ?>public/images/journey/j-7.webp"
                                type="image/webp"
                        />
                        <img
                                alt=""
                                class="lazyload"
                                data-src="<?= _SITEDIR_ ?>public/images/journey/j-7.png"
                        />
                    </picture>
                </div>
            </div>
        </div>
        <div class="journey-text">
            <div class="journey-text-caption" data-aos="fade-right">
                <h3 class="journey-text-title">
                    Our culture is fun, fearless and entrepreneurial.
                </h3>
                <a href="#" class="btn active-icon">
                    <span>Call to action</span>
                    <span>Call to action</span>
                </a>
            </div>

            <div class="journey-text-description-wr" data-aos="fade-left">
                <div class="journey-text-description">
                    <p>
                        Our culture is fun, fearless and entrepreneurial with a
                        shared passion for delivering the very best provision in
                        global manpower solutions – we value results, encourage
                        teamwork and embrace change
                    </p>
                    <p>
                        With a focus on individual needs, we provide personalised
                        work out and nutrition plans through our Company PT,
                        ensuring a healthy and balanced lifestyle. In addition to
                        competitive salaries and performance bonuses, employees can
                        look forward to regular incentives trips away and engaging
                        social events, fostering a vibrant and supportive team
                        culture.
                    </p>
                    <p>
                        We also encourage giving back to the community through
                        volunteering and sponsored charity events, making a positive
                        impact beyond the workplace.
                    </p>
                    <p>
                        There is a robust pension scheme is in place to secure the
                        future, and flexible working arrangements promote work-life
                        balance.
                    </p>
                    <p>
                        Moreover, we value the well-being of our employees by
                        incorporating dedicated well-being days into the work
                        schedule, prioritising mental and physical health.
                    </p>

                    <a href="#" class="btn active-icon">
                        <span>Call to action</span>
                        <span>Call to action</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="latest-job-section">
    <div class="cont">
        <div class="title-wr flex justify-between items-center">
            <h2 class="title title-marker clip-title" data-aos="clip-down">
                <span>Current Jobs</span>
            </h2>
            <a class="btn" href="#"
            ><span>All Jobs</span><span>All Jobs</span></a
            >
        </div>
        <div class="latest-job-swiper-wr">
            <div class="latest-job-swiper swiper">
                <div class="swiper-wrapper">
                    <?php include_once _SYSDIR_ .'modules/page/system/inc/_jobs.php'; ?>
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</section>
<section class="become-section">
    <div class="cont">
        <div class="become-tabs-body">
            <ul class="become-tabs">
                <li class="become-tabs-item active">
                    <div class="border-bottom"></div>
                    <div class="title-wr">
                        <h2
                                class="title title-marker clip-title"
                                data-aos="clip-down"
                        >
                            <span>Become a Knight</span>
                        </h2>
                    </div>
                    <div class="content-hidden hidden">
                        <div class="become-tabs-item-thumbnail-wr">
                            <div class="become-tabs-item-thumbnail">
                                <picture>
                                    <source
                                            data-srcset="<?= _SITEDIR_ ?>public/images/become-1.webp"
                                            type="image/webp"
                                    />
                                    <img
                                            alt=""
                                            class="lazyload"
                                            data-src="<?= _SITEDIR_ ?>public/images/become-1.jpg"
                                    />
                                </picture>
                            </div>
                        </div>
                        <div class="content-hidden-text">
                            <p>
                                Samuel Knight International are one of the largest
                                global manpower providers for the energy, rail and tech
                                sectors, and to continue our growth and success we
                                require talented individuals to help our clients achieve
                                their aims.
                            </p>
                            <p>
                                As global recruitment experts in renewable energy we’re
                                building an industry that impacts millions. We provide
                                leading recruitment services to our clients and
                                candidates that they won’t find elsewhere. Our team are
                                experts in what they do, working closely with clients to
                                identify, source and recruit worldwide talent ensuring
                                long-lasting, successful business relationships.
                            </p>
                        </div>
                    </div>
                </li>
                <li class="become-tabs-item">
                    <div class="title-wr">
                        <h2
                                class="title title-marker clip-title"
                                data-aos="clip-down"
                        >
                            <span>Development in Knighthood</span>
                        </h2>
                    </div>
                    <div class="content-hidden">
                        <div class="become-tabs-item-thumbnail-wr">
                            <div class="become-tabs-item-thumbnail">
                                <div class="mix-layer"></div>
                                <picture>
                                    <source
                                            data-srcset="<?= _SITEDIR_ ?>public/images/become-2.webp"
                                            type="image/webp"
                                    />
                                    <img
                                            alt=""
                                            class="lazyload"
                                            data-src="<?= _SITEDIR_ ?>public/images/become-2.jpg"
                                    />
                                </picture>
                            </div>
                            <div class="become-tabs-item-thumbnail-text-wr">
                                <div class="become-tabs-item-thumbnail-text">
                                    <div class="ski-story">SKI Success Story</div>
                                    <h3>Lorem ipsum dolor sit amet</h3>
                                    <p>
                                        Cras imperdiet sollicitudin magna, vel condimentum
                                        magna accumsan a. Sed purus risus, bibendum nec leo
                                        sit amet, faucibus malesuada odio. Curabitur in odio
                                        vehicula, consequat magna eu, consectetur lorem.
                                        Nullam congue est sit amet ipsum pharetra venenatis.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="content-hidden-text">
                            <p>
                                At Samuel Knight, we want to empower our people to be
                                the best, achieve their dreams and create a fantastic,
                                rewarding career through determination, hard work,
                                professional integrity and diligence to deliver
                                exceptional client and candidate service.
                            </p>
                            <p>
                                When you join SK, you will automatically enrol in a
                                structured training program, tailored to your needs -
                                with regular workshops, one-to-one training sessions and
                                more, to ensure you understand what it takes to be
                                successful here at SK and become a knight!
                            </p>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="become-tabs-content-rt" data-aos="fade-left">
                <div class="become-tabs-content-rt-html">
                    <div class="become-tabs-item-thumbnail">
                        <picture>
                            <source
                                    data-srcset="<?= _SITEDIR_ ?>public/images/become-1.webp"
                                    type="image/webp"
                            />
                            <img
                                    alt=""
                                    class="lazyload"
                                    data-src="<?= _SITEDIR_ ?>public/images/become-1.jpg"
                            />
                        </picture>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="diversity">
    <div class="cont">
        <div class="diversity-wr">
            <div class="diversity-thumbnail">
                <picture>
                    <source
                            data-srcset="<?= _SITEDIR_ ?>public/images/diversity.webp"
                            type="image/webp"
                    />
                    <img
                            alt=""
                            class="lazyload"
                            data-src="<?= _SITEDIR_ ?>public/images/diversity.jpg"
                    />
                </picture>
            </div>
            <div class="diversity-text">
                <h2 class="title title-marker clip-title" data-aos="clip-down">
                    <span>Diversity, equity & inclusion</span>
                </h2>
                <div
                        class="diversity-thumbnail diversity-thumbnail-text"
                        data-aos="fade-right"
                >
                    <picture>
                        <source
                                data-srcset="<?= _SITEDIR_ ?>public/images/diversity.webp"
                                type="image/webp"
                        />
                        <img
                                alt=""
                                class="lazyload"
                                data-src="<?= _SITEDIR_ ?>public/images/diversity.jpg"
                        />
                    </picture>
                </div>
                <div class="" data-aos="fade-up">
                    <p>
                        Samuel Knight International is committed to championing
                        diversity and inclusion in the workplace. We firmly believe
                        that embracing a diverse workforce strengthens our
                        organization and drives innovation.
                    </p>
                    <p>
                        We foster an inclusive culture that respects and appreciates
                        the unique perspectives and experiences of our employees and
                        strive to create an environment where everyone feels valued,
                        heard, and empowered to contribute their best work.
                    </p>
                    <p>
                        By nurturing an inclusive workplace, we enable our employees
                        to thrive personally and professionally.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="csr">
    <div class="cont">
        <h2 class="title title-marker clip-title" data-aos="clip-down">
            <span>CSR</span>
        </h2>
        <div class="csr-content">
            <div class="csr-caption">
                <div class="csr-caption-text text-gray" data-aos="fade-up">
                    <p>
                        At Samuel Knight International, we firmly believe in the
                        power of Corporate Social Responsibility (CSR) and its
                        significance in the workplace. We understand that as a
                        global organization, we have a responsibility to contribute
                        positively to society and the environment.
                    </p>
                </div>
                <a href="#" class="btn active-icon"
                ><span>Read More</span><span>Read More</span></a
                >
            </div>
            <div class="csr-motto" data-aos="fade-left">
                <p>
                    At Samuel Knight International, we view CSR as an integral
                    part of our identity,
                    <b>driving us to create a better future</b> for our employees,
                    communities, and the planet as a whole.
                </p>
            </div>
        </div>
    </div>
    <div class="csr-video">
        <div class="section-bg csr-bg">
            <picture>
                <source
                        data-srcset="<?= _SITEDIR_ ?>public/images/jt-section.webp"
                        type="image/webp"
                />
                <img
                        alt=""
                        class="lazyload"
                        data-src="<?= _SITEDIR_ ?>public/images/jt-section.png"
                />
            </picture>
        </div>
        <div class="cont">
            <div class="video-wr" data-aos="fade-up">
                <video
                        preload="auto"
                        class="plyr-video-item"
                        loop="loop"
                        muted=""
                        playsinline="playsinline"
                        data-poster="<?= _SITEDIR_ ?>public/images/video-poster.jpg"
                        poster="<?= _SITEDIR_ ?>public/images/video-poster.jpg"
                >
                    <source src="<?= _SITEDIR_ ?>public/images/video/video-2.mp4" type="video/mp4" />
                </video>
            </div>
        </div>
    </div>
</section>
<section class="latest-news-section latest-news-section-about">
    <div class="section-bg latest-section-bg">
        <picture>
            <source
                    data-srcset="<?= _SITEDIR_ ?>public/images/sectors-mission.webp"
                    type="image/webp"
            />
            <img
                    alt=""
                    class="lazyload"
                    data-src="<?= _SITEDIR_ ?>public/images/sectors-mission.jpg"
            />
        </picture>
    </div>
    <div class="cont">
        <div class="latest-news-section-caption">
            <h2
                    class="title-marker title-marker-small title clip-title"
                    data-aos="clip-down"
            >
                <span>Giving Back</span>
            </h2>
            <div
                    class="latest-news-section-motto text-gray"
                    data-aos="fade-up"
            >
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Aliquam a fermentum nulla, et rhoncus risus. Phasellus mattis
                    augue id turpis ultrices, ac malesuada nibh pellentesque. Nunc
                    porta mauris arcu, vel pretium orci porttitor vel. Fusce
                    congue sit amet odio auctor facilisis. Aliquam elementum
                    turpis at leo auctor lobortis. Suspendisse ut enim quis enim
                    vestibulum iaculis. Nulla eget metus at enim pharetra rutrum
                    sit amet id lorem.
                </p>
            </div>
        </div>

        <div class="news-swiper-wr">
            <div class="news-swiper swiper">
                <div class="swiper-wrapper">
                    <?php include_once _SYSDIR_ .'modules/page/system/inc/_blogs.php'; ?>
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</section>