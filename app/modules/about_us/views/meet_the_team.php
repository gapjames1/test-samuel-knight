<section class="hero-section">
    <div class="hero-section-bg sector-hero-section-bg">
        <div class="hero-l-gradient"></div>
        <div class="hero-black-layer mx-color"></div>
        <div class="hero-r-gradient"></div>
        <picture>
            <source
                    data-srcset="<?= _SITEDIR_ ?>public/images/hero-meet-the-team.webp"
                    type="image/webp"
            />
            <img
                    alt=""
                    class="lazyload"
                    data-src="<?= _SITEDIR_ ?>public/images/hero-meet-the-team.jpg"
            />
        </picture>
    </div>
    <div class="cont">
        <div class="hero-section-caption-wr">
            <div class="hero-section-caption">
                <h1
                        class="hero-section-title title-marker clip-title"
                        data-aos="clip-down"
                >
                    <span>Meet the</span> <br />
                    <span>Team</span> <br />
                </h1>
            </div>
        </div>
    </div>
</section>
<?php if ($this->team_senior) {?>
<section class="teammate-section">
    <div class="cont">
        <h2 class="title title-marker">Senior Management</h2>
        <div class="teammate-grid">
            <?php foreach ($this->team_senior as $senior) { ?>
            <div class="teammate-card">
                <div class="teammate-card-text">
                    <h3><?= $senior->firstname . ' ' . $senior->lastname?></h3>
                    <div class="teammate-card-info">
                        <p><?= $senior->job_title ?></p>
                        <a href="#"><i class="icon-linkedin"></i></a>
                    </div>
                    <div class="text-gray teammate-card-info-descr">
                        <?= reFilter($senior->description) ?>
                    </div>
                </div>
                <div class="teammate-card-thumbnail">
                    <picture>
                        <source
                                data-srcset="<?= _SITEDIR_ ?>data/users/mini_<?= getFileName($senior->image) . '.webp' ?>"
                                type="image/webp"
                        />
                        <img
                                class="lazyload"
                                data-src="<?= _SITEDIR_ ?>data/users/<?= $senior->image ?>"
                                alt="<?= $senior->firstname . ' ' . $senior->lastname ?>"
                        />
                    </picture>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
<?php } ?>
<?php if ($this->team) {?>
<section class="team-mb-section">
    <div class="cont">
        <h3 class="title">Team Members</h3>
        <div class="team-mb-swiper-wr">
            <div class="team-mb-swiper swiper">
                <div class="swiper-wrapper">
                    <?php foreach ($this->team as $user) { ?>
                    <div class="swiper-slide">
                        <div class="team-mb-card">
                            <div class="team-mb-card-text">
                                <h3 class="text-gray"><?= $user->firstname . ' ' . $user->lastname ?></h3>
                                <h4><?= $user->job_title ?></h4>
                                <div class="links-wr">
                                    <a href="#" class="linkedin-link"
                                    ><i class="icon-linkedin"></i
                                        ></a>
                                    <a href="#" class="btn">
                                        <span>Read More</span>
                                        <span>Read More</span>
                                    </a>
                                </div>
                                <div class="team-mb-card-text-hidden text-gray">
                                    <?= reFilter($user->description); ?>
                                </div>
                            </div>
                            <div class="team-mb-card-thumbnail-wr">
                                <div class="team-mb-card-thumbnail">
                                    <picture>
                                        <source
                                                data-srcset="<?= _SITEDIR_ ?>data/users/mini_<?= getFileName($user->image) . '.webp' ?>"
                                                type="image/webp"
                                        />
                                        <img
                                                class="lazyload"
                                                data-src="<?= _SITEDIR_ ?>data/users/<?= $user->image ?>"
                                                alt="<?= $user->firstname . ' ' . $user->lastname ?>"
                                        />
                                    </picture>
                                </div>
                                <div class="sectors-icon">
                                    <img
                                            src="<?= _SITEDIR_ ?>data/sectors/<?= $user->sector->image ?>"
                                            alt="sectors-icon"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</section>
<?php } ?>

<div class="dive-section">
    <div class="cont">
        <h2
                class="title title-marker title-marker-center text-center clip-title"
                data-aos="clip-down"
        >
            Ready to dive in?
        </h2>
        <a href="maito:hello@samuel-knight.com" data-aos="fade-up"
        >hello@samuel-knight.com</a
        >
    </div>
</div>