<?php
class   NewsModel extends Model
{
    public $version = 0;

    /**
     * Method module_install start automatically if it not exist in `modules` table at first importing of model
     */
    public function module_install()
    {
        $queries = array();

        foreach ($queries as $query)
            self::query($query);
    }

    /**
     * Method module_update start automatically if current $version != version in `modules` table, and start from "case 'i'", where i = prev version in modules` table
     * @param int $version
     */
    public function module_update($version)
    {
        $queries = array();

        switch ($version) {

        }

        foreach ($queries as $query)
            self::query($query);
    }

    public static function getPrevNews($id)
    {
        $sql = "
            SELECT *
            FROM `blog`
            WHERE `id` < '$id' AND `deleted` = 'no' AND `posted` = 'yes'
            ORDER BY `id` DESC
            LIMIT 1
        ";

        return self::fetch(self::query($sql));
    }

    public static function getNewsByID($where)
    {
        $sql = "
            SELECT *
            FROM `blog`
            WHERE `deleted` = 'no'
        ";

        if ($where)
            $sql .= " AND $where ";


        $blog = self::fetchAll(self::query($sql));

        if ($blog)
            $blog = Model::relationship($blog, 'blog', 'sectors', '*', false, 'sector_id', 'one_to_many');

        return $blog;
    }

    public static function getImage($id)
    {
        $sql = "
            SELECT image FROM `blog`
        ";

        return self::fetch(self::query($sql));
    }
    /**
     * Get all
     * @return array
     */
    public static function getAll($start = false, $end = false)
    {
        $sql = "
            SELECT *
            FROM `blog`
            WHERE `deleted` = 'no' AND `posted` = 'yes' AND `time` < " . time() . "
            ORDER BY `time` DESC
        ";

        if ($start !== false) {
            $sql .= " LIMIT $start";

            if ($end !== false)
                $sql .= ", $end";
        }

        $blogs = self::fetchAll(self::query($sql));
        $blogs = Model::relationship($blogs, 'blog', 'sectors', '*', false, 'sector_id', 'one_to_many');

        if ($blogs)
            $blogs = Image::getAltsForMany('blog', $blogs);
        return $blogs;
    }

    public static function getBySlug($slug)
    {
        $sql = "SELECT * 
            FROM `blog` 
            WHERE `deleted` = 'no' 
              AND `slug` = '$slug'
            LIMIT 1
        ";

        $blog = self::fetch(self::query($sql));
        $blog = Model::relationship($blog, 'blog', 'sectors', '*', false, 'sector_id', 'one_to_many');

        return $blog;
    }

    public static function search($keywords = false, $sector = false, $start = false, $end = false, $limit = false)
    {
        $sql = "
            SELECT *
            FROM `blog`
            WHERE `deleted` = 'no' AND `posted` = 'yes'";

        if ($keywords)
            $sql .= " AND (`title` LIKE '%$keywords%' OR `content` LIKE '%$keywords%')";

//        if ($type) {
//            if (is_array($type)) // by array of types
//                $sql .= " AND `vacancies`.`contract_type` IN ('" . implode("','", $type) . "')";
//            else // by once type
//                $sql .= " AND `vacancies`.`contract_type` = '$type'";
//        }

        $sql .= " ORDER BY `time` DESC";

        if ($start) {
            $sql .= " LIMIT $start";
            if ($end) {
                $sql .= ", $end";
            }
        }

        if ($limit !== false)
            $sql .= " LIMIT $limit ";

        $blogs = self::fetchAll(self::query($sql));

        $blogs = self::relationship($blogs, 'vacancies', 'sectors', ['id', 'name'], $sector,
            false, 'one_to_many');

        return $blogs;


    }

}

/* End of file */