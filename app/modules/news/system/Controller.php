<?php

class NewsController extends Controller
{
    private static $elemOnPage = 1;
    public function indexAction()
    {
        $this->view->heronews = NewsModel::search(false,false,false,false,1);

        Model::import('panel/vacancies/sectors');
        $this->view->sectors = SectorsModel::getAll();

        $keywords = get('keywords');

        $blogs = NewsModel::search($keywords);
        $this->view->count = $countNews = count($blogs);
        Pagination::calculate(post('page'), self::$elemOnPage, $countNews);

        $this->view->list = array_slice($blogs, Pagination::$start, Pagination::$end);
    }

    public function viewAction()
    {
        $slug = Request::getUri(0);
        $this->view->blog = NewsModel::getBySlug($slug);

        if (!$this->view->blog || (!User::get('id') && $this->view->blog->posted == 'no'))
            redirect(url('news'));

        if (setViewStat('blog_', $this->view->blog->id, 'blogs_analytics')) {
            $data_views['views'] = '++';
            Model::update('blog', $data_views, "`id` = '" . $this->view->blog->id . "'");
        }
        Request::setTitle('Blog - ' . ($this->view->blog->meta_title ?: $this->view->blog->title));
        Request::setDescription($this->view->blog->meta_desc);
        Request::setKeywords($this->view->blog->meta_keywords);
    }

    public function searchAction()
    {
        Request::ajaxPart();
        $keywords   = post('keywords');
        $sector     = post('sector');

        if ($sector && is_string($sector)) {
            $sector = explode(',', $sector);
        }

        $blogs = NewsModel::search($keywords, $sector);
        $countNews = count($blogs);
        Pagination::calculate(post('page'), self::$elemOnPage, $countNews);

        $this->view->list = array_slice($blogs, Pagination::$start, Pagination::$end);

        $post = allPost();
        if (is_array($sector)) {
            $post['sector'] = implode(',', $sector);
        }



        Request::addResponse('html', '#pagination', $countNews > self::$elemOnPage ? Pagination::ajax('news/search', $post) : '');
        Request::addResponse('html', '#search_results', $this->getView());
//        Request::addResponse('func', 'scrollToEl', '#search_results|500');
        Request::endAjax();
    }

}
