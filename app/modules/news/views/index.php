<?php if ($this->heronews) { ?>
        <?php foreach ($this->heronews as $heronew) {?>
<section class="hero-section">
    <div class="hero-section-bg sector-hero-section-bg">
        <div class="hero-l-gradient"></div>
        <div class="hero-black-layer mx-color"></div>
        <div class="hero-r-gradient"></div>
        <picture>
            <source
                    data-srcset="<?= _SITEDIR_ ?>data/blog/mini_<?= getFileName($heronew->image) . '.webp' ?>"
                    type="image/webp"
            />
            <img
                    alt=""
                    class="lazyload"
                    data-src="<?= _SITEDIR_ ?>data/blog/<?= $heronew->image ?>"
            />
        </picture>
    </div>
    <div class="cont">
        <div class="hero-section-caption-wr">
            <div class="hero-section-caption">
                <h1
                        class="hero-section-title title-marker clip-title"
                        data-aos="clip-down"
                >
                    <span>News.</span>
                </h1>

                <div class="hero-section-col" data-aos="fade-left">
                    <p>Featured</p>
                    <p class="hero-section-date">
                        <span> <?= date('M d, Y', $heronew->time) ?> </span>
                        <span> <?= $heronew->sector->name ?> </span>
                    </p>
                    <p class="hero-section-descr">
                        <?= $heronew->title ?>
                    </p>
                    <a href="{URL:news-post/<?= $heronew->slug ?>}" class="btn active-icon">
                        <span>Read More</span>
                        <span>Read More</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="sector-img-hover">
        <div class="opacity-0" data-sector="energy">
            <picture>
                <source
                        data-srcset="<?= _SITEDIR_ ?>public/images/hero-sectors-hover/1.webp"
                        type="image/webp"
                />
                <img
                        alt=""
                        class="lazyload"
                        data-src="<?= _SITEDIR_ ?>public/images/hero-sectors-hover/1.jpg"
                />
            </picture>
        </div>
        <div class="opacity-0" data-sector="rail">
            <picture>
                <source
                        data-srcset="<?= _SITEDIR_ ?>public/images/hero-sectors-hover/2.webp"
                        type="image/webp"
                />
                <img
                        alt=""
                        class="lazyload"
                        data-src="<?= _SITEDIR_ ?>public/images/hero-sectors-hover/2.jpg"
                />
            </picture>
        </div>
        <div class="opacity-0" data-sector="tech">
            <picture>
                <source
                        data-srcset="<?= _SITEDIR_ ?>public/images/hero-sectors-hover/3.webp"
                        type="image/webp"
                />
                <img
                        alt=""
                        class="lazyload"
                        data-src="<?= _SITEDIR_ ?>public/images/hero-sectors-hover/3.jpg"
                />
            </picture>
        </div>
        <div class="opacity-0" data-sector="projects">
            <picture>
                <source
                        data-srcset="<?= _SITEDIR_ ?>public/images/hero-sectors-hover/4.webp"
                        type="image/webp"
                />
                <img
                        alt=""
                        class="lazyload"
                        data-src="<?= _SITEDIR_ ?>public/images/hero-sectors-hover/4.jpg"
                />
            </picture>
        </div>
    </div>
</section>
    <?php } ?>
<?php } ?>

<section
        class="hero-form-section"
        data-aos="fade-up"
        id="hero-form-section"
>
    <div class="cont">
        <div class="hero-form-wr">
            <form class="hero-form" id="search_form" method="post">
                <div class="input-wr">
                    <input
                            id="keywords"
                            name="keywords"
                            placeholder="Enter keyword"
                            type="text"
                    />
                </div>
                <div class="select-wr">
                    <select class="select2-item hidden" data-placeholder="Sector" name="sector">
                        <option></option>
                        <?php if ($this->sectors) { ?>
                        <?php foreach ($this->sectors as $sector) { ?>
                        <option value="<?= $sector->id ?>"><?= $sector->name ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <button class="submit-btn" onclick="load('news/search', 'form:#search_form'); return false" type="submit">
                    <i class="icon-arrow-right"></i>
                </button>
            </form>
        </div>
    </div>
</section>

<section class="drop-cv-section" data-aos="fade-up">
    <div class="cont">
        <div class="drop-form-wr">
            <h3 class="title title-marker">Sign up to our newsletter</h3>
            <form action="#" class="drop-cv-form">
                <div class="input-wr">
                    <input name="name" placeholder="Name" type="text" />
                </div>
                <div class="input-wr">
                    <input name="emal" placeholder="Email" type="email" />
                </div>
                <button class="submit-btn" type="submit">
                    <i class="icon-arrow-right"></i>
                </button>
            </form>
        </div>
    </div>
</section>

<section class="news-blog" data-aos="fade-up">
    <div class="cont flex flex-col">
        <div class="news-blog__sort relative">
            <span>Sort by</span>
            <div class="select-wr">
                <select
                        class="select2-item hidden"
                        data-placeholder="Most Recent"
                >
                    <option></option>
                    <option value="10">Most Recent</option>
                    <option value="15">Most Recent</option>
                    <option value="25">Most Recent</option>
                    <option value="30">Most Recent</option>
                </select>
            </div>
        </div>
        <div class="news-blog__wrapper" id="search_results">
            <?php include_once _SYSDIR_ .'modules/news/views/search.php'; ?>
        </div>

        <div class="pagination" id="pagination">
                <?= Pagination::ajax('news/search') ?>
        </div>
    </div>
</section>