<?php if($this->blog) { ?>
<section class="hero-section sector-hero-section">
    <div class="hero-section-bg sector-hero-section-bg">
        <div class="hero-l-gradient"></div>
        <div class="hero-black-layer mx-color"></div>
        <div class="hero-r-gradient"></div>
        <picture>
            <source
                    data-srcset="<?= _SITEDIR_ ?>data/blog/mini_<?= getFileName($this->blog->image) . '.webp' ?>"
                    type="image/webp"
            />
            <img
                    alt=""
                    class="lazyload"
                    data-src="<?= _SITEDIR_ ?>data/blog/<?= $this->blog->image ?>"
            />
        </picture>
    </div>
    <div class="cont">
        <div class="hero-section-caption-wr">
            <div class="hero-section-caption">
                <div
                        class="hero-section-motto"
                        data-aos="fade-down"
                        data-aos-delay="300"
                >
                    <span><?= date('M d, Y', $this->blog->time) ?></span>
                    <span><?= $this->blog->sector->name ?></span>
                </div>
                <h1
                        class="hero-section-title title-marker clip-title"
                        data-aos="clip-down"
                >
                    <span><?= $this->blog->title ?></span>
                </h1>
            </div>
        </div>
    </div>
</section>
<?php } ?>
<?php if($this->blog) { ?>
<section class="news-text">
    <div class="cont">
        <div class="news-text__wrapper max-w-[836px]">
            <?= reFilter($this->blog->content) ?>
        </div>

        <div class="news-text__bottom">
            <div class="footer-line"></div>
            <a href="#" class="footer-link">
                <i class="icon-linkedin"></i>
                <span>Share on LinkedIn</span>
            </a>
            <a href="#" class="footer-link">
                <i class="icon-twitter"></i>
                <span>Share on Twitter</span>
            </a>
            <a href="#" class="footer-link">
                <i class="icon-instagram"></i>
                <span>Share via Email</span>
            </a>
        </div>
    </div>
</section>
<?php } ?>