<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="format-detection" content="telephone=no" />
		<meta
			name="viewport"
			content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=5"
		/>
		<link
			rel="icon"
			type="image/x-icon"
			href="<?= _SITEDIR_ ?>public/images/favicons/favicon-32x32.png"
		/>
		<title>404 page not found</title>

		<!-- normalize -->
		<link href="<?= _SITEDIR_ ?>public/css/plugins/normalize.css" type="text/css" rel="stylesheet" />

		<!--Jquery UI-->
		<link
			href="<?= _SITEDIR_ ?>public/css/plugins/jquery-ui.min.css"
			type="text/css"
			rel="stylesheet"
		/>

		<!--Swiper-->
		<link
			href="<?= _SITEDIR_ ?>public/css/plugins/swiper-bundle.min.css"
			type="text/css"
			rel="stylesheet"
		/>

		<link
			href="<?= _SITEDIR_ ?>public/css/plugins/select2.min.css"
			type="text/css"
			rel="stylesheet"
		/>

		<link href="<?= _SITEDIR_ ?>public/css/plugins/aos.min.css" type="text/css" rel="stylesheet" />
		<link href="<?= _SITEDIR_ ?>public/css/plugins/plyr.min.css" type="text/css" rel="stylesheet" />
		<link
			rel="stylesheet"
			href="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.css"
		/>
		<link href="<?= _SITEDIR_ ?>public/css/tailwind.css" type="text/css" rel="stylesheet" />

		<link href="<?= _SITEDIR_ ?>public/css/style.css" type="text/css" rel="stylesheet" />
	</head>

	<body>
		<main class="single-page-404">
			<h1>Page not found</h1>
			<p>The page may have changed the address or never existed</p>
			<a class="btn" href="./">
				<span>Home page</span>
				<span>Home page</span>
			</a>
		</main>
	</body>
</html>
