<section class="hero-section">
    <div class="hero-section-bg">
        <div class="lg-hidden">
            <picture>
                <source
                    data-srcset="<?= _SITEDIR_ ?>public/images/hero-home-bg.webp"
                    type="image/webp"
                />
                <img
                    alt=""
                    class="lazyload"
                    data-src="<?= _SITEDIR_ ?>public/images/hero-home-bg.jpg"
                />
            </picture>
        </div>
        <div class="mb-hidden">
            <video
                preload="auto"
                autoplay
                loop="loop"
                muted=""
                playsinline="playsinline"
            >
                <source
                    src="<?= _SITEDIR_ ?>public/images/video/video.webm#t=0.1"
                    type="video/webm"
                />
                <source src="<?= _SITEDIR_ ?>public/images/video/video.mp4#t=0.1" type="video/mp4" />
            </video>
        </div>
    </div>
    <div class="cont">
        <div class="hero-section-caption-wr">
            <div class="hero-section-caption">
                <h1
                    class="hero-section-title title-marker clip-title"
                    data-aos="clip-down"
                >
                    <span>Renewable</span> <br />
                    <span>Energy</span> <br />
                    <span>Solutions.</span>
                </h1>
                <div
                    class="hero-section-motto"
                    data-aos="fade-up"
                    data-aos-delay="300"
                >
                    <p>We are Samuel Knight</p>
                </div>
            </div>
            <div class="hero-sector-icons hero-sector-icons-home">
                <a
                    href="#"
                    class="sector-icon"
                    data-sector="energy"
                    data-aos="fade-left"
                >
                    <img
                        alt="hero-sector"
                        src="<?= _SITEDIR_ ?>public/images/hero-sector-icons/hero-sector-energy.svg"
                    /></a>
                <a
                    href="#"
                    class="sector-icon"
                    data-sector="projects"
                    data-aos="fade-left"
                >
                    <img
                        alt="hero-secror"
                        src="<?= _SITEDIR_ ?>public/images/hero-sector-icons/hero-sector-projects.svg"
                    />
                </a>
                <a
                    href="#"
                    class="sector-icon"
                    data-sector="tech"
                    data-aos="fade-left"
                ><img
                        alt="hero-secror"
                        src="<?= _SITEDIR_ ?>public/images/hero-sector-icons/hero-sector-tech.svg"
                    /></a>
                <a
                    href="#"
                    class="sector-icon"
                    data-sector="rail"
                    data-aos="fade-left"
                ><img
                        alt="hero-sector"
                        src="<?= _SITEDIR_ ?>public/images/hero-sector-icons/hero-sector-rail.svg"
                    /></a>
            </div>
        </div>
        <a class="scroll-down-label" href="#hero-form-section">
            <img alt="scroll-label" src="<?= _SITEDIR_ ?>public/images/scroll-label.svg" />
            <i class="icon-arrow-down-long"></i>
        </a>
    </div>
    <div class="sector-img-hover">
        <div class="opacity-0" data-sector="energy">
            <picture>
                <source
                    data-srcset="<?= _SITEDIR_ ?>public/images/hero-sectors-hover/1.webp"
                    type="image/webp"
                />
                <img
                    alt=""
                    class="lazyload"
                    data-src="<?= _SITEDIR_ ?>public/images/hero-sectors-hover/1.jpg"
                />
            </picture>
        </div>
        <div class="opacity-0" data-sector="rail">
            <picture>
                <source
                    data-srcset="<?= _SITEDIR_ ?>public/images/hero-sectors-hover/2.webp"
                    type="image/webp"
                />
                <img
                    alt=""
                    class="lazyload"
                    data-src="<?= _SITEDIR_ ?>public/images/hero-sectors-hover/2.jpg"
                />
            </picture>
        </div>
        <div class="opacity-0" data-sector="tech">
            <picture>
                <source
                    data-srcset="<?= _SITEDIR_ ?>public/images/hero-sectors-hover/3.webp"
                    type="image/webp"
                />
                <img
                    alt=""
                    class="lazyload"
                    data-src="<?= _SITEDIR_ ?>public/images/hero-sectors-hover/3.jpg"
                />
            </picture>
        </div>
        <div class="opacity-0" data-sector="projects">
            <picture>
                <source
                    data-srcset="<?= _SITEDIR_ ?>public/images/hero-sectors-hover/4.webp"
                    type="image/webp"
                />
                <img
                    alt=""
                    class="lazyload"
                    data-src="<?= _SITEDIR_ ?>public/images/hero-sectors-hover/4.jpg"
                />
            </picture>
        </div>
    </div>
</section>
<section
    class="hero-form-section"
    data-aos="fade-up"
    id="hero-form-section"
>
    <div class="cont">
        <div class="hero-form-wr">
            <div class="hero-form-title">Job Search</div>
            <form action="#" class="hero-form" method="post">
                <div class="input-wr">
                    <input
                        name="keywords"
                        placeholder="Enter keyword e.g engineer"
                        type="text"
                    />
                </div>
                <div class="select-wr">
                    <select
                        class="select2-item hidden"
                        data-placeholder="Select location"
                    >
                        <option></option>
                        <option value="10">Location item</option>
                        <option value="15">Location item</option>
                        <option value="25">Location item</option>
                        <option value="30">Location item</option>
                    </select>
                </div>
                <div class="select-wr">
                    <select class="select2-item hidden" data-placeholder="Sector">
                        <option></option>
                        <option value="10">Sector item</option>
                        <option value="15">Sector item</option>
                        <option value="25">Sector item</option>
                        <option value="30">Sector item</option>
                    </select>
                </div>
                <button class="submit-btn" type="submit">
                    <i class="icon-arrow-right"></i>
                </button>
            </form>
        </div>
    </div>
</section>
<section class="mission-section">
    <div class="cont">
        <div class="mission-section-motto" data-aos="fade-up">
            <h3>
                On a mission to help build a greener and more environmental
                friendly world by working with our global community to achieve
                Zero Carbon Emissions by 2050.
            </h3>
            <a class="btn" href="#"
            ><span>Learn more</span><span>Learn more</span></a
            >
        </div>
        <div class="mission-swiper-wr">
            <div class="mission-swiper-bg">
                <img alt="mission-map" src="<?= _SITEDIR_ ?>public/images/mission-map.svg" />
            </div>
            <div class="mission-swiper swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="mission-card" data-aos="fade-down">
                            <h3 class="mission-card-title title-marker">
                                Global recruitment specialists in the Renewable Energy,
                                Tech and Rail industries.
                            </h3>
                            <div class="mission-card-text">
                                <p>
                                    With Samuel Knight Projects we also provide a full
                                    service offering for your renewable energy project.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="mission-card" data-aos="fade-down">
                            <h3 class="mission-card-title title-marker">
                                Placing the best talent <br />
                                in the market, not <br />
                                necessarily on the market.
                            </h3>
                            <div class="mission-card-text">
                                <p>
                                    While we have a global talent pool of active
                                    candidates, we also have access to a large selection
                                    of passive candidates in our unique, market leading
                                    technology platform.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="mission-card" data-aos="fade-down">
                            <h3 class="mission-card-title title-marker">
                                We’re a true partner to both our clients and candidates.
                            </h3>
                            <div class="mission-card-text">
                                <p>
                                    By providing one point of contact, whether that’s a
                                    dedicated consultant or an account manager for
                                    large-scale projects, we understand the importance of
                                    quality delivery.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="stats-section">
    <div class="cont">
        <h3 class="title" data-aos="fade-down">
            Our stats speak for themselves
        </h3>
        <div class="stats-grid">
            <?php if ($this->counters) { ?>
            <?php foreach ($this->counters as $counter) { ?>
            <div class="stats-card" data-aos="zoom-in">
                <div class="stats-card-counter" data-suffix="<?= $counter->suffix ?>"><?= $counter->numbers ?></div>
                <div class="stats-card-text"><?= reFilter($counter->content) ?></div>
            </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</section>
<?php if ($this->jobs) { ?>
<section class="latest-job-section">
    <div class="cont">
        <div class="title-wr flex justify-between items-center">
            <h2 class="title title-marker clip-title" data-aos="clip-down">
                <span>Latest Jobs.</span>
            </h2>
            <a class="btn" href="#"
            ><span>All Jobs</span><span>All Jobs</span></a
            >
        </div>
        <div class="latest-job-swiper-wr">
            <div class="latest-job-swiper swiper">
                <div class="swiper-wrapper">
                    <?php include_once _SYSDIR_ .'modules/page/system/inc/_jobs.php'; ?>
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</section>
<?php } ?>

<section class="sectors-section">
    <div class="cont">
        <h2
            class="title title-marker title-marker-center text-center clip-title"
            data-aos="clip-down"
        >
            <span>Our Sectors</span>
        </h2>
        <div class="sectors-list">
            <div class="sector-card-wr">
                <div data-aos="fade-up" class="sector-card sector-energy">
                    <div class="sector-card-content">
                        <h3 class="sector-card-title">Energy</h3>
                        <img
                            class="sector-card-icon"
                            src="<?= _SITEDIR_ ?>public/images/hero-sector-icons/hero-sector-energy.svg"
                        />
                        <div class="sector-card-content-hidden">
                            <div class="sc-hidden-wr">
                                <h3 class="sector-card-content-hidden-title">Energy</h3>
                                <div class="sector-card-content-hidden-description">
                                    <p>
                                        SK Energy works with Utilities, IPPs, EPCs,
                                        Developers, OEMs and Renewable Technology providers
                                        throughout the UK & USA. For the last 8 years Samuel
                                        Knight has helped clients secure the best possible
                                        talent for their Renewable Energy projects both
                                        contract and permanent staffing solutions.
                                    </p>
                                </div>
                                <a class="btn" href="#"
                                ><span>Learn more</span><span>Learn more</span></a
                                >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sector-card-wr">
                <div data-aos="fade-up" class="sector-card sector-rail">
                    <div class="sector-card-content">
                        <h3 class="sector-card-title">Rail</h3>
                        <img
                            class="sector-card-icon"
                            src="<?= _SITEDIR_ ?>public/images/hero-sector-icons/hero-sector-rail.svg"
                        />
                        <div class="sector-card-content-hidden">
                            <div class="sc-hidden-wr">
                                <h3 class="sector-card-content-hidden-title">Rail</h3>
                                <div class="sector-card-content-hidden-description">
                                    <p>
                                        SK Energy works with Utilities, IPPs, EPCs,
                                        Developers, OEMs and Renewable Technology providers
                                        throughout the UK & USA. For the last 8 years Samuel
                                        Knight has helped clients secure the best possible
                                        talent for their Renewable Energy projects both
                                        contract and permanent staffing solutions.
                                    </p>
                                </div>
                                <a class="btn" href="#"
                                ><span>Learn more</span><span>Learn more</span></a
                                >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sector-card-wr">
                <div data-aos="fade-up" class="sector-card sector-tech">
                    <div class="sector-card-content">
                        <h3 class="sector-card-title">Tech</h3>
                        <img
                            class="sector-card-icon"
                            src="<?= _SITEDIR_ ?>public/images/hero-sector-icons/hero-sector-tech.svg"
                        />
                        <div class="sector-card-content-hidden">
                            <div class="sc-hidden-wr">
                                <h3 class="sector-card-content-hidden-title">Tech</h3>
                                <div class="sector-card-content-hidden-description">
                                    <p>
                                        SK Energy works with Utilities, IPPs, EPCs,
                                        Developers, OEMs and Renewable Technology providers
                                        throughout the UK & USA. For the last 8 years Samuel
                                        Knight has helped clients secure the best possible
                                        talent for their Renewable Energy projects both
                                        contract and permanent staffing solutions.
                                    </p>
                                </div>
                                <a class="btn" href="#"
                                ><span>Learn more</span><span>Learn more</span></a
                                >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sector-card-wr">
                <div data-aos="fade-up" class="sector-card sector-projects">
                    <div class="sector-card-content">
                        <h3 class="sector-card-title">Projects</h3>
                        <img
                            class="sector-card-icon"
                            src="<?= _SITEDIR_ ?>public/images/hero-sector-icons/hero-sector-projects.svg"
                        />
                        <div class="sector-card-content-hidden">
                            <div class="sc-hidden-wr">
                                <h3 class="sector-card-content-hidden-title">
                                    Projects
                                </h3>
                                <div class="sector-card-content-hidden-description">
                                    <p>
                                        SK Energy works with Utilities, IPPs, EPCs,
                                        Developers, OEMs and Renewable Technology providers
                                        throughout the UK & USA. For the last 8 years Samuel
                                        Knight has helped clients secure the best possible
                                        talent for their Renewable Energy projects both
                                        contract and permanent staffing solutions.
                                    </p>
                                </div>
                                <a class="btn" href="#"
                                ><span>Learn more</span><span>Learn more</span></a
                                >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="progress-section">
    <div class="cont">
        <div class="progress-swiper-wr">
            <div class="progress-swiper-line"></div>
            <div class="progress-swiper swiper">
                <div class="swiper-wrapper">
                    <?php if($this->stories) { ?>
                            <?php foreach($this->stories as $story) { ?>
                     <div data-aos="zoom-in" class="swiper-slide">
                        <div class="progress-card">
                            <h3 class="progress-card-year"><?= $story->year ?></h3>
                            <div class="progress-card-point"></div>
                            <div class="progress-card-text"><?= $story->title ?></div>
                            <div class="progress-card-popup-wr">
                                <div class="progress-card-popup">
                                    <div class="progress-card-popup-container">
                                        <div class="progress-card-text">
                                            <h3><?= $story->year ?></h3>
                                            <?= reFilter($story->content) ?>
                                        </div>
                                        <a href="{URL:about-us}" class="btn"
                                        ><span>Learn more</span><span>Learn more</span></a
                                        >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="swiper-pagination"></div>
        <div
            class="progress-card-popup-wr hidden"
            id="progress-card-mobile-popup"
        ></div>
    </div>
</section>
<section class="latest-news-section">
    <div class="cont">
        <div class="title-wr">
            <h2 class="title title-marker clip-title" data-aos="clip-down">
                <span>The latest</span>
            </h2>
            <a href="#" class="btn"
            ><span>All News</span><span>All News</span></a
            >
        </div>
        <div class="news-swiper-wr">
            <div class="news-swiper swiper">
                <div class="swiper-wrapper">
                    <?php include_once _SYSDIR_ .'modules/page/system/inc/_blogs.php'; ?>
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</section>
<section class="partners-section">
    <div class="cont">
        <h3 class="title">Members of</h3>
        <div class="partners-list" id="flex-elements-parent">
            <?php if ($this->partners) { ?>
                <?php foreach ($this->partners as $partner) { ?>
                    <div data-aos="fade-up" class="partners-card flex-element">
                        <img src="<?= _SITEDIR_ ?>data/partners/<?=$partner->image?>" />
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</section>
<section class="journey-section">
    <div class="cont">
        <h2
            class="title title-marker text-center title-marker-center clip-title"
            data-aos="clip-down"
        >
            <span>Join the Journey</span>
        </h2>
        <div class="journey-list grid grid-cols-5">
            <div
                class="col-span-3 max-md:col-span-5 grid grid-cols-3 col-first"
            >
                <div data-aos="zoom-in" class="journey-list-item">
                    <picture>
                        <source
                            data-srcset="<?= _SITEDIR_ ?>public/images/journey/j-1.webp"
                            type="image/webp"
                        />
                        <img
                            alt=""
                            class="lazyload"
                            data-src="<?= _SITEDIR_ ?>public/images/journey/j-1.png"
                        />
                    </picture>
                </div>
                <div data-aos="zoom-in" class="journey-list-item">
                    <picture>
                        <source
                            data-srcset="<?= _SITEDIR_ ?>public/images/journey/j-2.webp"
                            type="image/webp"
                        />
                        <img
                            alt=""
                            class="lazyload"
                            data-src="<?= _SITEDIR_ ?>public/images/journey/j-2.png"
                        />
                    </picture>
                </div>
                <div data-aos="zoom-in" class="journey-list-item">
                    <picture>
                        <source
                            data-srcset="<?= _SITEDIR_ ?>public/images/journey/j-5.webp"
                            type="image/webp"
                        />
                        <img
                            alt=""
                            class="lazyload"
                            data-src="<?= _SITEDIR_ ?>public/images/journey/j-5.png"
                        />
                    </picture>
                </div>
                <div data-aos="zoom-in" class="journey-list-item">
                    <picture>
                        <source
                            data-srcset="<?= _SITEDIR_ ?>public/images/journey/j-6.webp"
                            type="image/webp"
                        />
                        <img
                            alt=""
                            class="lazyload"
                            data-src="<?= _SITEDIR_ ?>public/images/journey/j-6.png"
                        />
                    </picture>
                </div>
            </div>
            <div
                class="col-span-2 max-md:col-span-5 grid grid-cols-2 col-last"
            >
                <div data-aos="zoom-in" class="journey-list-item">
                    <picture>
                        <source
                            data-srcset="<?= _SITEDIR_ ?>public/images/journey/j-3.webp"
                            type="image/webp"
                        />
                        <img
                            alt=""
                            class="lazyload"
                            data-src="<?= _SITEDIR_ ?>public/images/journey/j-3.png"
                        />
                    </picture>
                </div>
                <div data-aos="zoom-in" class="journey-list-item">
                    <picture>
                        <source
                            data-srcset="<?= _SITEDIR_ ?>public/images/journey/j-4.webp"
                            type="image/webp"
                        />
                        <img
                            alt=""
                            class="lazyload"
                            data-src="<?= _SITEDIR_ ?>public/images/journey/j-4.png"
                        />
                    </picture>
                </div>
                <div data-aos="zoom-in" class="journey-list-item">
                    <picture>
                        <source
                            data-srcset="<?= _SITEDIR_ ?>public/images/journey/j-7.webp"
                            type="image/webp"
                        />
                        <img
                            alt=""
                            class="lazyload"
                            data-src="<?= _SITEDIR_ ?>public/images/journey/j-7.png"
                        />
                    </picture>
                </div>
            </div>
        </div>
        <div class="journey-text">
            <h3 class="journey-text-title" data-aos="fade-right">
                Lorem ipsum dolor sit amet
            </h3>
            <div class="journey-text-description-wr" data-aos="fade-left">
                <div class="journey-text-description">
                    <p>
                        Donec eget odio ultricies, ultricies erat sit amet, pulvinar
                        purus. Proin ligula enim, finibus nec euismod tristique,
                        ultricies at nunc. Suspendisse pellentesque nibh quam. Nunc
                        bibendum dolor a dignissim molestie. Proin scelerisque,
                        massa vel aliquet rutrum, lorem ipsum venenatis odio.
                    </p>
                </div>
                <a href="#" class="btn"
                ><span>Call to action</span><span>Call to action</span></a
                >
            </div>
        </div>
    </div>
</section>
<div class="dive-section">
    <div class="cont">
        <h2
            class="title title-marker title-marker-center text-center clip-title"
            data-aos="clip-down"
        >
            Ready to dive in?
        </h2>
        <a href="maito:hello@samuel-knight.com" data-aos="fade-up"
        >hello@samuel-knight.com</a
        >
    </div>
</div>