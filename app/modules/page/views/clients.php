<section class="hero-section">
    <div class="hero-section-bg sector-hero-section-bg">
        <div class="hero-l-gradient"></div>
        <div class="hero-black-layer mx-color"></div>
        <div class="hero-r-gradient"></div>
        <picture>
            <source
                    data-srcset="<?= _SITEDIR_ ?>public/images/hero-clients-bg.webp"
                    type="image/webp"
            />
            <img
                    alt=""
                    class="lazyload"
                    data-src="<?= _SITEDIR_ ?>public/images/hero-clients-bg.jpg"
            />
        </picture>
    </div>
    <div class="cont">
        <div class="hero-section-caption-wr">
            <div class="hero-section-caption">
                <h1
                        class="hero-section-title title-marker clip-title"
                        data-aos="clip-down"
                >
                    <span>Clients</span>
                </h1>
                <div
                        class="hero-section-motto"
                        data-aos="fade-up"
                        data-aos-delay="300"
                >
                    <p>We are Samuel Knight</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="mission-section">
    <div class="cont">
        <div class="mission-swiper-wr">
            <div class="mission-swiper-bg">
                <img alt="mission-map" src="<?= _SITEDIR_ ?>public/images/mission-map.svg" />
            </div>
            <div class="mission-swiper swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="mission-card" data-aos="fade-down">
                            <h3 class="mission-card-title title-marker">
                                Global recruitment specialists in the Renewable Energy,
                                Tech and Rail industries.
                            </h3>
                            <div class="mission-card-text">
                                <p>
                                    With Samuel Knight Projects we also provide a full
                                    service offering for your renewable energy project.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="mission-card" data-aos="fade-down">
                            <h3 class="mission-card-title title-marker">
                                Placing the best talent <br />
                                in the market, not <br />
                                necessarily on the market.
                            </h3>
                            <div class="mission-card-text">
                                <p>
                                    While we have a global talent pool of active
                                    candidates, we also have access to a large selection
                                    of passive candidates in our unique, market leading
                                    technology platform.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="mission-card" data-aos="fade-down">
                            <h3 class="mission-card-title title-marker">
                                We’re a true partner to both our clients and candidates.
                            </h3>
                            <div class="mission-card-text">
                                <p>
                                    By providing one point of contact, whether that’s a
                                    dedicated consultant or an account manager for
                                    large-scale projects, we understand the importance of
                                    quality delivery.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if ($this->solutions) { ?>
<section class="sdv-vertical-section sdv-vertical-section-solutions">
    <div class="cont">
        <div class="sdv-vertical-body">
            <div class="sdv-vertical-section-solutions-bg"></div>
            <h2
                    class="title title-marker text-center clip-title"
                    data-aos="clip-down"
            >
                <span>Solutions</span>
            </h2>
            <div class="sdv-vertical-body-items">
                        <?php foreach ($this->solutions as $solution) { ?>
                <div data-aos="fade-left" class="sdv-vertical-item">
                    <div class="sdv-vertical-item-thumbnail">
                        <div class="mixed-layer"></div>
                        <div class="mixed-layer"></div>
                        <picture>
                            <source
                                    data-srcset="<?= _SITEDIR_ ?>data/solutions/<?= getFileName($solution->image) . '.webp' ?>"
                                    type="image/webp"
                            />
                            <img
                                    alt=""
                                    class="lazyload"
                                    data-src="<?= _SITEDIR_ ?>data/solutions/<?= $solution->image ?>"
                            />
                        </picture>

                    </div>
                    <h3 class="sdv-vertical-item-title"><?= $solution->title ?></h3>
                    <div class="sdv-vertical-item-content-wr gray">
                        <div class="sdv-vertical-item-text">
                            <?= reFilter($solution->content) ?>
                        </div>
                    </div>
                </div>
                    <?php } ?>
            </div>
        </div>
    </div>
</section>
<?php } ?>
<section class="work-with-section">
    <div class="cont">
        <h3 class="title clip-title" data-aos="clip-down">
            <span>Who we work with</span>
        </h3>
    </div>
    <div class="ww-swiper-wr">
        <div class="ww-swiper swiper">
            <div class="swiper-wrapper">
                <?php if($this->works) { ?>
                    <?php foreach ($this->works as $work) { ?>
                <div data-aos="zoom-in" class="swiper-slide">
                    <img src="<?= _SITEDIR_ ?>data/we_work/<?=$work->image?>" />
                </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<?php if ($this->blogs) { ?>
<section class="case-studies-swiper-section">
    <div class="cont">
        <div class="title-wr">
            <h2 class="title title-marker clip-title" data-aos="clip-down">
                <span>Case Studies</span>
            </h2>
            <a href="#" class="btn"
            ><span>All Case Studies</span><span>All Case Studies</span></a
            >
        </div>
        <div class="case-studies-swiper-wr">
            <div class="case-studies-swiper swiper">
                <div class="swiper-wrapper">
                    <?php foreach ($this->blogs as $blog) { ?>
                    <div data-aos="zoom-in" class="swiper-slide">
                        <a href="#" class="case-study-card">
                            <div class="case-study-card-text">
                                <div class="case-study-card-sector text-gray">
                                    <?= $blog->sector->name ?>
                                </div>
                                <h3 class="title-marker title-marker-small text-gray">
                                    <?= $blog->title ?>
                                </h3>
                                <div>
                                    <?= reFilter($blog->content) ?>
                                </div>
                            </div>
                            <div class="case-study-card-thumbnail">
                                <picture>
                                    <source
                                            data-srcset="<?= _SITEDIR_ ?>data/blog/mini_<?= getFileName($blog->image) . '.webp' ?>"
                                            type="image/webp"
                                    />
                                    <img
                                            alt=""
                                            class="lazyload"
                                            data-src="<?= _SITEDIR_ ?>data/blog/<?= $blog->image ?>"
                                    />
                                </picture>
                            </div>
                        </a>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</section>
<?php } ?>
<section class="partners-section">
    <div class="cont">
        <h3 class="title">Members of</h3>
        <div class="partners-list" id="flex-elements-parent">
            <?php if ($this->partners) { ?>
                <?php foreach ($this->partners as $partner) { ?>
                    <div data-aos="fade-up" class="partners-card flex-element">
                        <img src="<?= _SITEDIR_ ?>data/partners/<?=$partner->image?>" />
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</section>
<div class="dive-section">
    <div class="cont">
        <h2
                class="title title-marker title-marker-center text-center clip-title"
                data-aos="clip-down"
        >
            Ready to dive in?
        </h2>
        <a href="maito:hello@samuel-knight.com" data-aos="fade-up"
        >hello@samuel-knight.com</a
        >
    </div>
</div>