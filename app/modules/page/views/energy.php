<section class="hero-section sector-hero-section">
    <div class="hero-section-bg sector-hero-section-bg">
        <div class="hero-l-gradient"></div>
        <div class="hero-bg-layer bg-blue mx-soft-light"></div>
        <div class="hero-black-layer mx-color"></div>
        <div class="hero-r-gradient"></div>
        <picture>
            <source
                    data-srcset="<?= _SITEDIR_ ?>public/images/energy-home-bg.webp"
                    type="image/webp"
            />
            <img
                    alt=""
                    class="lazyload"
                    data-src="<?= _SITEDIR_ ?>public/images/energy-home-bg.jpg"
            />
        </picture>
    </div>
    <div class="cont">
        <div class="hero-section-caption-wr">
            <div class="hero-section-caption">
                <h1
                        class="hero-section-title title-marker clip-title"
                        data-aos="clip-down"
                >
                    <span>Renewable</span> <br />
                    <span>Energy</span> <br />
                    <span>Solutions.</span>
                </h1>
                <div
                        class="hero-section-motto"
                        data-aos="fade-up"
                        data-aos-delay="300"
                >
                    <p>We are Samuel Knight</p>
                </div>
            </div>
            <div class="hero-sector-icons">
                <div class="sector-icon" data-aos="fade-left">
                    <img
                            alt="hero-sector"
                            src="<?= _SITEDIR_ ?>public/images/hero-sector-icons/hero-sector-energy.svg"
                    />
                </div>
            </div>
        </div>
    </div>
</section>
<section class="video-section">
    <div class="cont">
        <div class="video-section-content">
            <div class="video-section-text" data-aos="fade-right">
                <h3 class="title-marker title-marker-small">
                    Lorem ipsum dolor sit amet
                </h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
                    ultrices et ante ut ullamcorper. Suspendisse id ullamcorper
                    felis, in ultricies libero. Cras imperdiet sollicitudin magna,
                    vel condimentum magna accumsan a. Sed purus risus, bibendum
                    nec leo sit amet.
                </p>
            </div>
            <div class="video-wr" data-aos="fade-left">
                <video
                        preload="auto"
                        class="plyr-video-item"
                        loop="loop"
                        muted=""
                        playsinline="playsinline"
                        data-poster="<?= _SITEDIR_ ?>public/images/video-poster.jpg"
                        poster="<?= _SITEDIR_ ?>public/images/video-poster.jpg"
                >
                    <source src="<?= _SITEDIR_ ?>public/images/video/video-2.mp4" type="video/mp4" />
                </video>
            </div>
        </div>
    </div>
</section>
<section class="sector-mission-cards">
    <div class="sector-mission-cards-bg">
        <picture>
            <source
                    data-srcset="<?= _SITEDIR_ ?>public/images/sectors-mission.webp"
                    type="image/webp"
            />
            <img
                    alt=""
                    class="lazyload"
                    data-src="<?= _SITEDIR_ ?>public/images/sectors-mission.jpg"
            />
        </picture>
    </div>
    <div class="cont">
        <div class="sector-mission-swiper-wr">
            <div class="sector-mission-swiper swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="mission-card" data-aos="flip-up">
                            <h3 class="mission-card-title title-marker">
                                Project Empowerment
                            </h3>
                            <div class="mission-card-text">
                                <p>
                                    We provide project empowerment to our clients through
                                    expert global manpower solutions.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="mission-card" data-aos="flip-up">
                            <h3 class="mission-card-title title-marker">
                                Project Solutions
                            </h3>
                            <div class="mission-card-text">
                                <p>
                                    Our solutions are creative. We are not reckless – we
                                    are fearless, and bring this to your project.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="mission-card" data-aos="flip-up">
                            <h3 class="mission-card-title title-marker">
                                Contractor Services
                            </h3>
                            <div class="mission-card-text">
                                <p>
                                    As a specialist contractor provider, we offer our
                                    clients expert industry insight and can quickly
                                    mobilise large teams to assist with your project
                                    requirements.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="mission-card" data-aos="flip-up">
                            <h3 class="mission-card-title title-marker">
                                Contractor Care Solutions
                            </h3>
                            <div class="mission-card-text">
                                <p>
                                    AWe provide project empowerment to our clients through
                                    expert global manpower solutions.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="jt-section">
    <div class="section-bg jt-section-bg">
        <picture>
            <source
                    data-srcset="<?= _SITEDIR_ ?>public/images/jt-section.webp"
                    type="image/webp"
            />
            <img alt="" class="lazyload" data-src="<?= _SITEDIR_ ?>public/images/jt-section.png" />
        </picture>
    </div>
    <div class="cont">
        <div class="jt-section-content">
            <div data-aos="fade-down" class="jt-section-thumbnail mb-hidden">
                <picture>
                    <source
                            data-srcset="<?= _SITEDIR_ ?>public/images/contract-type.webp"
                            type="image/webp"
                    />
                    <img
                            alt=""
                            class="lazyload"
                            data-src="<?= _SITEDIR_ ?>public/images/contract-type.jpg"
                    />
                </picture>
            </div>
            <div class="jt-section-tabs" data-aos="fade-up">
                <div class="jt-section-tabs-list">
                    <div class="jt-section-tab">
                        <h3 class="title-marker title-marker-small active">
                            Contract Solutions
                        </h3>
                        <div class="jt-section-tab-content hidden">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Aliquam a fermentum nulla, et rhoncus risus. Phasellus
                                mattis augue id turpis ultrices, ac malesuada nibh
                                pellentesque. Nunc porta mauris arcu, vel pretium orci
                                porttitor vel. Fusce congue sit amet odio auctor
                                facilisis. Aliquam elementum turpis at leo auctor
                                lobortis. Suspendisse ut enim quis enim vestibulum
                                iaculis.
                            </p>
                        </div>
                    </div>
                    <div class="jt-section-tab">
                        <h3 class="title-marker title-marker-small">
                            Permanent Solutions
                        </h3>
                        <div class="jt-section-tab-content hidden">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing
                                elit. A cum libero sequi sint unde! Consequuntur error
                                facilis fugiat in molestiae natus optio similique!
                                Impedit laudantium nisi nulla, quia quidem vero.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="jt-section-thumbnail lg-hidden">
                    <picture>
                        <source
                                data-srcset="<?= _SITEDIR_ ?>public/images/contract-type.webp"
                                type="image/webp"
                        />
                        <img
                                alt=""
                                class="lazyload"
                                data-src="<?= _SITEDIR_ ?>public/images/contract-type.jpg"
                        />
                    </picture>
                </div>
                <div
                        class="jt-section-tabs-item-content text-light-gray"
                        id="jt-content-container"
                >
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Aliquam a fermentum nulla, et rhoncus risus. Phasellus
                        mattis augue id turpis ultrices, ac malesuada nibh
                        pellentesque. Nunc porta mauris arcu, vel pretium orci
                        porttitor vel. Fusce congue sit amet odio auctor facilisis.
                        Aliquam elementum turpis at leo auctor lobortis. Suspendisse
                        ut enim quis enim vestibulum iaculis.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="sdv-section">
    <div class="cont">
        <h2
                class="title title-marker text-center title-marker-center clip-title"
                data-aos="clip-down"
        >
            <span>Sub Divisions</span>
        </h2>
        <div class="sdv-swiper-wr">
            <div class="sdv-swiper swiper">
                <div class="sdv-body swiper-wrapper" data-aos="fade-up">
                    <div class="swiper-slide sdv-item">
                        <div class="sdv-item-thumbnail">
                            <div class="mixed-layer"></div>
                            <div class="mixed-layer"></div>
                            <picture>
                                <source
                                        data-srcset="<?= _SITEDIR_ ?>public/images/svd-thumbnail.webp"
                                        type="image/webp"
                                />
                                <img
                                        alt=""
                                        class="lazyload"
                                        data-src="<?= _SITEDIR_ ?>public/images/svd-thumbnail.jpg"
                                />
                            </picture>
                        </div>
                        <h3 class="sdv-item-title">Wind offshore</h3>
                        <div class="sdv-item-content">
                            <div class="sdv-item-content-text text-light-gray">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing
                                    elit. Aliquam a fermentum nulla, et rhoncus risus.
                                    Phasellus mattis augue id turpis ultrices, ac
                                    malesuada nibh pellentesque. Nunc porta mauris arcu,
                                    vel pretium orci porttitor vel.
                                </p>
                            </div>
                            <a class="btn" href="#">
                                <span>View jobs in Wind offshore</span>
                                <span>View jobs in Wind offshore</span>
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide sdv-item">
                        <div class="sdv-item-thumbnail">
                            <div class="mixed-layer"></div>
                            <div class="mixed-layer"></div>
                            <picture>
                                <source
                                        data-srcset="<?= _SITEDIR_ ?>public/images/svd-thumbnail.webp"
                                        type="image/webp"
                                />
                                <img
                                        alt=""
                                        class="lazyload"
                                        data-src="<?= _SITEDIR_ ?>public/images/svd-thumbnail.jpg"
                                />
                            </picture>
                        </div>
                        <h3 class="sdv-item-title">Wind onshore</h3>
                        <div class="sdv-item-content">
                            <div class="sdv-item-content-text text-light-gray">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing
                                    elit. Aliquam a fermentum nulla, et rhoncus risus.
                                    Phasellus mattis augue id turpis ultrices, ac
                                    malesuada nibh pellentesque. Nunc porta mauris arcu,
                                    vel pretium orci porttitor vel.
                                </p>
                            </div>
                            <a class="btn" href="#">
                                <span>View jobs in Wind onshore</span>
                                <span>View jobs in Wind onshore</span>
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide sdv-item">
                        <div class="sdv-item-thumbnail">
                            <div class="mixed-layer"></div>
                            <div class="mixed-layer"></div>
                            <picture>
                                <source
                                        data-srcset="<?= _SITEDIR_ ?>public/images/svd-thumbnail.webp"
                                        type="image/webp"
                                />
                                <img
                                        alt=""
                                        class="lazyload"
                                        data-src="<?= _SITEDIR_ ?>public/images/svd-thumbnail.jpg"
                                />
                            </picture>
                        </div>
                        <h3 class="sdv-item-title">Solar</h3>
                        <div class="sdv-item-content">
                            <div class="sdv-item-content-text text-light-gray">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing
                                    elit. Aliquam a fermentum nulla, et rhoncus risus.
                                    Phasellus mattis augue id turpis ultrices, ac
                                    malesuada nibh pellentesque. Nunc porta mauris arcu,
                                    vel pretium orci porttitor vel.
                                </p>
                            </div>
                            <a class="btn" href="#">
                                <span>View jobs in Solar</span>
                                <span>View jobs in Solar</span>
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide sdv-item">
                        <div class="sdv-item-thumbnail">
                            <div class="mixed-layer"></div>
                            <div class="mixed-layer"></div>
                            <picture>
                                <source
                                        data-srcset="<?= _SITEDIR_ ?>public/images/svd-thumbnail.webp"
                                        type="image/webp"
                                />
                                <img
                                        alt=""
                                        class="lazyload"
                                        data-src="<?= _SITEDIR_ ?>public/images/svd-thumbnail.jpg"
                                />
                            </picture>
                        </div>
                        <h3 class="sdv-item-title">Battery storage</h3>
                        <div class="sdv-item-content">
                            <div class="sdv-item-content-text text-light-gray">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing
                                    elit. Aliquam a fermentum nulla, et rhoncus risus.
                                    Phasellus mattis augue id turpis ultrices, ac
                                    malesuada nibh pellentesque. Nunc porta mauris arcu,
                                    vel pretium orci porttitor vel.
                                </p>
                            </div>
                            <a class="btn" href="#">
                                <span>View jobs in Battery Storage</span>
                                <span>View jobs in Battery Storage</span>
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide sdv-item">
                        <div class="sdv-item-thumbnail">
                            <div class="mixed-layer"></div>
                            <div class="mixed-layer"></div>
                            <picture>
                                <source
                                        data-srcset="<?= _SITEDIR_ ?>public/images/svd-thumbnail.webp"
                                        type="image/webp"
                                />
                                <img
                                        alt=""
                                        class="lazyload"
                                        data-src="<?= _SITEDIR_ ?>public/images/svd-thumbnail.jpg"
                                />
                            </picture>
                        </div>
                        <h3 class="sdv-item-title">T&D</h3>
                        <div class="sdv-item-content">
                            <div class="sdv-item-content-text text-light-gray">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing
                                    elit. Aliquam a fermentum nulla, et rhoncus risus.
                                    Phasellus mattis augue id turpis ultrices, ac
                                    malesuada nibh pellentesque. Nunc porta mauris arcu,
                                    vel pretium orci porttitor vel.
                                </p>
                            </div>
                            <a class="btn" href="#">
                                <span>View jobs in T&D</span>
                                <span>View jobs in T&D</span>
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide sdv-item">
                        <div class="sdv-item-thumbnail">
                            <div class="mixed-layer"></div>
                            <div class="mixed-layer"></div>
                            <picture>
                                <source
                                        data-srcset="<?= _SITEDIR_ ?>public/images/svd-thumbnail.webp"
                                        type="image/webp"
                                />
                                <img
                                        alt=""
                                        class="lazyload"
                                        data-src="<?= _SITEDIR_ ?>public/images/svd-thumbnail.jpg"
                                />
                            </picture>
                        </div>
                        <h3 class="sdv-item-title">Hydro</h3>
                        <div class="sdv-item-content">
                            <div class="sdv-item-content-text text-light-gray">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing
                                    elit. Aliquam a fermentum nulla, et rhoncus risus.
                                    Phasellus mattis augue id turpis ultrices, ac
                                    malesuada nibh pellentesque. Nunc porta mauris arcu,
                                    vel pretium orci porttitor vel.
                                </p>
                            </div>
                            <a class="btn" href="#">
                                <span>View jobs in Hydro</span>
                                <span>View jobs in Hydro</span>
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide sdv-item">
                        <div class="sdv-item-thumbnail">
                            <div class="mixed-layer"></div>
                            <div class="mixed-layer"></div>
                            <picture>
                                <source
                                        data-srcset="<?= _SITEDIR_ ?>public/images/svd-thumbnail.webp"
                                        type="image/webp"
                                />
                                <img
                                        alt=""
                                        class="lazyload"
                                        data-src="<?= _SITEDIR_ ?>public/images/svd-thumbnail.jpg"
                                />
                            </picture>
                        </div>
                        <h3 class="sdv-item-title">Hydrogen</h3>
                        <div class="sdv-item-content">
                            <div class="sdv-item-content-text text-light-gray">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing
                                    elit. Aliquam a fermentum nulla, et rhoncus risus.
                                    Phasellus mattis augue id turpis ultrices, ac
                                    malesuada nibh pellentesque. Nunc porta mauris arcu,
                                    vel pretium orci porttitor vel.
                                </p>
                            </div>
                            <a class="btn" href="#">
                                <span>View jobs in Hydrogen</span>
                                <span>View jobs in Hydrogen</span>
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide sdv-item">
                        <div class="sdv-item-thumbnail">
                            <div class="mixed-layer"></div>
                            <div class="mixed-layer"></div>
                            <picture>
                                <source
                                        data-srcset="<?= _SITEDIR_ ?>public/images/svd-thumbnail.webp"
                                        type="image/webp"
                                />
                                <img
                                        alt=""
                                        class="lazyload"
                                        data-src="<?= _SITEDIR_ ?>public/images/svd-thumbnail.jpg"
                                />
                            </picture>
                        </div>
                        <h3 class="sdv-item-title">Power</h3>
                        <div class="sdv-item-content">
                            <div class="sdv-item-content-text text-light-gray">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing
                                    elit. Aliquam a fermentum nulla, et rhoncus risus.
                                    Phasellus mattis augue id turpis ultrices, ac
                                    malesuada nibh pellentesque. Nunc porta mauris arcu,
                                    vel pretium orci porttitor vel.
                                </p>
                            </div>
                            <a class="btn" href="#">
                                <span>View jobs in Power</span>
                                <span>View jobs in Power</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sdv-item-content" id="sdv-item-content-mobile">
                <div class="sdv-item-content-text text-light-gray">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Aliquam a fermentum nulla, et rhoncus risus. Phasellus
                        mattis augue id turpis ultrices, ac malesuada nibh
                        pellentesque. Nunc porta mauris arcu, vel pretium orci
                        porttitor vel.
                    </p>
                </div>
                <a class="btn" href="#">
                    <span>View jobs in Battery Storage</span>
                    <span>View jobs in Battery Storage</span>
                </a>
            </div>
        </div>
    </div>
</section>
<section class="stats-section stats-section-col-3">
    <div class="cont">
        <h3 class="title" data-aos="fade-down">
            Our stats speak for themselves
        </h3>
        <div class="stats-grid">
            <?php if ($this->counters) { ?>
                <?php foreach ($this->counters as $counter) { ?>
                    <div class="stats-card" data-aos="zoom-in">
                        <div class="stats-card-counter" data-suffix="<?= $counter->suffix ?>"><?= $counter->numbers ?></div>
                        <div class="stats-card-text"><?= reFilter($counter->content) ?></div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</section>
<?php if ($this->users) { ?>
<section class="team-swiper-section">
    <div class="cont">
        <h2 class="title title-marker clip-title" data-aos="clip-down">
            <span>Energy Management Team</span>
        </h2>
        <div class="team-swiper-wr">
            <div class="team-swiper swiper">
                <div class="swiper-wrapper">
                    <?php include_once _SYSDIR_ .'modules/page/system/inc/_users.php'; ?>
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</section>
<?php } ?>
<section class="work-with-section">
    <div class="cont">
        <h3 class="title clip-title" data-aos="clip-down">
            <span>Who we work with</span>
        </h3>
    </div>
    <div class="ww-swiper-wr">
        <div class="ww-swiper swiper">
            <div class="swiper-wrapper">
                <?php if($this->works) { ?>
                    <?php foreach ($this->works as $work) { ?>
                        <div data-aos="zoom-in" class="swiper-slide">
                            <img src="<?= _SITEDIR_ ?>data/we_work/<?=$work->image?>" />
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<section class="case-study-section">
    <div class="cont">
        <div class="case-study-section-content">
            <div class="title-wr flex justify-between items-center">
                <h2 class="title-marker title clip-title" data-aos="clip-down">
                    <span>Case Study</span>
                </h2>
                <a class="btn" href="#">
                    <span>All Case Studies</span>
                    <span>All Case Studies</span>
                </a>
            </div>
            <a href="#" data-aos="fade-up" class="case-study-card">
                <div class="case-study-card-text">
                    <div class="case-study-card-sector text-gray">Energy</div>
                    <h3 class="title-marker title-marker-small text-gray">
                        Lorem ipsum dolor sit amet
                    </h3>
                    <div class="case-study-card-text-content" data-simplebar>
                        <p>
                            Cras imperdiet sollicitudin magna, vel condimentum magna
                            accumsan a. Sed purus risus, bibendum nec leo sit amet,
                            faucibus malesuada odio. Curabitur in odio vehicula,
                            consequat magna eu, consectetur lorem. Nullam congue est
                            sit amet ipsum pharetra venenatis.
                        </p>
                    </div>
                </div>
                <div class="case-study-card-thumbnail">
                    <picture>
                        <source
                                data-srcset="<?= _SITEDIR_ ?>public/images/case-study-thumbnail.webp"
                                type="image/webp"
                        />
                        <img
                                alt=""
                                class="lazyload"
                                data-src="<?= _SITEDIR_ ?>public/images/case-study-thumbnail.jpg"
                        />
                    </picture>
                </div>
            </a>
        </div>
    </div>
</section>
<?php if ($this->partners) { ?>
<section class="partners-section lg:pb-[87px]">
    <div class="cont">
        <h3 class="title">Members of</h3>
        <div class="partners-list" id="flex-elements-parent">
                <?php foreach ($this->partners as $partner) { ?>
                    <div data-aos="fade-up" class="partners-card flex-element">
                        <img src="<?= _SITEDIR_ ?>data/partners/<?=$partner->image?>" />
                    </div>
                <?php } ?>
        </div>
    </div>
</section>
<?php } ?>
<?php if ($this->jobs) { ?>
    <section class="latest-job-section">
        <div class="cont">
            <div class="title-wr flex justify-between items-center">
                <h2 class="title title-marker clip-title" data-aos="clip-down">
                    <span><contentElement name="jobs-list-title" type="input">Latest Energy Jobs</contentElement></span>
                </h2>
                <a class="btn" href="{LINK:jobs}?sector=1"
                >
                    <span><contentElement name="jobs-list-button" type="input">All Jobs</contentElement></span>
                    <span><contentElement name="jobs-list-button" type="input">All Jobs</contentElement></span>
                </a
                >
            </div>
            <div class="latest-job-swiper-wr">
                <div class="latest-job-swiper swiper">
                    <div class="swiper-wrapper">
                        <?php include 'app/modules/page/system/inc/_jobs.php' ?>
                    </div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </section>
<?php } ?>

<section class="drop-cv-section" data-aos="fade-down">
    <div class="cont">
        <div class="drop-form-wr">
            <h2 class="title title-marker clip-title" data-aos="clip-down">
                <span>Drop us your CV</span>
            </h2>
            <div class="drop-cv-form-motto text-gray">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
                    ultrices et ante ut ullamcorper. Suspendisse id ullamcorper
                    felis, in ultricies libero.
                </p>
            </div>
            <form action="#" class="drop-cv-form">
                <div class="input-wr">
                    <input name="name" placeholder="Your name" type="text" />
                </div>
                <div class="input-wr">
                    <input name="emal" placeholder="Your email" type="email" />
                </div>
                <div class="select-wr">
                    <select class="select2-item hidden" data-placeholder="Sector">
                        <option></option>
                        <option value="Energy">Energy</option>
                        <option value="Rail">Rail</option>
                        <option value="Projects">Projects</option>
                        <option value="Tech">Tech</option>
                    </select>
                </div>
                <div class="upload-btn">
                    <label>
                        <input
                                class="hidden"
                                id="cv_field"
                                name="cv_field"
                                type="hidden"
                                value="<?= post('cv_field', false, User::get('cv')); ?>"
                        />
                        <input
                                class="hidden"
                                accept=".doc, .docx, .txt, .pdf, .fotd"
                                class="pf-text-field"
                                name="cv_field"
                                onchange="initFile(this); load('cv/upload/', 'field=#cv_field', 'preview=.cv_file_name');"
                                type="file"
                        />
                        <span class="upload-status">Upload File</span>
                    </label>
                </div>
                <button class="submit-btn" type="submit">
                    <i class="icon-arrow-right"></i>
                </button>
            </form>
        </div>
    </div>
</section>
<section class="latest-news-section">
    <div class="cont">
        <div class="title-wr">
            <h2 class="title title-marker clip-title" data-aos="clip-down">
                <span>The latest</span>
            </h2>
            <a href="#" class="btn"
            ><span>All News</span><span>All News</span></a
            >
        </div>
        <div class="news-swiper-wr">
            <div class="news-swiper swiper">
                <div class="swiper-wrapper">
                    <?php include 'app/modules/page/system/inc/_blogs.php' ?>
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</section>