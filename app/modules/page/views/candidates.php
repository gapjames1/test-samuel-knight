<section class="hero-section">
    <div class="hero-section-bg sector-hero-section-bg">
        <div class="hero-l-gradient"></div>
        <div class="hero-black-layer mx-color"></div>
        <div class="hero-r-gradient"></div>
        <picture>
            <picture>
                <source
                        data-srcset="<?= _SITEDIR_ ?>public/images/hero-candidates-bg.webp"
                        type="image/webp"
                />
                <img
                        alt=""
                        class="lazyload"
                        data-src="<?= _SITEDIR_ ?>public/images/hero-candidates-bg.jpg"
                />
            </picture>
        </picture>
    </div>
    <div class="cont">
        <div class="hero-section-caption-wr">
            <div class="hero-section-caption">
                <h1
                        class="hero-section-title title-marker clip-title"
                        data-aos="clip-down"
                >
                    <span>Candidates</span>
                </h1>
                <div
                        class="hero-section-motto"
                        data-aos="fade-up"
                        data-aos-delay="300"
                >
                    <p>We are Samuel Knight</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section
        class="hero-form-section"
        data-aos="fade-up"
        id="hero-form-section"
>
    <div class="cont">
        <div class="hero-form-wr">
            <div class="hero-form-title">Job Search</div>
            <form action="#" class="hero-form" method="post">
                <div class="input-wr">
                    <input
                            name="keywords"
                            placeholder="Enter keyword e.g engineer"
                            type="text"
                    />
                </div>
                <div class="select-wr">
                    <select
                            class="select2-item hidden"
                            data-placeholder="Select location"
                    >
                        <option></option>
                        <option value="10">Location item</option>
                        <option value="15">Location item</option>
                        <option value="25">Location item</option>
                        <option value="30">Location item</option>
                    </select>
                </div>
                <div class="select-wr">
                    <select class="select2-item hidden" data-placeholder="Sector">
                        <option></option>
                        <option value="10">Sector item</option>
                        <option value="15">Sector item</option>
                        <option value="25">Sector item</option>
                        <option value="30">Sector item</option>
                    </select>
                </div>
                <button class="submit-btn" type="submit">
                    <i class="icon-arrow-right"></i>
                </button>
            </form>
        </div>
    </div>
</section>
<section class="mission-section">
    <div class="cont">
        <div class="mission-swiper-wr">
            <div class="mission-swiper-bg">
                <img alt="mission-map" src="<?= _SITEDIR_ ?>public/images/mission-map.svg" />
            </div>
            <div class="mission-swiper swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="mission-card" data-aos="fade-down">
                            <h3 class="mission-card-title title-marker">
                                Global recruitment specialists in the Renewable Energy,
                                Tech and Rail industries.
                            </h3>
                            <div class="mission-card-text">
                                <p>
                                    With Samuel Knight Projects we also provide a full
                                    service offering for your renewable energy project.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="mission-card" data-aos="fade-down">
                            <h3 class="mission-card-title title-marker">
                                Placing the best talent <br />
                                in the market, not <br />
                                necessarily on the market.
                            </h3>
                            <div class="mission-card-text">
                                <p>
                                    While we have a global talent pool of active
                                    candidates, we also have access to a large selection
                                    of passive candidates in our unique, market leading
                                    technology platform.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="mission-card" data-aos="fade-down">
                            <h3 class="mission-card-title title-marker">
                                We’re a true partner to both our clients and candidates.
                            </h3>
                            <div class="mission-card-text">
                                <p>
                                    By providing one point of contact, whether that’s a
                                    dedicated consultant or an account manager for
                                    large-scale projects, we understand the importance of
                                    quality delivery.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="sb-sectors-section">
    <div class="cont">
        <h2
                class="title text-center title-marker title-marker-center clip-title"
                data-aos="clip-down"
        >
            <span>Sub Sectors</span>
        </h2>
        <div class="sb-sectors" data-aos="fade-up">
            <div class="sb-sectors-item">
                <div class="sb-sectors-item-content">
                    <h3>
                        Recruitment
                        <br />
                        process
                    </h3>
                </div>
            </div>
            <div class="sb-sectors-item">
                <div class="sb-sectors-item-content">
                    <h3>Mobilisation</h3>
                </div>
            </div>
            <div class="sb-sectors-item">
                <div class="sb-sectors-item-content">
                    <h3>Safety</h3>
                </div>
            </div>
            <div class="sb-sectors-item">
                <div class="sb-sectors-item-content">
                    <h3>
                        Contractor <br />
                        portal
                    </h3>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="crc-section">
    <div class="cont">
        <div class="crc-list">
            <div data-aos="flip-up" class="crc-item">
                <div class="crc-item-content">
                    <h3>
                        Key Point <br />
                        Cards
                    </h3>
                </div>
            </div>
            <div data-aos="flip-up" class="crc-item">
                <div class="crc-item-content">
                    <h3>Rule Book Modules</h3>
                </div>
            </div>
            <div data-aos="flip-up" class="crc-item">
                <div class="crc-item-content">
                    <h3>Hand Books</h3>
                </div>
            </div>
            <div data-aos="flip-up" class="crc-item">
                <div class="crc-item-content">
                    <h3>
                        Safety Critical <br />
                        Forms
                    </h3>
                </div>
            </div>
            <div data-aos="flip-up" class="crc-item">
                <div class="crc-item-content">
                    <h3>Standards</h3>
                </div>
            </div>
            <div data-aos="flip-up" class="crc-item">
                <div class="crc-item-content">
                    <h3>
                        Track Working <br />
                        Instructions
                    </h3>
                </div>
            </div>
            <div data-aos="flip-up" class="crc-item">
                <div class="crc-item-content">
                    <h3>
                        Customer <br />
                        Specific Forms
                    </h3>
                </div>
            </div>
            <div data-aos="flip-up" class="crc-item">
                <div class="crc-item-content">
                    <h3>Progression Path</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if ($this->blogs) { ?>
<section class="case-studies-swiper-section">
    <div class="cont">
        <div class="title-wr">
            <h2 class="title title-marker">Case Studies</h2>
            <a href="#" class="btn"
            ><span>All Case Studies</span><span>All Case Studies</span></a
            >
        </div>
        <div class="case-studies-swiper-wr">
            <div class="case-studies-swiper swiper">
                <div class="swiper-wrapper">
                    <?php foreach ($this->blogs as $blog) { ?>
                        <div data-aos="zoom-in" class="swiper-slide">
                            <a href="#" class="case-study-card">
                                <div class="case-study-card-text">
                                    <div class="case-study-card-sector text-gray">
                                        <?= $blog->sector->name ?>
                                    </div>
                                    <h3 class="title-marker title-marker-small text-gray">
                                        <?= $blog->title ?>
                                    </h3>
                                    <div>
                                        <?= reFilter($blog->content) ?>
                                    </div>
                                </div>
                                <div class="case-study-card-thumbnail">
                                    <picture>
                                        <source
                                                data-srcset="<?= _SITEDIR_ ?>data/blog/mini_<?= getFileName($blog->image) . '.webp' ?>"
                                                type="image/webp"
                                        />
                                        <img
                                                alt=""
                                                class="lazyload"
                                                data-src="<?= _SITEDIR_ ?>data/blog/<?= $blog->image ?>"
                                        />
                                    </picture>
                                </div>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</section>
<?php } ?>
<section class="drop-cv-section" data-aos="fade-down">
    <div class="cont">
        <div class="drop-form-wr">
            <h2 class="title title-marker clip-title" data-aos="clip-down">
                <span>Drop us your CV</span>
            </h2>
            <div class="drop-cv-form-motto text-gray">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
                    ultrices et ante ut ullamcorper. Suspendisse id ullamcorper
                    felis, in ultricies libero.
                </p>
            </div>
            <form action="#" class="drop-cv-form">
                <div class="input-wr">
                    <input name="name" placeholder="Your name" type="text" />
                </div>
                <div class="input-wr">
                    <input name="emal" placeholder="Your email" type="email" />
                </div>
                <div class="select-wr">
                    <select class="select2-item hidden" data-placeholder="Sector">
                        <option></option>
                        <option value="Energy">Energy</option>
                        <option value="Rail">Rail</option>
                        <option value="Projects">Projects</option>
                        <option value="Tech">Tech</option>
                    </select>
                </div>
                <div class="upload-btn">
                    <label>
                        <input
                                class="hidden"
                                id="cv_field"
                                name="cv_field"
                                type="hidden"
                                value="<?= post('cv_field', false, User::get('cv')); ?>"
                        />
                        <input
                                class="hidden"
                                accept=".doc, .docx, .txt, .pdf, .fotd"
                                class="pf-text-field"
                                name="cv_field"
                                onchange="initFile(this); load('cv/upload/', 'field=#cv_field', 'preview=.cv_file_name');"
                                type="file"
                        />
                        <span class="upload-status">Upload File</span>
                    </label>
                </div>
                <button class="submit-btn" type="submit">
                    <i class="icon-arrow-right"></i>
                </button>
            </form>
        </div>
    </div>
</section>
<div class="dive-section">
    <div class="cont">
        <h2
                class="title title-marker title-marker-center text-center clip-title"
                data-aos="clip-down"
        >
            Ready to dive in?
        </h2>
        <a href="maito:hello@samuel-knight.com" data-aos="fade-up"
        >hello@samuel-knight.com</a
        >
    </div>
</div>