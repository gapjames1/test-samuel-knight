<?php foreach ($this->users as $user) { ?>
    <div data-aos="zoom-in" class="swiper-slide">
        <div class="teammate-card">
            <div class="teammate-card-text">
                <h3><?= $user->firstname ?> <?= $user->lastname ?></h3>
                <div class="teammate-card-info">
                    <p><?= $user->job_title ?></p>
                    <?php if ($user->linkedin) { ?>
                        <a href="<?= $user->linkedin ?>">
                            <i class="icon-linkedin"></i>
                        </a>
                    <?php } ?>
                </div>
            </div>
            <div class="teammate-card-thumbnail">
                <picture>
                    <source
                            data-srcset="<?= _SITEDIR_ ?>data/users/mini_<?= getFileName($user->image) . '.webp' ?>"
                            type="image/webp"
                    />
                    <img
                            alt=""
                            class="lazyload"
                            data-src="<?= _SITEDIR_ ?>data/users/<?= $user->image ?>"
                    />
                </picture>
            </div>
        </div>
    </div>
<?php } ?>
