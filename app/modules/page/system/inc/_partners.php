<?php if ($this->partners) { ?>
    <?php foreach ($this->partners as $partner) { ?>
    <div data-aos="fade-up" class="partners-card flex-element">
        <img src="<?= _SITEDIR_ ?>data/partners/<?=$partner->image?>" />
    </div>
    <?php } ?>
<?php } ?>