<?php if ($this->blogs) { ?>
    <?php foreach($this->blogs as $blog) { ?>
<div data-aos="zoom-in" class="swiper-slide">
    <a href="{URL:news-post/<?= $blog->slug ?>}" class="news-card">
        <div class="news-card-text">
            <div class="news-card-time">
                <time><?= date('M d, Y', $blog->time) ?></time>
                <span><?= $blog->sector->name ?></span>
            </div>
            <h3 class="news-card-title">
                <?= $blog->title ?>
            </h3>
            <div class="news-card-description">
                <?= reFilter($blog->content); ?>
            </div>
        </div>
        <div class="news-card-thumbnail">
            <picture>
                <source
                    data-srcset="<?= _SITEDIR_ ?>data/blog/mini_<?= getFileName($blog->image) . '.webp' ?>"
                    type="image/webp"
                />
                <img
                    alt=""
                    class="lazyload"
                    data-src="<?= _SITEDIR_ ?>data/blog/<?= $blog->image ?>"
                />
            </picture>
        </div>
    </a>
</div>
    <?php } ?>
<?php } ?>