<?php foreach ($this->jobs as $job) { ?>
    <div class="swiper-slide" data-aos="fade-down">
        <a href="{URL:job/<?= $job->slug ?>}" class="job-card">
            <div class="job-card-sector"><?= $job->sector->name ?></div>
            <h3 class="job-card-title title-marker">
                <?= $job->title ?>
            </h3>
            <ul class="job-card-params">
                <?php if ($job->locations) { ?>
                    <li>
                        <div><?= propertiesToString($job->locations) ?></div>
                    </li>
                <?php } ?>
                <?php if ($job->contract_type) { ?>
                    <li>
                        <div><?= ucfirst($job->contract_type) ?></div>
                    </li>
                <?php } ?>
                <?php if ($job->sub_sector->title) { ?>
                    <li>
                        <div><?= $job->sub_sector->title ?></div>
                    </li>
                <?php } ?>
            </ul>
            <div class="job-card-description">
                <?= reFilter($job->content_short) ?>
            </div>
        </a>
    </div>
<?php } ?>
