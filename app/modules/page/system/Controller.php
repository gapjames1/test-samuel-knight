<?php

class PageController extends Controller
{
    use Validator;

    public function indexAction()
    {
        Model::import('panel/counters');
        $this->view->counters = CountersModel::getByPage('home');

        Model::import('panel/partners');
        $this->view->partners = PartnersModel::getByPage('home');

        Model::import('panel/stories');
        $this->view->stories = StoriesModel::getByPage('home');

        Model::import('news');
        $this->view->blogs = NewsModel::search(false,false,false,false,5);

        Model::import('jobs');
        $this->view->jobs = JobsModel::search(false,false,false,false,false,false,false,5);
    }

    public function candidatesAction()
    {
        Model::import('news');
        $this->view->blogs = NewsModel::search(false,false,false,false,5);
    }

    public function clientsAction()
    {
        Model::import('panel/partners');
        $this->view->partners = PartnersModel::getByPage('clients');

        Model::import('news');
        $this->view->blogs = NewsModel::search(false,false,false,false,5);

        Model::import('panel/we_work');
        $this->view->works = We_workModel::getByPage('clients');

        Model::import('panel/solutions');
        $this->view->solutions = SolutionsModel::getAll();
    }

    public function cookiesAction()
    {
    }

    public function energyAction()
    {
        Model::import('panel/counters');
        $this->view->counters = CountersModel::getByPage('energy');

        Model::import('jobs');
        $this->view->jobs = JobsModel::search(false, false, false, 1, false, false, false, 5);

        Model::import('panel/partners');
        $this->view->partners = PartnersModel::getByPage('energy');

        Model::import('panel/team');
        $this->view->users = TeamModel::getUsersWhere(" `sector_id` = 1");

        Model::import('news');
        $this->view->blogs = NewsModel::search(false,1,false,false,5);


        Model::import('panel/we_work');
        $this->view->works = We_workModel::getByPage('energy');
    }

    public function policyAction()
    {
    }

    public function projectsAction()
    {
        Model::import('panel/counters');
        $this->view->counters = CountersModel::getByPage('projects');

        Model::import('panel/partners');
        $this->view->partners = PartnersModel::getByPage('projects');

        Model::import('panel/we_work');
        $this->view->works = We_workModel::getByPage('projects');

        Model::import('panel/team');
        $this->view->users = TeamModel::getUsersWhere(" `sector_id` = 3");

        Model::import('news');
        $this->view->blogs = NewsModel::search(false,3,false,false,5);

        Model::import('jobs');
        $this->view->jobs = JobsModel::search(false, false, false, 3, false, false, false, 5);
    }

    public function railAction()
    {
        Model::import('panel/counters');
        $this->view->counters = CountersModel::getByPage('rail');

        Model::import('panel/team');
        $this->view->users = TeamModel::getUsersWhere(" `sector_id` = 2");

        Model::import('panel/partners');
        $this->view->partners = PartnersModel::getByPage('rail');

        Model::import('panel/we_work');
        $this->view->works = We_workModel::getByPage('rail');

        Model::import('news');
        $this->view->blogs = NewsModel::search(false,2,false,false,5);

        Model::import('jobs');
        $this->view->jobs = JobsModel::search(false, false, false, 2, false, false, false, 5);
    }

    public function techAction()
    {
        Model::import('panel/counters');
        $this->view->counters = CountersModel::getByPage('tech');

        Model::import('panel/we_work');
        $this->view->works = We_workModel::getByPage('tech');

        Model::import('panel/team');
        $this->view->users = TeamModel::getUsersWhere(" `sector_id` = 4");

        Model::import('panel/partners');
        $this->view->partners = PartnersModel::getByPage('tech');

        Model::import('jobs');
        $this->view->jobs = JobsModel::search(false, false, false, 4, false, false, false, 5);

        Model::import('news');
        $this->view->blogs = NewsModel::search(false,4,false,false,5);
    }

    public function termsAction()
    {
    }

    /**
     * access to file(CV) download (do not remove)
     */
    public function file_downloadAction()
    {
        if (User::getRole() !== 'admin')
            error404();

        $path_to_file = _SYSDIR_ . 'data/cvs/' . Request::getUri(0);

        if (file_exists($path_to_file)) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);

            header('Content-Type: ' . finfo_file($finfo, $path_to_file));

            $finfo = finfo_open(FILEINFO_MIME_ENCODING);
            header('Content-Transfer-Encoding: ' . finfo_file($finfo, $path_to_file));
            header('Content-disposition: attachment; filename="' . basename($path_to_file) . '"');
            readfile($path_to_file);
        } else {
            error404();
        }
        exit;
    }

    /**
     * Email status (do not remove)
     * @return void
     */
    public function email_statusAction()
    {
        $token = get('token');

        Model::update('email_logs', ['status' => 'read'], "`token` = '$token'");

        $image = imagecreatefrompng('images/panel/px.png');
        header("Content-type: " . image_type_to_mime_type(IMAGETYPE_PNG));
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-cache, must-relative");
        imagegif($image);
        imagedestroy($image);
        exit;
    }

    public function uploadAction()
    {
        Request::ajaxPart(); // if not Ajax part load

        $name    = post('name', true); // file name, if not set - will be randomly
        $path    = post('path', true, 'tmp'); // path where file will be saved, default: 'tmp'
        $field   = post('field', true, '#image'); // field where to put file name after uploading
        $preview = post('preview', true, '#preview_file'); // field where to put file name after uploading
        $real_name = post('file_real_name', true); // field where to put file name after uploading

        $path = 'data/' . $path . '/';

        $result = null;
        foreach ($_FILES as $file) {
            $result = File::uploadCV($file, $path, $name);
            break;
        }

        $newFileName = $result['name'] . '.' . $result['format']; // randomized name

        Request::addResponse('val', $field, $newFileName);
        Request::addResponse('val', $real_name, str_replace(' ', '_', $result['fileName']));
        Request::addResponse('html', $preview, $result['fileName']);
    }

    /**
     * Autogenerate sitemap
     * @return void
     */
    public function sitemapAction()
    {
        header("Content-type: text/xml");

        Model::import('panel/settings/sitemap');

        $time = SettingsModel::get('sitemap_links_time');
        if (intval($time) + 600 > time()) {
            $tablesLinks = json_decode(File::read(_SYSDIR_ . 'data/cache/sitemap_links.json'));
        } else {
            // Get all tables
            $allTables = SitemapModel::getAll();

            $tablesLinks = [];
            foreach ($allTables as $table) {
                // Проверяем передавался ли параметр в url
                // Если параметр есть, то достаём его имя и подсатавляем в выборку
                $row = 'id';
                $url = $table->url;
                if (strpos($table->url, '{')) {
                    $urlArray = explode('{', $table->url);
                    $url = $urlArray[0];
                    $row = substr($urlArray[1], 0, -1);
                }

                $allRows = Model::fetchAll(Model::select($table->table, reFilter($table->where), $row));
                $strUrl = $table->base_url . $url;
                foreach ($allRows as $value) {
                    array_push($tablesLinks, $strUrl . $value->$row);
                }
            }

            $customLinks = SettingsModel::get('sitemap_links');

            if ($customLinks) {
                $customLinks = preg_split('/\n|\r\n?/', $customLinks);

                foreach ($customLinks as $link) {
                    array_unshift($tablesLinks, $link);
                }
            }

            File::write(File::mkdir('data/cache/') . 'sitemap_links.json', (json_encode($tablesLinks))); // reFilter
            SettingsModel::set('sitemap_links_time', time());
        }

        // Write data to sitemap.xml
        $fd = fopen(_BASEPATH_ . "sitemap.xml", 'w') or die("Some error, we cant create file!");

        $sitemap = '<?xml version="1.0" encoding="UTF-8"?>'
            . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">' . PHP_EOL;

        $sitemap .= '<url>'
            . '<loc>' . SITE_URL . '</loc>'
            . '<lastmod>' . date("Y-m-d", time()) . '</lastmod>'
            . '<priority>0.9</priority>'
            . '</url>' . PHP_EOL;

        foreach ($tablesLinks as $item) {
            $sitemap .= PHP_EOL . '<url>'
                . '<loc>' . $item . '</loc>'
                . '<lastmod>' . date("Y-m-d", time()) . '</lastmod>'
                . '<priority>0.9</priority>'
                . '</url>' . PHP_EOL;
        }

        $sitemap .= PHP_EOL . '</urlset>';
        fwrite($fd, $sitemap);
        fclose($fd);

        echo file_get_contents("sitemap.xml");
        exit;
    }

    /**
     * view uploaded document / uploads module
     */
    public function uploadsAction()
    {
        $slug = Request::getUri(0);

        Model::import('panel/uploads');
        $file  = UploadsModel::getBySlug($slug);

        if (!$file)
            error404();

        $file = $file->file;

        if (!$file || !$file->file) {
            error404();
        }

        $path_to_file =  _SYSDIR_ . 'data/uploads/' . $file;

        if (file_exists($path_to_file)) {
            $disposition = 'attachment';

            $pathinfo = pathinfo($path_to_file);
            if ($pathinfo['extension'] === 'pdf')
                $disposition = 'inline';

            $finfo = finfo_open(FILEINFO_MIME_TYPE);

            header('Content-Type: ' . finfo_file($finfo, $path_to_file));

            $finfo = finfo_open(FILEINFO_MIME_ENCODING);
            header('Content-Transfer-Encoding: ' . finfo_file($finfo, $path_to_file));
            header('Content-disposition: ' . $disposition . '; filename="' . $slug . '.' . $pathinfo['extension'] . '"');
            @readfile($path_to_file);
        } else {
            error404();
        }
        exit;
    }
}
/* End of file */
