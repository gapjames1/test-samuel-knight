<?php

class JobsController extends Controller
{

    private static $elemOnPage = 1;
    public function indexAction()
    {
        Model::import('panel/vacancies/sectors');
        Model::import('panel/vacancies/locations');
        Model::import('panel/vacancies/sub_sectors');

        $this->view->sectors = SectorsModel::getAll();
        $this->view->locations = LocationsModel::getAll();
        $this->view->sub_sectors = Sub_sectorsModel::getAll();

        $keywords = get('keywords');

        $jobs = JobsModel::search($keywords);
        $this->view->count = $countJobs = count($jobs);
        Pagination::calculate(post('page'), self::$elemOnPage, $countJobs);

        $this->view->list = array_slice($jobs, Pagination::$start, Pagination::$end);

    }

    public function viewAction()
    {
        Model::import('panel/vacancies/locations');
        $this->view->locations = LocationsModel::getAll();

        $this->view->jobs = JobsModel::search(false, false, false, false, false, false, 6);

        Model::import('news');
        $this->view->blogs = NewsModel::search(false,false,false,false,5);

        $slug = Request::getUri(0);
        $this->view->job = JobsModel::getBySlug($slug);

        if (!$this->view->job || (!User::get('id') && $this->view->job->posted == 'no'))
            redirect(url('jobs'));

        if (setViewStat('vacancy_', $this->view->job->id, 'vacancies_analytics')) {
            $data_views['views'] = '++';
            Model::update('vacancies', $data_views, "`id` = '" . $this->view->job->id . "'");
        }
        Request::setTitle('Vacancy - ' . ($this->view->job->meta_title ?: $this->view->job->title));
        Request::setDescription($this->view->job->meta_desc);
        Request::setKeywords($this->view->job->meta_keywords);
    }

    public function searchAction()
    {
        Request::ajaxPart();
        $keywords   = post('keywords');
        $locations  = post('locations');
        $location   = post('location');
        $sector     = post('sector');
        $sub_sector = post('sub_sector');

        if ($locations && is_string($locations)) {
            $locations = explode(',', $locations);
        }

        if ($location && is_string($location)) {
            $location = explode(',', $location);
        }

        if ($sector && is_string($sector)) {
            $sector = explode(',', $sector);
        }

        if ($sub_sector && is_string($sub_sector)) {
            $sub_sector = explode(',', $sub_sector);
        }

        if ($location) {
            $locations[] = $location;
        }


        $jobs = JobsModel::search($keywords, $sector, $locations, $sub_sector);
        $countJobs = count($jobs);
        Pagination::calculate(post('page'), self::$elemOnPage, $countJobs);

        $this->view->list = array_slice($jobs, Pagination::$start, Pagination::$end);

        $post = allPost();

        if (is_array($location)) {
            $post['location'] = implode(',', $location);
        }

        if (is_array($location)) {
            $post['locations'] = implode(',', $locations);
        }


        if (is_array($sector)) {
            $post['sector'] = implode(',', $sector);
        }

        if (is_array($sub_sector)) {
            $post['sub_sector'] = implode(',', $sub_sector);
        }


        Request::addResponse('html', '#pagination', $countJobs > self::$elemOnPage ? Pagination::ajax('jobs/search', $post) : '');
        Request::addResponse('html', '#search_results', $this->getView());
//        Request::addResponse('func', 'scrollToEl', '#search_results|500');
        Request::endAjax();
    }
}
