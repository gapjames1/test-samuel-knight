<?php
class   JobsModel extends Model
{
    public $version = 0;

    /**
     * Method module_install start automatically if it not exist in `modules` table at first importing of model
     */
    public function module_install()
    {
        $queries = array();

        foreach ($queries as $query)
            self::query($query);
    }

    /**
     * Method module_update start automatically if current $version != version in `modules` table, and start from "case 'i'", where i = prev version in modules` table
     * @param int $version
     */
    public function module_update($version)
    {
        $queries = array();

        switch ($version) {

        }

        foreach ($queries as $query)
            self::query($query);
    }

    public static function getPrevJobs($id)
    {
        $sql = "
            SELECT *
            FROM `jobs`
            WHERE `id` < '$id' AND `deleted` = 'no' AND `posted` = 'yes'
            ORDER BY `id` DESC
            LIMIT 1
        ";

        return self::fetch(self::query($sql));
    }

    public static function countJobs()
    {
        $sql = "
            SELECT COUNT(`id`)
            FROM `vacancies`
            WHERE `deleted` = 'no' AND `posted` = 'yes' AND `time_expire` > '" . time() . "' AND `time` < " . time();

        return self::fetch(self::query($sql), 'row')[0];
    }

    public static function getBySlug($slug)
    {
        $sql = "SELECT * 
            FROM `vacancies` 
            WHERE `deleted` = 'no' 
              AND `slug` = '$slug'
            LIMIT 1
        ";

        $vacancy = self::fetch(self::query($sql));
        $vacancy = Model::relationship($vacancy, 'vacancies', 'locations');
        $vacancy = Model::relationship($vacancy, 'vacancies', 'users', ['id', 'firstname', 'lastname', 'slug', 'image', 'display_team', 'email', 'tel', 'linkedin', 'job_title'], false, 'consultant_id', 'one_to_many');
        $vacancy = Model::relationship($vacancy, 'vacancies', 'sub_sectors', '*', false, 'sub_sector_id', 'one_to_many');

        return $vacancy;

    }

    public static function getJobsByID($id)
    {
        $sql = "
            SELECT *
            FROM `jobs`
            WHERE `id` = '$id' AND `deleted` = 'no'
            LIMIT 1
        ";

        return self::fetch(self::query($sql));
    }

    public static function getImage($id)
    {
        $sql = "
            SELECT image FROM jobs
        ";

        return self::fetch(self::query($sql));
    }
    /**
     * Get all
     * @return array
     */
    public static function getAll($start = false, $end = false)
    {
        $sql = "
            SELECT *
            FROM `jobs`
            WHERE `deleted` = 'no' AND `posted` = 'yes' AND `time` < " . time() . "
            ORDER BY `time` DESC
        ";

        if ($start !== false) {
            $sql .= " LIMIT $start";

            if ($end !== false)
                $sql .= ", $end";
        }

        $blogs = self::fetchAll(self::query($sql));

        if ($blogs)
            $blogs = Image::getAltsForMany('blog', $blogs);

        return $blogs;
    }

    public static function search($keywords = false, $location = false, $locations = false, $sector = false, $sub_sector = false, $start = false, $end = false, $limit = false)
    {
        $sql = "
            SELECT *
            FROM `vacancies`
            WHERE `deleted` = 'no' AND `posted` = 'yes' AND `time_expire` > '" . time() . "' AND `time` < " . time();

        if ($keywords)
            $sql .= " AND (`title` LIKE '%$keywords%' OR `content` LIKE '%$keywords%' OR `ref` LIKE '%$keywords%')";

//        if ($type) {
//            if (is_array($type)) // by array of types
//                $sql .= " AND `vacancies`.`contract_type` IN ('" . implode("','", $type) . "')";
//            else // by once type
//                $sql .= " AND `vacancies`.`contract_type` = '$type'";
//        }

        $sql .= " ORDER BY `time` DESC";

        if ($start) {
            $sql .= " LIMIT $start";
            if ($end) {
                $sql .= ", $end";
            }
        }

        if ($limit !== false)
            $sql .= " LIMIT $limit ";


        $vacancies = self::fetchAll(self::query($sql));

        $vacancies = self::relationship($vacancies, 'vacancies', 'locations', ['id', 'name'], $location);
        $vacancies = self::relationship($vacancies, 'vacancies', 'locations', ['id', 'name'], $locations);
        $vacancies = self::relationship($vacancies, 'vacancies', 'sectors', ['id', 'name'], $sector,
            false, 'one_to_many');
        $vacancies = self::relationship($vacancies, 'vacancies', 'sub_sectors', ['id', 'name'], $sub_sector,
            false, 'one_to_many');

        return $vacancies;
    }

}

/* End of file */