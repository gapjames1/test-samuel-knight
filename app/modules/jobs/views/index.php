<section class="hero-section">
    <div class="cont">
        <div class="hero-section-caption-wr">
            <div class="hero-section-caption">
                <h1
                        class="hero-section-title title-marker clip-title"
                        data-aos="clip-down"
                >
                    <span>Job</span> <br />
                    <span>Search.</span>
                </h1>
            </div>
        </div>
    </div>
</section>

<section
        class="hero-form-section"
        data-aos="fade-up"
        id="hero-form-section"
>
    <div class="cont">
        <div class="hero-form-wr">
            <form class="hero-form" id="search_form" method="post">
                <div class="input-wr">
                    <input
                            id="keywords"
                            name="keywords"
                            placeholder="Enter keyword e.g engineer"
                            type="text"
                    />
                </div>
                <div class="select-wr">
                    <select
                            class="select2-item hidden"
                            data-placeholder="Select location"
                            name="location"
                    >
                        <option></option>
                        <?php if ($this->locations) { ?>
                        <?php foreach ($this->locations as $location) { ?>
                        <option value="<?= $location->id ?>"><?= $location->name ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="select-wr">
                    <select class="select2-item hidden" data-placeholder="Sector" name="sector">
                        <option></option>
                        <?php if ($this->sectors) { ?>
                            <?php foreach ($this->sectors as $sector) { ?>
                                <option value="<?= $sector->id ?>"><?= $sector->name ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="select-wr">
                    <select
                            class="select2-item hidden"
                            data-placeholder="Sub sector"
                            name="sub_sector"
                    >
                        <option></option>
                        <?php if ($this->sub_sectors) { ?>
                            <?php foreach ($this->sub_sectors as $sub_sector) { ?>
                                <option value="<?= $sub_sector->id ?>"><?= $sub_sector->name ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <button class="submit-btn" onclick="load('jobs/search', 'form:#search_form', 'form:#search_form2'); return false" type="submit">
                    <i class="icon-arrow-right"></i>
                </button>
            </form>
        </div>
    </div>
</section>

<div class="job-search__reset">
    <div class="cont flex" data-aos="fade-up">
        <a href="{URL:/jobs}" class="btn">
            <span>Reset Search</span>
            <span>Reset Search</span>
        </a>
        <div class="job-search__sort relative">
            <span>Sort by</span>
            <div class="select-wr">
                <select
                        class="select2-item hidden"
                        data-placeholder="Best match"
                >
                    <option></option>
                    <option value="10">Most Recent</option>
                    <option value="15">Most Recent</option>
                    <option value="25">Most Recent</option>
                    <option value="30">Most Recent</option>
                </select>
            </div>
        </div>
    </div>
</div>

<section class="jobs-box">
    <div class="cont">
        <div class="jobs-box__wrapper">
            <div class="jobs-box__btn">
                <span></span>
            </div>
            <form class="jobs-box__filter" id="search_form2">
                <div
                        class="jobs-box__form"
                        data-simplebar
                        data-aos="fade-right"
                >
                    <div class="jobs-box__accardion">
                        <div class="jobs-box__accardion-title">Sector</div>
                        <div class="jobs-box__accardion-content">
                            <?php if ($this->sectors) { ?>
                                <?php foreach ($this->sectors as $sector) { ?>
                                    <label>
                                        <input type="checkbox" onchange="load('jobs/search', 'form:#search_form', 'form:#search_form2'); return false" name="sector[]" value="<?= $sector->id ?>" />
                                        <span class="jobs-box__checkbox"></span>
                                        <?= $sector->name ?>
                                        <span>(10)</span>
                                    </label>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="jobs-box__accardion">
                        <div class="jobs-box__accardion-title">Sub-Sector</div>
                        <div class="jobs-box__accardion-content">
                            <?php if ($this->sub_sectors) { ?>
                                <?php foreach ($this->sub_sectors as $sub_sector) { ?>
                                    <label>
                                        <input type="checkbox" onchange="load('jobs/search', 'form:#search_form', 'form:#search_form2'); return false" name="sub_sector[]" value="<?= $sub_sector->id ?>" />
                                        <span class="jobs-box__checkbox"></span>
                                        <?= $sub_sector->name ?>
                                        <span></span>
                                    </label>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="jobs-box__accardion">
                        <div class="jobs-box__accardion-title">Location</div>
                        <div class="jobs-box__accardion-content">
                            <?php if ($this->locations) { ?>
                                    <?php foreach ($this->locations as $location) { ?>
                            <label>
                                <input type="checkbox" onchange="load('jobs/search', 'form:#search_form', 'form:#search_form2'); return false"
                                       name="locations[]"  value="<?= $location->id ?>" />
                                <span class="jobs-box__checkbox"></span>
                                <?= $location->name ?>
                                <span>(10)</span>
                            </label>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="jobs-box__bottom">
                    <button class="btn">
                        <span>Reset Search</span>
                        <span>Reset Search</span>
                    </button>

                    <button class="submit-btn" type="submit">
                        <i class="icon-arrow-right"></i>
                    </button>
                </div>
            </form>

            <div class="jobs-box__close-btn">
                <span></span>
            </div>

            <div class="jobs-box__job-box" data-aos="fade-left">

                <div id="search_results">
                <?php include_once _SYSDIR_ .'modules/jobs/views/search.php'; ?>
                </div>

                <div class="pagination">
                    <div class="pagination" id="pagination">
                        <?= Pagination::ajax('jobs/search') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>