<?php if ($this->list) { ?>
    <?php foreach ($this->list as $job) { ?>
        <a href="{URL:job/<?= $job->slug ?>}" class="job-card">
            <div class="job-card-sector"><?= $job->sectors ?></div>
            <h3 class="job-card-title title-marker">
                <?= $job->title ?>
            </h3>
            <ul class="job-card-params">
                <li>
                    <div><?= propertiesToString($job->locations) ?></div>
                </li>
                <li>
                    <div><?= ucfirst($job->contract_type) ?></div>
                </li>
                <li>
                    <div><?= $job->sub_sector->name ?></div>
                </li>
            </ul>
            <div class="job-card-description">
                <p>
                    <?= reFilter($job->content_short) ?>
                </p>
            </div>
        </a>
    <?php } ?>
<?php } ?>