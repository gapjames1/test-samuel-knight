<?php if ($this->job) { ?>
<section class="hero-section sector-hero-section">
    <div class="hero-section-bg sector-hero-section-bg">
        <div class="hero-l-gradient"></div>
        <div class="hero-black-layer mx-color"></div>
        <div class="hero-r-gradient"></div>
        <picture>
            <source
                    data-srcset="<?= _SITEDIR_ ?>public/images/energy-home-bg.webp"
                    type="image/webp"
            />
            <img
                    alt=""
                    class="lazyload"
                    data-src="<?= _SITEDIR_ ?>public/images/energy-home-bg.jpg"
            />
        </picture>
    </div>
    <div class="cont">
        <div class="hero-section-caption-wr">
            <div class="hero-section-caption">
                <h1
                        class="hero-section-title title-marker clip-title"
                        data-aos="clip-down"
                >
                    <span> <?= $this->job->title ?> </span>
                </h1>
                <div
                        class="hero-section-tags"
                        data-aos="fade-up"
                        data-aos-delay="300"
                >
                    <span><?= propertiesToString($this->job->locations) ?></span>
                    <span> <?= $this->job->sub_sector->name ?> </span>
                    <span> <?= $this->job->contract_type ?> </span>
                    <span> <?= $this->job->salary_value ?> </span>

                    <span> #<?= $this->job->ref ?> </span>
                </div>
                <div
                        class="hero-section-social"
                        data-aos="fade-up"
                        data-aos-delay="300"
                >
                    <span>Share this job</span>
                    <a href="#" class="icon-linkedin"></a>
                    <a href="#" class="icon-twitter"></a>
                    <a href="#" class="icon-instagram"></a>
                </div>
            </div>
            <div class="hero-sector-icons">
                <div class="sector-icon" data-aos="fade-left">
                    <img
                            alt="hero-sector"
                            src="<?= _SITEDIR_ ?>public/images/hero-sector-icons/hero-sector-energy.svg"
                    />
                </div>
            </div>
        </div>
    </div>
</section>

<div class="job-post__post" data-aos="fade-up" data-aos-delay="300">
    <div class="cont">
        <div class="job-post__wrapper">
            <div class="job-post__text-box">
                <div class="job-post__text">
                    <h2>Job Description</h2>
                    <div>
                        <?= reFilter($this->job->content); ?>
                    </div>
                </div>

                <div
                        class="job-post__social"
                        data-aos="fade-up"
                        data-aos-delay="300"
                >
                    <span>Share this job</span>
                    <a href="#" class="icon-linkedin"></a>
                    <a href="#" class="icon-twitter"></a>
                    <a href="#" class="icon-instagram"></a>
                </div>
            </div>
            <div class="job-post__col">
                <div class="teammate-card">
                    <div class="teammate-card-thumbnail">
                        <picture>
                            <source
                                    data-srcset="<?= _SITEDIR_ ?>data/users/mini_<?= getFileName($this->job->user->image) . '.webp' ?>"
                                    type="image/webp"
                            />
                            <img
                                    alt=""
                                    class="lazyload"
                                    data-src="<?= _SITEDIR_ ?>data/users/<?= $this->job->user->image ?>"
                            />
                        </picture>
                    </div>
                    <div class="teammate-card-text">
                        <h3><?= $this->job->user->firstname ?> <?= $this->job->user->lastname ?></h3>
                        <div class="teammate-card-info">
                            <p><?= $this->job->user->job_title ?></p>
                            <a href="<?= $this->job->user->linkedin ?>">
                                <i class="icon-linkedin"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<section class="drop-cv-section alerts-section">
    <div class="cont">
        <div class="drop-form-wr">
            <h2 class="title title-marker">Get Job Alerts</h2>
            <form action="#" class="drop-cv-form">
                <div class="input-wr">
                    <input name="emal" placeholder="Email" type="email" />
                </div>
                <div class="select-wr">
                    <select
                            class="select2-item hidden"
                            data-placeholder="Select location"
                    >
                        <option></option>
                        <option value="Energy">Energy</option>
                        <option value="Rail">Rail</option>
                        <option value="Projects">Projects</option>
                        <option value="Tech">Tech</option>
                    </select>
                </div>
                <div class="select-wr">
                    <select class="select2-item hidden" data-placeholder="Sector">
                        <option></option>
                        <option value="Energy">Energy</option>
                        <option value="Rail">Rail</option>
                        <option value="Projects">Projects</option>
                        <option value="Tech">Tech</option>
                    </select>
                </div>
                <div class="select-wr">
                    <select
                            class="select2-item hidden"
                            data-placeholder="Sub sector"
                    >
                        <option></option>
                        <option value="Energy">Energy</option>
                        <option value="Rail">Rail</option>
                        <option value="Projects">Projects</option>
                        <option value="Tech">Tech</option>
                    </select>
                </div>

                <button class="submit-btn" type="submit">
                    <i class="icon-arrow-right"></i>
                </button>
            </form>
        </div>
    </div>
</section>
<?php if ($this->jobs) { ?>
<section class="latest-job-section">
    <div class="cont">
        <div class="title-wr flex justify-between items-center">
            <h2 class="title title-marker clip-title" data-aos="clip-down">
                <span>Related Jobs</span>
            </h2>
            <a class="btn" href="{LINK:jobs}"
            ><span>All Jobs</span><span>All Jobs</span></a
            >
        </div>
        <div class="latest-job-swiper-wr">
            <div class="latest-job-swiper swiper">
                <div class="swiper-wrapper">
                    <?php foreach ($this->jobs as $job) { ?>
                    <div class="swiper-slide" data-aos="fade-down">
                        <a href="{URL:job/<?= $job->slug ?>}" class="job-card">
                            <div class="job-card-sector"><?= $job->sector->name ?></div>
                            <h3 class="job-card-title title-marker">
                                <?= $job->title ?>
                            </h3>
                            <ul class="job-card-params">
                            <?php if ($job->locations) { ?>
                                <li>
                                    <div><?= propertiesToString($job->locations) ?></div>
                                </li>
                            <?php } ?>
                            <?php if ($job->contract_type) { ?>
                                <li>
                                    <div><?= ucfirst($job->contract_type) ?></div>
                                </li>
                            <?php } ?>

                            <?php if ($job->sub_sector->title) { ?>
                                <div><?= $job->sub_sector->title ?></div>
                            <?php } ?>
                            </ul>
                            <div class="job-card-description">
                                <?= reFilter($job->content_short) ?>
                            </div>
                        </a>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</section>
    <?php } ?>
<?php if ($this->blogs) { ?>
<section class="latest-news-section">
    <div class="cont">
        <div class="title-wr">
            <h2 class="title title-marker clip-title" data-aos="clip-down">
                <span>The latest</span>
            </h2>
            <a href="#" class="btn"
            ><span>All News</span><span>All News</span></a
            >
        </div>
        <div class="news-swiper-wr">
            <div class="news-swiper swiper">
                <div class="swiper-wrapper">
                    <?php include 'app/modules/page/system/inc/_blogs.php' ?>
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</section>
<?php } ?>