<?php

class SolutionsController extends Controller
{
    protected $layout = 'layout_panel';

    use Validator;
    use RecordVersion;

    public function indexAction()
    {
        $this->view->list = SolutionsModel::getAll();
        Request::setTitle('Solutions');
    }

    public function addAction()
    {
        if ($this->startValidation()) {
            $this->validatePost('title', 'Title', 'required|trim|min_length[1]|max_length[255]');

            if ($this->isValid()) {
                $data = [
                    'title' => post('title'),
                    'time'  => time()
                ];

                $result   = Model::insert('solutions', $data); // Insert row
                $insertID = Model::insertID();

                if (!$result && $insertID) {

                    $this->makeVersion($insertID, 'solutions', 'add');

                    Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'add', 'entity' => 'solutions#' . $insertID, 'time' => time()]);

                    Request::addResponse('redirect', false, url('panel', 'solutions', 'edit', $insertID));
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax()) {
                    Request::returnErrors($this->validationErrors);
                }
            }
        }

        Request::setTitle('Add Solutions');
    }

    public function editAction()
    {
        $id = intval(Request::getUri(0));
        $this->view->edit = SolutionsModel::get($id);

        if (!$this->view->edit) {
            redirect(url('panel/solutions'));
        }

        if ($this->startValidation()) {
            $this->validatePost('title',   'Title',           'required|trim|min_length[1]|max_length[200]');
            $this->validatePost('content', 'Content Page',    'required|trim|min_length[1]|max_length[500]');
            $this->validatePost('image',   'Image',           'required|trim|min_length[1]|max_length[200]');

            if ($this->isValid()) {
                $data = array(
                    'title'     => post('title'),
                    'content'   => post('content'),
                    'image'     => post('image'),
                    'time'      => time()
                );

                if ($this->view->solutions->image !== $data['image'] && $data['image']) {
                    if (File::copy('data/tmp/' . $data['image'], 'data/solutions/' . $data['image'])) {
                        File::remove('data/solutions/' . $this->view->solutions->image);
                        File::remove('data/solutions/mini_' . $this->view->solutions->image);
                        File::resize(
                            _SYSDIR_ . 'data/solutions/' . $data['image'],
                            _SYSDIR_ . 'data/solutions/mini_' . $data['image'],
                            420, 280
                        );
                        File::convertToWebP("app/data/solutions/" . $data['image']);
                    } else
                        Request::returnError('Image copy error: ' . error_get_last()['message']);
                }

                $result = Model::update('solutions', $data, "`id` = '$id'");

                if ($result) {
                    $this->makeVersion($id, 'solutions', 'edit');

                    //save alt attributes
                    Image::saveAlts(post('alt_attributes'), 'solutions', $id);

                    Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'edit', 'entity' => 'solutions#' . $id, 'time' => time()]);

                    Request::addResponse('func', 'noticeSuccess', 'Saved');
                    Request::endAjax();
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax())
                    Request::returnErrors($this->validationErrors);
            }
        }

        Request::setTitle('Edit solutions');
    }

    public function deleteAction()
    {
        Request::ajaxPart();

        $id = intval(Request::getUri(0));
        $item = SolutionsModel::get($id);

        if (!$item) {
            Request::returnError('Solutions error');
        }

        $data['deleted'] = 'yes';
        $result = Model::update('solutions', $data, "`id` = '$id'"); // Update row

        if ($result) {

            $this->makeVersion($id, 'solutions', 'delete');

            Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'delete', 'entity' => 'solutions#' . $id, 'time' => time()]);

            Request::addResponse('func', 'noticeSuccess', 'Removed');
            Request::addResponse('remove', '#item_' . $id);
            Request::endAjax();
        } else {
            Request::returnError('Database error');
        }
    }

    public function archiveAction()
    {
        $this->view->list = SolutionsModel::getArchived();

        Request::setTitle('Archive Solutions');
    }

    public function resumeAction()
    {
        $id = Request::getUri(0);
        $item = SolutionsModel::get($id);

        if (!$item) {
            redirect(url('panel/solutions/archive'));
        }

        $result = Model::update('solutions', ['deleted' => 'no'], "`id` = '$id'"); // Update row

        if ($result) {
            Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'restore', 'entity' => 'solutions#' . $id, 'time' => time()]);
        } else {
            Request::returnError('Database error');
        }

        redirect(url('panel/solutions/archive'));
    }
}
