<?php

class SolutionsModel extends Model
{
    public $version = 0; // increment it for auto-update

    /**
     * Method module_install start automatically if it not exist in `modules` table at first importing of model
     */
    public function module_install()
    {
        $queries = [
            "CREATE TABLE IF NOT EXISTS `solutions` (
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `title` varchar(200) NOT NULL,
                `image` varchar(200) DEFAULT NULL,
                `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                `deleted` enum('no','yes') DEFAULT 'no',
                `time` int(10) unsigned NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 COLLATE=utf8mb4_unicode_ci ;"
        ];

        foreach ($queries as $query) {
            self::query($query);
        }
    }

    /**
     * Method module_update start automatically if current $version != version in `modules` table, and start from "case 'i'", where i = prev version in modules` table
     * @param int $version
     */
    public function module_update($version)
    {
        $queries = [];

        switch ($version) {
        }

        foreach ($queries as $query) {
            self::query($query);
        }
    }

    /**
     * Get solutions by $id
     * @param $id
     * @return array|object|null
     */
    public static function get($id)
    {
        $sql = "
            SELECT *
            FROM `solutions`
            WHERE `id` = '$id'
            LIMIT 1
        ";

        $solutions = self::fetch(self::query($sql));

        if ($solutions)
            $solutions = Image::getAlts('solutions', $solutions);

        return $solutions;


    }

    /**
     * Get all
     * @return array
     */
    public static function getAll()
    {
        $sql = "
            SELECT *
            FROM `solutions`
            WHERE `deleted` = 'no'
        ";

        $solutions = self::fetchAll(self::query($sql));

        if ($solutions)
            $solutions = Image::getAltsForMany('solutions', $solutions);

        return $solutions;
    }

    public static function getArchived()
    {
        $sql = "
            SELECT *
            FROM `solutions`
            WHERE `deleted` = 'yes'
        ";

        return self::fetchAll(self::query($sql));
    }
}
