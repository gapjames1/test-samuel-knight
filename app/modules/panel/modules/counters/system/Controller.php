<?php

class CountersController extends Controller
{
    protected $layout = 'layout_panel';

    use Validator;
    use RecordVersion;

    public function indexAction()
    {
        $this->view->list = CountersModel::getAll();
        Request::setTitle('Counters');
    }

    public function addAction()
    {
        if ($this->startValidation()) {
            $this->validatePost('numbers', 'Number', 'required|trim|min_length[1]|max_length[200]');

            if ($this->isValid()) {
                $data = [
                    'numbers' => post('numbers'),
                    'time'  => time()
                ];

                $result   = Model::insert('counters', $data); // Insert row
                $insertID = Model::insertID();

                if (!$result && $insertID) {

                    $this->makeVersion($insertID, 'counters', 'add');

                    Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'add', 'entity' => 'counters#' . $insertID, 'time' => time()]);

                    Request::addResponse('redirect', false, url('panel', 'counters', 'edit', $insertID));
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax()) {
                    Request::returnErrors($this->validationErrors);
                }
            }
        }

        Request::setTitle('Add Counters');
    }

    public function editAction()
    {
        $id = intval(Request::getUri(0));
        $this->view->edit = CountersModel::get($id);

        if (!$this->view->edit) {
            redirect(url('panel/counters'));
        }

        if ($this->startValidation()) {
            $this->validatePost('numbers',  'Numbers',     'required|trim|min_length[1]|max_length[200]');
            $this->validatePost('content',  'content',     'required|trim|min_length[1]|max_length[200]');
            $this->validatePost('suffix',   'suffix',      'required|trim|min_length[1]|max_length[1]');

            if ($this->isValid()) {
                $data = array(
                    'numbers'   => post('numbers'),
                    'content'   => post('content'),
                    'suffix'    => post('suffix'),
                    'pages'     => arrayToString(post('pages') ?: []),
                    'time'      => time()
                );

                $result = Model::update('counters', $data, "`id` = '$id'");

                if ($result) {
                    //Versions
                    $this->makeVersion($id, 'counters', 'edit');

                    Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'edit', 'entity' => 'counters#' . $id, 'time' => time()]);

                    Request::addResponse('func', 'noticeSuccess', 'Saved');
                    Request::endAjax();
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax()) {
                    Request::returnErrors($this->validationErrors);
                }
            }
        }

        Request::setTitle('Edit counters');
    }

    public function deleteAction()
    {
        Request::ajaxPart();

        $id = intval(Request::getUri(0));
        $item = CountersModel::get($id);

        if (!$item) {
            Request::returnError('Counters error');
        }

        $data['deleted'] = 'yes';
        $result = Model::update('counters', $data, "`id` = '$id'"); // Update row

        if ($result) {

            $this->makeVersion($id, 'counters', 'delete');

            Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'delete', 'entity' => 'counters#' . $id, 'time' => time()]);

            Request::addResponse('func', 'noticeSuccess', 'Removed');
            Request::addResponse('remove', '#item_' . $id);
            Request::endAjax();
        } else {
            Request::returnError('Database error');
        }
    }

    public function archiveAction()
    {
        $this->view->list = CountersModel::getArchived();

        Request::setTitle('Archive Counters');
    }

    public function resumeAction()
    {
        $id = Request::getUri(0);
        $item = CountersModel::get($id);

        if (!$item) {
            redirect(url('panel/counters/archive'));
        }

        $result = Model::update('counters', ['deleted' => 'no'], "`id` = '$id'"); // Update row

        if ($result) {
            Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'restore', 'entity' => 'counters#' . $id, 'time' => time()]);
        } else {
            Request::returnError('Database error');
        }

        redirect(url('panel/counters/archive'));
    }
}
