<?php

class Sub_sectorsController extends Controller
{
    protected $layout = 'layout_panel';

    use Validator, RecordVersion;

    public function indexAction()
    {
        $this->view->list = Sub_sectorsModel::getAll();

        Request::setTitle('Expirence sub_sectors');
    }

    public function archiveAction()
    {
        $this->view->list = Sub_sectorsModel::getArchived();

        Request::setTitle('Archived sub_sectors');
    }

    public function resumeAction()
    {
        $id = (Request::getUri(0));
        $user = Sub_sectorsModel::get($id);

        if (!$user)
            redirect(url('panel/vacancies/sub_sectors/archive'));

        $result = Model::update('sub_sectors', ['deleted' => 'no'], "`id` = '$id'"); // Update row

        if ($result) {
            Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'restore', 'entity' => 'level#' . $id, 'time' => time()]);
        } else {
            Request::returnError('Database error');
        }

        redirect(url('panel/vacancies/sub_sectors/archive'));

    }

    public function addAction()
    {
        if ($this->startValidation()) {
            $this->validatePost('name', 'Name', 'required|trim|min_length[1]|max_length[200]');

            if ($this->isValid()) {
                $data = array(
                    'name' => post('name'),
                );

                $result = Model::insert('sub_sectors', $data); // Insert row
                $insertID = Model::insertID();

                if (!$result && $insertID) {
                    $this->makeVersion($insertID, 'sub_sectors', 'add');

                    Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'add', 'entity' => 'sub_sectors#' . $insertID, 'time' => time()]);

                    //                    $this->session->set_flashdata('success', 'User created successfully.');
                    Request::addResponse('redirect', false, url('panel', 'vacancies', 'sub_sectors', 'edit', $insertID));
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax())
                    Request::returnErrors($this->validationErrors);
            }
        }

        Request::setTitle('Add sub_sectors');
    }

    public function editAction()
    {
        $id = intval(Request::getUri(0));
        $this->view->edit = Sub_sectorsModel::get($id);

        if (!$this->view->edit)
            redirect(url('panel/vacancies/sub_sectors'));

        if ($this->startValidation()) {
            $this->validatePost('name', 'Name', 'required|trim|min_length[1]|max_length[200]');

            if ($this->isValid()) {
                $data = array(
                    'name' => post('name'),
                );

                $result = Model::update('sub_sectors', $data, "`id` = '$id'"); // Update row

                if ($result) {
                    $this->makeVersion($id, 'sub_sectors', 'edit');

                    Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'edit', 'entity' => 'sub_sectors#' . $id, 'time' => time()]);

                    Request::addResponse('func', 'noticeSuccess', 'Saved');
                    Request::endAjax();
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax())
                    Request::returnErrors($this->validationErrors);
            }
        }

        Request::setTitle('Edit sub_sectors');
    }

    public function deleteAction()
    {
        Request::ajaxPart();

        $id = intval(Request::getUri(0));
        $user = Sub_sectorsModel::get($id);

        if (!$user)
            Request::returnError('Archived sub_sectors error');

        $data['deleted'] = 'yes';
        $result = Model::update('sub_sectors', $data, "`id` = '$id'"); // Update row

        if ($result) {
            $this->makeVersion($id, 'sub_sectors', 'delete');

            Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'delete', 'entity' => 'sub_sectors#' . $id, 'time' => time()]);

            Request::addResponse('func', 'noticeSuccess', 'Removed');
            Request::addResponse('remove', '#item_' . $id);
            Request::endAjax();
        } else {
            Request::returnError('Database error');
        }
    }
}
/* End of file */
