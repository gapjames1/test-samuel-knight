<?php

class SectorsController extends Controller
{
    protected $layout = 'layout_panel';

    use Validator, RecordVersion;

    public function indexAction()
    {
        $this->view->list = SectorsModel::getAll();

        Request::setTitle('Expirence sectors');
    }

    public function archiveAction()
    {
        $this->view->list = sectorsModel::getArchived();

        Request::setTitle('Archived sectors');
    }

    public function resumeAction()
    {
        $id = (Request::getUri(0));
        $user = SectorsModel::get($id);

        if (!$user)
            redirect(url('panel/vacancies/sectors/archive'));

        $result = Model::update('sectors', ['deleted' => 'no'], "`id` = '$id'"); // Update row

        if ($result) {
            Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'restore', 'entity' => 'level#' . $id, 'time' => time()]);
        } else {
            Request::returnError('Database error');
        }

        redirect(url('panel/vacancies/sectors/archive'));

    }

    public function addAction()
    {
        if ($this->startValidation()) {
            $this->validatePost('name', 'Name', 'required|trim|min_length[1]|max_length[200]');

            if ($this->isValid()) {
                $data = array(
                    'name' => post('name'),
                );

                $result = Model::insert('sectors', $data); // Insert row
                $insertID = Model::insertID();

                if (!$result && $insertID) {
                    $this->makeVersion($insertID, 'sectors', 'add');

                    Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'add', 'entity' => 'sectors#' . $insertID, 'time' => time()]);

                    //                    $this->session->set_flashdata('success', 'User created successfully.');
                    Request::addResponse('redirect', false, url('panel', 'vacancies', 'sectors', 'edit', $insertID));
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax())
                    Request::returnErrors($this->validationErrors);
            }
        }

        Request::setTitle('Add sectors');
    }

    public function editAction()
    {
        $id = intval(Request::getUri(0));
        $this->view->edit = sectorsModel::get($id);

        if (!$this->view->edit)
            redirect(url('panel/vacancies/sectors'));

        if ($this->startValidation()) {
            $this->validatePost('name',    'Name',            'required|trim|min_length[1]|max_length[200]');
            $this->validatePost('image',   'Image',           'required|trim|min_length[1]|max_length[200]');

            if ($this->isValid()) {
                $data = array(
                    'name'    => post('name'),
                    'image'   => post('image'),
                );

                if ($this->view->sectors->image !== $data['image'] && $data['image']) {
                    if (File::copy('data/tmp/' . $data['image'], 'data/sectors/' . $data['image'])) {
                        File::remove('data/sectors/' . $this->view->sectors->image);
                        File::remove('data/sectors/mini_' . $this->view->sectors->image);
                        File::resize(
                            _SYSDIR_ . 'data/sectors/' . $data['image'],
                            _SYSDIR_ . 'data/sectors/mini_' . $data['image'],
                            420, 280
                        );
                        File::convertToWebP("app/data/sectors/" . $data['image']);
                    } else
                        Request::returnError('Image copy error: ' . error_get_last()['message']);
                }

                $result = Model::update('sectors', $data, "`id` = '$id'");

                if ($result) {
                    $this->makeVersion($id, 'sectors', 'edit');

                    //save alt attributes
                    Image::saveAlts(post('alt_attributes'), 'sectors', $id);

                    Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'edit', 'entity' => 'sectors#' . $id, 'time' => time()]);

                    Request::addResponse('func', 'noticeSuccess', 'Saved');
                    Request::endAjax();
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax())
                    Request::returnErrors($this->validationErrors);
            }
        }

        Request::setTitle('Edit sectors');
    }

    public function deleteAction()
    {
        Request::ajaxPart();

        $id = intval(Request::getUri(0));
        $user = sectorsModel::get($id);

        if (!$user)
            Request::returnError('Archived sectors error');

        $data['deleted'] = 'yes';
        $result = Model::update('sectors', $data, "`id` = '$id'"); // Update row

        if ($result) {
            $this->makeVersion($id, 'sectors', 'delete');

            Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'delete', 'entity' => 'sectors#' . $id, 'time' => time()]);

            Request::addResponse('func', 'noticeSuccess', 'Removed');
            Request::addResponse('remove', '#item_' . $id);
            Request::endAjax();
        } else {
            Request::returnError('Database error');
        }
    }
}
/* End of file */
