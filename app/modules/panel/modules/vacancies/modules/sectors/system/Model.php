<?php

class sectorsModel extends Model
{
    public $version = 0; // increment it for auto-update

    /**
     * Method module_install start automatically if it not exist in `modules` table at first importing of model
     */
    public function module_install()
    {
        $queries = array(
            "CREATE TABLE IF NOT EXISTS `sectors` (
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `name` varchar(200) NOT NULL,
                `image` varchar(200) DEFAULT NULL,
                `deleted` enum('no','yes') DEFAULT 'no',
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 COLLATE=utf8mb4_unicode_ci ;"
        );

        foreach ($queries as $query)
            self::query($query);
    }

    /**
     * Method module_update start automatically if current $version != version in `modules` table, and start from "case 'i'", where i = prev version in modules` table
     * @param int $version
     */
    public function module_update($version)
    {
        $queries = array();

        switch ($version) {
//            case '0':
//                $queries[] = "ALTER TABLE `sectors` ADD COLUMN `subtitle` varchar(200) DEFAULT NULL AFTER `name`;";
        }

        foreach ($queries as $query)
            self::query($query);
    }

    /**
     * Get user by $id
     * @param $id
     * @return array|object|null
     */
    public static function get($id)
    {
        $sql = "
            SELECT *
            FROM `sectors`
            WHERE `id` = '$id'
            LIMIT 1
        ";

        $sectors = self::fetch(self::query($sql));

        if ($sectors)
            $sectors = Image::getAlts('sectors', $sectors);

        return $sectors;
    }

    /**
     * Get all
     * @return array
     */
    public static function getAll()
    {
        $sql = "
            SELECT *
            FROM `sectors`
            WHERE `deleted` = 'no'
        ";

        $sectors = self::fetchAll(self::query($sql));

        if ($sectors)
            $sectors = Image::getAltsForMany('sectors', $sectors);

        return $sectors;
    }

    public static function getArchived()
    {
        $sql = "
            SELECT *
            FROM `sectors`
            WHERE `deleted` = 'yes'
        ";

        return self::fetchAll(self::query($sql));
    }
}

/* End of file */