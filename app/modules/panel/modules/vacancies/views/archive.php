<!--LOADER-->
<div id="loader_fon">
    <div id="loader"></div>
</div>
<!--END LOADER-->

<div class="layout-px-spacing">
    <div class="row layout-top-spacing">

        <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
            <div class="widget-content widget-content-area br-6">

                <!-- Head -->
                <div class="flex-btw flex-vc mob_fc">
                    <h1>
                        Manage Vacancies <?= (isset($this->microsite)) ? 'for ' . $this->microsite->title : '' ?>
                    </h1>

                    <a class="btn btn-primary mb-2 mr-2" href="{URL:panel/vacancies}">
                        <i class="fas fa-database"></i>
                        Active <span class="hide_block768">Vacancies</span>
                    </a>
                </div>

                <!-- Table -->
                <div class="table-responsive mb-4 mt-4">
                    <table id="zero-config" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th>Ref</th>
                                <th class="min_w_name">Job Title</th>
                                <th>Sector</th>
                                <th>Location</th>
                                <th>Views</th>
                                <th>Applies</th>
                                <th>Posted</th>
                                <th>Reason</th>
                                <th>Expire</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>
</div>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?= _SITEDIR_ ?>public/plugins/datatable/datatables.js"></script>
<script>
    $(function () {
        var table = $('#zero-config').DataTable({
            'processing': true,
            'serverSide': true,
            'ajax': {
                'url':'<?= SITE_URL ?>/panel/vacancies/pagination',
                'type': 'POST',
                'data': {
                    'method': 'archive'
                }
            },
            "createdRow": function( row, data ) {
                $(row).addClass( 'tr-hovered' );
                $(row).attr('id', 'item_' + data['id']);
            },
            'columns': [
                { data: 'ref' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (data, type, full) {
                        return `<a href="<?= SITE_URL ?>panel/vacancies/edit/${full.id}">${full.title}</a>`;
                    }
                },
                { data: 'sectors' },
                { data: 'locations' },
                { data: 'views' },
                { data: 'applies' },
                { data: 'posted' },
                { data: 'expire_reason' },
                { data: 'time_expire' },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (data, type, full) {
                        console.log(full)
                        return `
                            <td>
                                <div class="items-row align-items-center">
                                    <div class="dropdown dropup custom-dropdown-icon">
                                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink-3">
                                            <a href="<?= SITE_URL ?>panel/vacancies/edit/${full.id}" class="dropdown-item"><i class="fa fa-pencil-alt"></i> Edit</a>
                                            ${full.deleted === 'yes' ? '<a href="<?= SITE_URL ?>panel/vacancies/resume/' + full.id + '" class="dropdown-item restore-item"><i class="fa fa-trash-restore"></i> Re-live</a>' : ''}
                                        </div>
                                    </div>

                                    <div class="btns-list">
                                        ${full.deleted === 'yes' ? '<a href="<?= SITE_URL ?>panel/vacancies/resume/' + full.id + '" class="btn-rectangle bs-tooltip fa fa-trash-restore restore-item" title="Re-live"></a>' : ''}
                                    </div>
                                </div>
                            </td>`;
                    }
                }
            ],
            "oLanguage": {
                "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
                "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [2, 5, 25, 50, 100],
            "pageLength": 25
        });

        table.on('draw', function () {
            if (!!$.prototype.confirm) {
                // Remove confirmation
                $(".restore-item").confirm({
                    buttons: {
                        tryAgain: {
                            text: "Yes, restore",
                            btnClass: "btn-red",
                            action: function () {
                                const link = this.$target.attr("@click");
                                console.log("Clicked tooltip");

                                if (typeof link === "undefined") {
                                    location.href = this.$target.attr("href");
                                } else {
                                    eval(link);
                                }
                            },
                        },
                        cancel: function () {},
                    },
                    icon: "fas fa-trash-restore",
                    title: "Are you sure?",
                    content:
                        "Are you sure you wish to delete this item? Please re-confirm this action.",
                    type: "green",
                    typeAnimated: true,
                    boxWidth: "30%",
                    useBootstrap: false,
                    theme: "modern",
                    animation: "scale",
                    backgroundDismissAnimation: "shake",
                    draggable: false,
                });
            }
        });
    });
</script>
<!-- END PAGE LEVEL SCRIPTS -->
