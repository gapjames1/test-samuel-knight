<?php

class We_workController extends Controller
{
    protected $layout = 'layout_panel';

    use Validator;
    use RecordVersion;

    public function indexAction()
    {
        $this->view->list = We_workModel::getAll();
        Request::setTitle('We work');
    }

    public function addAction()
    {
        if ($this->startValidation()) {
            $this->validatePost('title', 'Title', 'required|trim|min_length[1]|max_length[255]');

            if ($this->isValid()) {
                $data = [
                    'title' => post('title'),
                    'time'  => time()
                ];

                $result   = Model::insert('we_work', $data); // Insert row
                $insertID = Model::insertID();

                if (!$result && $insertID) {

                    $this->makeVersion($insertID, 'we_work', 'add');

                    Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'add', 'entity' => 'we_work#' . $insertID, 'time' => time()]);

                    Request::addResponse('redirect', false, url('panel', 'we_work', 'edit', $insertID));
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax()) {
                    Request::returnErrors($this->validationErrors);
                }
            }
        }

        Request::setTitle('Add We work');
    }

    public function editAction()
    {
        $id = intval(Request::getUri(0));
        $this->view->edit = We_workModel::get($id);

        if (!$this->view->edit) {
            redirect(url('panel/we_work'));
        }

        if ($this->startValidation()) {
            $this->validatePost('title', 'Title', 'required|trim|min_length[1]|max_length[200]');
            $this->validatePost('image', 'Image', 'required|trim|min_length[1]|max_length[200]');

            if ($this->isValid()) {
                $data = array(
                    'title' => post('title'),
                    'image' => post('image'),
                    'pages' => arrayToString(post('pages') ?: []),
                    'time'  => time()
                );

                if ($this->view->we_work->image !== $data['image'] && $data['image']) {
                    if (File::copy('data/tmp/' . $data['image'], 'data/we_work/' . $data['image'])) {
                        File::remove('data/we_work/' . $this->view->we_work->image);
                        File::remove('data/we_work/mini_' . $this->view->we_work->image);
                        File::resize(
                            _SYSDIR_ . 'data/we_work/' . $data['image'],
                            _SYSDIR_ . 'data/we_work/mini_' . $data['image'],
                            420, 280
                        );
                        File::convertToWebP("app/data/we_work/" . $data['image']);
                    } else
                        Request::returnError('Image copy error: ' . error_get_last()['message']);
                }

                $result = Model::update('we_work', $data, "`id` = '$id'"); // Update row

                if ($result) {
                    $this->makeVersion($id, 'we_work', 'edit');

                    //save alt attributes
                    Image::saveAlts(post('alt_attributes'), 'we_work', $id);

                    Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'edit', 'entity' => 'we_work#' . $id, 'time' => time()]);

                    Request::addResponse('func', 'noticeSuccess', 'Saved');
                    Request::endAjax();
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax())
                    Request::returnErrors($this->validationErrors);
            }
        }

        Request::setTitle('Edit we_work');
    }

    public function deleteAction()
    {
        Request::ajaxPart();

        $id = intval(Request::getUri(0));
        $item = We_workModel::get($id);

        if (!$item) {
            Request::returnError('We work error');
        }

        $data['deleted'] = 'yes';
        $result = Model::update('we_work', $data, "`id` = '$id'"); // Update row

        if ($result) {

            $this->makeVersion($id, 'we_work', 'delete');

            Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'delete', 'entity' => 'we_work#' . $id, 'time' => time()]);

            Request::addResponse('func', 'noticeSuccess', 'Removed');
            Request::addResponse('remove', '#item_' . $id);
            Request::endAjax();
        } else {
            Request::returnError('Database error');
        }
    }

    public function archiveAction()
    {
        $this->view->list = We_workModel::getArchived();

        Request::setTitle('Archive Partners');
    }

    public function resumeAction()
    {
        $id = Request::getUri(0);
        $item = We_workModel::get($id);

        if (!$item) {
            redirect(url('panel/we_work/archive'));
        }

        $result = Model::update('we_work', ['deleted' => 'no'], "`id` = '$id'"); // Update row

        if ($result) {
            Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'restore', 'entity' => 'we_work#' . $id, 'time' => time()]);
        } else {
            Request::returnError('Database error');
        }

        redirect(url('panel/we_work/archive'));
    }
}
