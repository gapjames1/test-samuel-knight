<form id="form_box" action="{URL:panel/we_work/edit/<?= $this->edit->id; ?>}" method="post" enctype="multipart/form-data">
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">

            <!-- Title ROW -->
            <div class="col-xl-12 col-lg-12 col-sm-12 layout-spacing">
                <div class="statbox widget box box-shadow widget-top">
                    <div class="widget-header">

                        <div class="items_group items_group-wrap">
                            <div class="items_left-side">
                                <div class="title-block">
                                    <a class="btn-ellipse bs-tooltip fas fa-rss" href="{URL:panel/we_work}"></a>
                                    <h1 class="page_title"><?= $this->edit->title ?></h1>
                                </div>
                            </div>

                            <div class="items_right-side">
                                <div class="items_small-block">

                                </div>

                                <a class="btn btn-outline-warning" href="{URL:panel/we_work}"><i class="fas fa-reply"></i>Back</a>
                            </div>
                        </div>

                        <div class="items_group items_group-wrap items_group-bottom">
                            <div class="items_left-side">
                                <div class="option-btns-list scroll-list">
                                    <a class="btn btn-rectangle_medium active"><i class="bs-tooltip fa fa-pencil-alt"></i>Edit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- General -->
            <div id="flFormsGrid" class="col-lg-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <h4>General</h4>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" name="title" id="title" value="<?= post('title', false, $this->edit->title); ?>">
                            </div>

                            <div class="fixed">
                                <label>Pages</label>
                                <div class="form-check scroll_max_200 border_1">
                                    <div class="custom-control custom-checkbox checkbox-info">
                                        <input class="custom-control-input" type="checkbox" name="pages[]"
                                               id="energy" value="energy"
                                            <?= checkCheckboxValue(post('energy'), 'energy', stringToArray($this->edit->pages)); ?>
                                        ><label class="custom-control-label" for="energy">Energy</label>
                                    </div>

                                    <div class="custom-control custom-checkbox checkbox-info">
                                        <input class="custom-control-input" type="checkbox" name="pages[]"
                                               id="tech" value="tech"
                                            <?= checkCheckboxValue(post('tech'), 'tech', stringToArray($this->edit->pages)); ?>
                                        ><label class="custom-control-label" for="tech">Tech</label>
                                    </div>

                                    <div class="custom-control custom-checkbox checkbox-info">
                                        <input class="custom-control-input" type="checkbox" name="pages[]"
                                               id="rail" value="rail"
                                            <?= checkCheckboxValue(post('rail'), 'rail', stringToArray($this->edit->pages)); ?>
                                        ><label class="custom-control-label" for="rail">Rail</label>
                                    </div>

                                    <div class="custom-control custom-checkbox checkbox-info">
                                        <input class="custom-control-input" type="checkbox" name="pages[]"
                                               id="projects" value="projects"
                                            <?= checkCheckboxValue(post('projects'), 'projects', stringToArray($this->edit->pages)); ?>
                                        ><label class="custom-control-label" for="projects">Projects</label>
                                    </div>

                                    <div class="custom-control custom-checkbox checkbox-info">
                                        <input class="custom-control-input" type="checkbox" name="pages[]"
                                               id="clients" value="clients"
                                            <?= checkCheckboxValue(post('clients'), 'clients', stringToArray($this->edit->pages)); ?>
                                        ><label class="custom-control-label" for="clients">Clients</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <?= Image::createField(
                                'image',
                                "'panel/select_image', 'field=#image'",
                                $this->edit->image,
                                _SITEDIR_ . 'data/we_work/',
                                $this->edit->alt_attributes['image']
                            )?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Save Buttons -->
            <div class="col-xl-12 col-lg-12 col-sm-12 layout-spacing sticky-block-bottom">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header widget-bottom">
                        <a class="btn btn-outline-warning" href="{URL:panel/we_work}"><i class="fas fa-reply"></i>Back</a>
                        <a class="btn btn-success" onclick="
                                load('panel/we_work/edit/<?= $this->edit->id; ?>', 'form:#form_box'); return false;">
                            <i class="fas fa-save"></i>Save Changes
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</form>
