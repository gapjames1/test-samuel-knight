<?php

class PartnersController extends Controller
{
    protected $layout = 'layout_panel';

    use Validator;
    use RecordVersion;

    public function indexAction()
    {
        $this->view->list = PartnersModel::getAll();
        Request::setTitle('Partners');
    }

    public function addAction()
    {
        if ($this->startValidation()) {
            $this->validatePost('title', 'Title', 'required|trim|min_length[1]|max_length[255]');

            if ($this->isValid()) {
                $data = [
                    'title' => post('title'),
                    'time'  => time()
                ];

                $result   = Model::insert('partners', $data); // Insert row
                $insertID = Model::insertID();

                if (!$result && $insertID) {

                    $this->makeVersion($insertID, 'partners', 'add');

                    Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'add', 'entity' => 'partners#' . $insertID, 'time' => time()]);

                    Request::addResponse('redirect', false, url('panel', 'partners', 'edit', $insertID));
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax()) {
                    Request::returnErrors($this->validationErrors);
                }
            }
        }

        Request::setTitle('Add Partners');
    }

    public function editAction()
    {
        $id = intval(Request::getUri(0));
        $this->view->edit = PartnersModel::get($id);

        if (!$this->view->edit) {
            redirect(url('panel/partners'));
        }

        if ($this->startValidation()) {
            $this->validatePost('title', 'Title', 'required|trim|min_length[1]|max_length[200]');
            $this->validatePost('image', 'Image', 'required|trim|min_length[1]|max_length[200]');

            if ($this->isValid()) {
                $data = array(
                    'title' => post('title'),
                    'image' => post('image'),
                    'pages' => arrayToString(post('pages') ?: []),
                    'time'  => time()
                );

                if ($this->view->partners->image !== $data['image'] && $data['image']) {
                    if (File::copy('data/tmp/' . $data['image'], 'data/partners/' . $data['image'])) {
                        File::remove('data/partners/' . $this->view->partners->image);
                        File::remove('data/partners/mini_' . $this->view->partners->image);
                        File::resize(
                            _SYSDIR_ . 'data/partners/' . $data['image'],
                            _SYSDIR_ . 'data/partners/mini_' . $data['image'],
                            420, 280
                        );
                        File::convertToWebP("app/data/partners/" . $data['image']);
                    } else
                        Request::returnError('Image copy error: ' . error_get_last()['message']);
                }

                $result = Model::update('partners', $data, "`id` = '$id'"); // Update row

                if ($result) {
                    $this->makeVersion($id, 'partners', 'edit');

                    //save alt attributes
                    Image::saveAlts(post('alt_attributes'), 'partners', $id);

                    Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'edit', 'entity' => 'partners#' . $id, 'time' => time()]);

                    Request::addResponse('func', 'noticeSuccess', 'Saved');
                    Request::endAjax();
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax())
                    Request::returnErrors($this->validationErrors);
            }
        }

        Request::setTitle('Edit partners');
    }

    public function deleteAction()
    {
        Request::ajaxPart();

        $id = intval(Request::getUri(0));
        $item = PartnersModel::get($id);

        if (!$item) {
            Request::returnError('Partners error');
        }

        $data['deleted'] = 'yes';
        $result = Model::update('partners', $data, "`id` = '$id'"); // Update row

        if ($result) {

            $this->makeVersion($id, 'partners', 'delete');

            Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'delete', 'entity' => 'partners#' . $id, 'time' => time()]);

            Request::addResponse('func', 'noticeSuccess', 'Removed');
            Request::addResponse('remove', '#item_' . $id);
            Request::endAjax();
        } else {
            Request::returnError('Database error');
        }
    }

    public function archiveAction()
    {
        $this->view->list = PartnersModel::getArchived();

        Request::setTitle('Archive Partners');
    }

    public function resumeAction()
    {
        $id = Request::getUri(0);
        $item = PartnersModel::get($id);

        if (!$item) {
            redirect(url('panel/partners/archive'));
        }

        $result = Model::update('partners', ['deleted' => 'no'], "`id` = '$id'"); // Update row

        if ($result) {
            Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'restore', 'entity' => 'partners#' . $id, 'time' => time()]);
        } else {
            Request::returnError('Database error');
        }

        redirect(url('panel/partners/archive'));
    }
}
