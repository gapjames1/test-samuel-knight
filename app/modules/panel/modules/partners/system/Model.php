<?php

class PartnersModel extends Model
{
    public $version = 0; // increment it for auto-update

    /**
     * Method module_install start automatically if it not exist in `modules` table at first importing of model
     */
    public function module_install()
    {
        $queries = [
            "CREATE TABLE IF NOT EXISTS `partners` (
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `title` varchar(200) NOT NULL,
                `image` varchar(200) DEFAULT NULL,
                `pages` varchar(255) DEFAULT NULL,
                `deleted` enum('no','yes') DEFAULT 'no',
                `time` int(10) unsigned NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 COLLATE=utf8mb4_unicode_ci ;"
        ];

        foreach ($queries as $query) {
            self::query($query);
        }
    }

    /**
     * Method module_update start automatically if current $version != version in `modules` table, and start from "case 'i'", where i = prev version in modules` table
     * @param int $version
     */
    public function module_update($version)
    {
        $queries = [];

        switch ($version) {
        }

        foreach ($queries as $query) {
            self::query($query);
        }
    }

    /**
     * Get partners by $id
     * @param $id
     * @return array|object|null
     */
    public static function get($id)
    {
        $sql = "
            SELECT *
            FROM `partners`
            WHERE `id` = '$id'
            LIMIT 1
        ";

        $partners = self::fetch(self::query($sql));

        if ($partners)
            $partners = Image::getAlts('partners', $partners);

        return $partners;
    }

    /**
     * Get all
     * @return array
     */
    public static function getAll()
    {
        $sql = "
            SELECT *
            FROM `partners`
            WHERE `deleted` = 'no'
        ";

        $partners = self::fetchAll(self::query($sql));

        if ($partners)
            $partners = Image::getAltsForMany('partners', $partners);

        return $partners;
    }

    public static function getArchived()
    {
        $sql = "
            SELECT *
            FROM `partners`
            WHERE `deleted` = 'yes'
        ";

        return self::fetchAll(self::query($sql));
    }

    public static function getByPage($page)
    {
        $sql = "
            SELECT *
            FROM `partners`
            WHERE `pages` LIKE '%$page%' AND `deleted` = 'no'
        ";

        return self::fetchAll(self::query($sql));
    }
}
