<?php

class StoriesController extends Controller
{
    protected $layout = 'layout_panel';

    use Validator;
    use RecordVersion;

    public function indexAction()
    {
        $this->view->list = StoriesModel::getAll();
        Request::setTitle('Stories');
    }

    public function addAction()
    {
        if ($this->startValidation()) {
            $this->validatePost('title', 'Title', 'required|trim|min_length[1]|max_length[255]');

            if ($this->isValid()) {
                $data = [
                    'title' => post('title'),
                    'time'  => time()
                ];

                $result   = Model::insert('stories', $data); // Insert row
                $insertID = Model::insertID();

                if (!$result && $insertID) {

                    $this->makeVersion($insertID, 'stories', 'add');

                    Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'add', 'entity' => 'stories#' . $insertID, 'time' => time()]);

                    Request::addResponse('redirect', false, url('panel', 'stories', 'edit', $insertID));
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax()) {
                    Request::returnErrors($this->validationErrors);
                }
            }
        }

        Request::setTitle('Add Stories');
    }

    public function editAction()
    {
        $id = intval(Request::getUri(0));
        $this->view->edit = StoriesModel::get($id);

        if (!$this->view->edit) {
            redirect(url('panel/stories'));
        }

        if ($this->startValidation()) {
            $this->validatePost('title',            'Title',                      'required|trim|min_length[1]|max_length[200]');
            $this->validatePost('content',          'Page Content',               'required|trim|min_length[0]|max_length[500]');
            $this->validatePost('year',             'Year',                       'required|trim|min[1]|max_length[4]');

            if ($this->isValid()) {
                $data = array(
                    'title'         => post('title'),
                    'content'       => post('content'),
                    'year'          => post('year', 'int', 0),
                    'pages'         => arrayToString(post('pages') ?: []),
                    'time'          => time()
                );

                $result = Model::update('stories', $data, "`id` = '$id'");

                if ($result) {
                    //Versions
                    $this->makeVersion($id, 'stories', 'edit');

                    Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'edit', 'entity' => 'stories#' . $id, 'time' => time()]);

                    Request::addResponse('func', 'noticeSuccess', 'Saved');
                    Request::endAjax();
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax()) {
                    Request::returnErrors($this->validationErrors);
                }
            }
        }

        Request::setTitle('Edit stories');
    }

    public function deleteAction()
    {
        Request::ajaxPart();

        $id = intval(Request::getUri(0));
        $item = StoriesModel::get($id);

        if (!$item) {
            Request::returnError('Stories error');
        }

        $data['deleted'] = 'yes';
        $result = Model::update('stories', $data, "`id` = '$id'"); // Update row

        if ($result) {

            $this->makeVersion($id, 'stories', 'delete');

            Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'delete', 'entity' => 'stories#' . $id, 'time' => time()]);

            Request::addResponse('func', 'noticeSuccess', 'Removed');
            Request::addResponse('remove', '#item_' . $id);
            Request::endAjax();
        } else {
            Request::returnError('Database error');
        }
    }

    public function archiveAction()
    {
        $this->view->list = StoriesModel::getArchived();

        Request::setTitle('Archive Stories');
    }

    public function resumeAction()
    {
        $id = Request::getUri(0);
        $item = StoriesModel::get($id);

        if (!$item) {
            redirect(url('panel/stories/archive'));
        }

        $result = Model::update('stories', ['deleted' => 'no'], "`id` = '$id'"); // Update row

        if ($result) {
            Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'restore', 'entity' => 'stories#' . $id, 'time' => time()]);
        } else {
            Request::returnError('Database error');
        }

        redirect(url('panel/stories/archive'));
    }
}
