<?php

class {{ className }}Controller extends Controller
{
    protected $layout = 'layout_panel';

    use Validator;
    use RecordVersion;

    public function indexAction()
    {
        $this->view->list = {{ className }}Model::getAll();
        Request::setTitle('{{ className }}');
    }

    public function addAction()
    {
        if ($this->startValidation()) {
            $this->validatePost('title', 'Title', 'required|trim|min_length[1]|max_length[255]');

            if ($this->isValid()) {
                $data = [
                    'title' => post('title'),
                    'time'  => time()
                ];

                $result   = Model::insert('{{ tableName }}', $data); // Insert row
                $insertID = Model::insertID();

                if (!$result && $insertID) {

                    $this->makeVersion($insertID, '{{ tableName }}', 'add');

                    Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'add', 'entity' => '{{ classNamePath }}#' . $insertID, 'time' => time()]);

                    Request::addResponse('redirect', false, url('panel', '{{ classNamePath }}', 'edit', $insertID));
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax()) {
                    Request::returnErrors($this->validationErrors);
                }
            }
        }

        Request::setTitle('Add {{ className }}');
    }

    public function editAction()
    {
        $id = intval(Request::getUri(0));
        $this->view->edit = {{ className }}Model::get($id);

        if (!$this->view->edit) {
            redirect(url('panel/{{ classNamePath }}'));
        }

        if ($this->startValidation()) {
            $this->validatePost('title',        'Title',                'required|trim|min_length[1]|max_length[200]');
            $this->validatePost('image',        'Image',                'required|trim|min_length[1]|max_length[100]');
            $this->validatePost('content',      'Page Content',         'required|trim|min_length[0]');

            if ($this->isValid()) {
                $data = array(
                    'title'   => post('title'),
                    'image'   => post('image'),
                    'content' => post('content'),
                    'time'    => time()
                );

                // Copy and remove image
                if ($this->view->edit->image !== $data['image']) {
                    if (File::copy('data/tmp/' . $data['image'], 'data/{{ classNamePath }}/' . $data['image'])) {
                        File::remove('data/{{ classNamePath }}/' . $this->view->edit->image);
                        File::remove('data/{{ classNamePath }}/mini_' . $this->view->edit->image);
                        File::resize(
                            _SYSDIR_ . 'data/{{ classNamePath }}/' . $data['image'],
                            _SYSDIR_ . 'data/{{ classNamePath }}/mini_' . $data['image'],
                            600, 400
                        );
                    } else
                        Request::returnError('Image copy error: ' . error_get_last()['message']);
                }

                $result = Model::update('{{ tableName }}', $data, "`id` = '$id'");

                if ($result) {
                   //save alt attributes
                    Image::saveAlts(post('alt_attributes'), '{{ tableName }}', $id);

                    //Versions
                    $this->makeVersion($id, '{{ tableName }}', 'edit');

                    Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'edit', 'entity' => '{{ classNamePath }}#' . $id, 'time' => time()]);

                    Request::addResponse('func', 'noticeSuccess', 'Saved');
                    Request::endAjax();
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax()) {
                    Request::returnErrors($this->validationErrors);
                }
            }
        }

        Request::setTitle('Edit {{ classNamePath }}');
    }

    public function deleteAction()
    {
        Request::ajaxPart();

        $id = intval(Request::getUri(0));
        $item = {{ className }}Model::get($id);

        if (!$item) {
            Request::returnError('{{ className }} error');
        }

        $data['deleted'] = 'yes';
        $result = Model::update('{{ tableName }}', $data, "`id` = '$id'"); // Update row

        if ($result) {

            $this->makeVersion($id, '{{ tableName }}', 'delete');

            Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'delete', 'entity' => '{{ classNamePath }}#' . $id, 'time' => time()]);

            Request::addResponse('func', 'noticeSuccess', 'Removed');
            Request::addResponse('remove', '#item_' . $id);
            Request::endAjax();
        } else {
            Request::returnError('Database error');
        }
    }

    public function archiveAction()
    {
        $this->view->list = {{ className }}Model::getArchived();

        Request::setTitle('Archive {{ className }}');
    }

    public function resumeAction()
    {
        $id = Request::getUri(0);
        $item = {{ className }}Model::get($id);

        if (!$item) {
            redirect(url('panel/{{ classNamePath }}/archive'));
        }

        $result = Model::update('{{ tableName }}', ['deleted' => 'no'], "`id` = '$id'"); // Update row

        if ($result) {
            Model::insert('actions_logs', ['user_id' => User::get('id'), 'action' => 'restore', 'entity' => '{{ classNamePath }}#' . $id, 'time' => time()]);
        } else {
            Request::returnError('Database error');
        }

        redirect(url('panel/{{ classNamePath }}/archive'));
    }
}
