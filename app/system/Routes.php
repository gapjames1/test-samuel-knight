<?php
/**
* ROUTES
*/

// Example:
//Route::set('login', 'profile@login')
//    ->name('profile')
//    ->where('id', '[0-9]+')
//    ->where('name', '[A-Za-z]+')
//    ->where(['id' => '[0-9]+', 'name' => '[a-z]+']);
//Do not remove
Route::set('app/data/cvs/{slug}', 'page@file_download')->name('file_download');
Route::set('es.png', 'page@email_status')->name('e_image');
Route::set('sitemap.xml$', 'page@sitemap')->name('sitemap');
Route::set('uploads/{slug}', 'page@uploads')->name('uploads');

//Home
Route::set('candidates', 'page@candidates')->name('candidates');
Route::set('clients', 'page@clients')->name('clients');
Route::set('cookies', 'page@cookies')->name('cookies');
Route::set('energy', 'page@energy')->name('energy');
Route::set('home', 'page@index')->name('home');
Route::set('policy', 'page@policy')->name('policy');
Route::set('projects', 'page@projects')->name('projects');
Route::set('rail', 'page@rail')->name('rail');
Route::set('tech', 'page@tech')->name('tech');
Route::set('terms', 'page@terms')->name('terms');

//About US
Route::set('about-us', 'about_us@index')->name('about_us');
Route::set('contact-us', 'about_us@contact_us')->name('contact_us');
Route::set('meet-the-team', 'about_us@meet_the_team')->name('meet_the_team');

//Jobs
Route::set('jobs', 'jobs@index')->name('jobs');
Route::set('job/{slug}', 'jobs@view');

//News
Route::set('news', 'news@index')->name('news');
Route::set('news-post/{slug}', 'news@view');
/* End of file */
